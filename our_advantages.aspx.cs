﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ourAdvantage : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindContent(2);
            BindInnerBanner(3);
            BindCsr(1);
        }
    }

    public string BindContent(int Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS'," + Id + "");
        if (dt.Rows.Count > 0)
        {
            ltrContent.Text = dt.Rows[0]["Description"].ToString();
        }
        return ltrContent.Text;
    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            imgInnerBanner.ImageUrl = "Content/uploads/InnerBanner/" + dt.Rows[0]["Image"].ToString();
        }
        else
        {
            imgInnerBanner.ImageUrl = "images/banner/about_banner.jpg";
        }


    }

    public void BindCsr(int Id)
    {
        StringBuilder strTitle = new StringBuilder();
        StringBuilder strDesc = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_TabMaster 'get',0,'" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                strTitle.Append("<li>" + dr["Title"].ToString() + "</li>");

                DataTable dt1 = new DataTable();
                dt1 = utility.Display("Exec Proc_TabContent 'get',0,'" + dr["TabId"].ToString() + "'");

                strDesc.Append("<div>");
                strDesc.Append("<div class=\"whiteBox p-4 mb-4\">");
                int i = 0;
                foreach (DataRow dr1 in dt1.Rows)
                {                    
                    if (i == 0)
                    {
                        strDesc.Append("<div class=\"heading\">");
                        strDesc.Append("<strong>" + dr1["Title"].ToString() + "</strong>");
                        strDesc.Append("<i class=\"fa fa-minus\" aria-hidden=\"true\"></i>");
                        strDesc.Append("</div>");

                        strDesc.Append("<div class=\"content newCont\" style=\"display:block;\">");
                        strDesc.Append(dr1["Description"].ToString());
                        strDesc.Append("</div>");

                    }
                    else
                    {
                        strDesc.Append("<div class=\"heading\">");
                        strDesc.Append("<strong>" + dr1["Title"].ToString() + "</strong>");
                        strDesc.Append("<i class=\"fa fa-plus\" aria-hidden=\"true\"></i>");
                        strDesc.Append("</div>");

                        strDesc.Append("<div class=\"content\">");
                        strDesc.Append(dr1["Description"].ToString());
                        strDesc.Append("</div>");
                    }
                    i++;

                }
                strDesc.Append("</div>");
                strDesc.Append("</div>");

            }
            ltrHeading.Text = strTitle.ToString();
            ltrDescription.Text = strDesc.ToString();
        }
    }
}