﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class index : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            HomeBanner();
            Team();
        }
    }

    public void HomeBanner()
    {        
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_Banner 'bindBanner'");
        if (dt.Rows.Count > 0)
        {
            rptBanner.DataSource = dt;
            rptBanner.DataBind();
        }
    }

    public void Team()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_PeopleCultureMaster 'bindPeopleCulture'");
        if (dt.Rows.Count > 0)
        {
            StringBuilder strTitle = new StringBuilder();
            StringBuilder strDesc = new StringBuilder();
            foreach (DataRow dr in dt.Rows)
            {
                strTitle.Append("<li class=\"col-md-4\">" + dr["Title"].ToString() + "</li>");

                strDesc.Append("<div>");
                strDesc.Append("<div class=\"row\">");

                DataTable dt1 = new DataTable();
                dt1 = utility.Display("Exec Proc_PeopleCultureMaster 'bindPeopleCultureById','" + dr["PcmId"].ToString() + "'");
                
                foreach (DataRow dr1 in dt1.Rows)
                {
                    strDesc.Append("<div class=\"col-md-4 col-lg-3 mb-4 text-center\">");

                    strDesc.Append("<a href=\""+ dr1["slug"].ToString() + "\">");
                    strDesc.Append("<div class=\"brownBorder fadeAnim mb-4\">");
                    strDesc.Append("<img src=\"Content/uploads/Team/" + dr1["Image"].ToString() + "\" class=\"img-fluid mb-3\" alt=\"\">");
                    strDesc.Append("<h5 class=\"text-uppercase\">"+ dr1["Name"].ToString() + " </h5>");
                    strDesc.Append("<p class=\"text-uppercase\">"+ dr1["Designation"].ToString() + "</p>");                    
                    strDesc.Append("</div>");
                    strDesc.Append("<div class=\"plusIcon rounded-circle brownBg\"><span>+</span></div>");
                    strDesc.Append("</a>");

                    strDesc.Append("<div class=\"professorInfo container\" id=\"lashit\">");
                    strDesc.Append("<div class=\"row align-items-center\">");
                    strDesc.Append("<div class=\"col-md-4\"><img src=\"Content/uploads/Team/" + dr1["Image"].ToString() + "\" class=\"img-fluid mb-3\" alt=\"\"></div>");
                    strDesc.Append("<div class=\"col-md-8\">");
                    strDesc.Append("<h5 class=\"text-uppercase\">"+ dr1["Name"].ToString() + "</h5>");
                    strDesc.Append("<p class=\"borderBottom\">"+ dr1["Designation"].ToString() + "</p>");
                    strDesc.Append(dr1["Description"].ToString());           
                    strDesc.Append("</div>");
                    strDesc.Append("</div>");
                    strDesc.Append("</div>");

                    strDesc.Append("</div>");

                }

                strDesc.Append("</div>");
                strDesc.Append("</div>");
            }
            ltrTeamType.Text = strTitle.ToString();
            ltrTeamDetails.Text = strDesc.ToString();
        }

    }
}