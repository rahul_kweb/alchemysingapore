﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class media_center : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        BindInnerBanner(18);
        BindUpperDesc(14);
        BindMediaCenter();
    }



    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            imgInnerBanner.ImageUrl = "Content/uploads/InnerBanner/" + dt.Rows[0]["Image"].ToString();
        }
        else
        {
            imgInnerBanner.ImageUrl = "images/banner/about_banner.jpg";
        }


    }


    public void BindUpperDesc(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            ltrUpperDisc.Text = dt.Rows[0]["Description"].ToString();
        }

    }


    public void BindMediaCenter()
    {
        StringBuilder strTitle = new StringBuilder();
        StringBuilder strDesc = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_Insights 'MediaCenterTitle'");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                strTitle.Append("<li>" + dr["Title"].ToString() + "</li>");

                strDesc.Append("<div>");
                strDesc.Append("<h4>" + dr["Title"].ToString() + "</h4>");

                if (dr["Video"].ToString() != "")
                {
                    strDesc.Append("<div class='currentVid mb-4'>");
                    strDesc.Append("<iframe src='" + dr["Video"].ToString() + "' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>");
                    strDesc.Append("</div>");

                }

                DataTable dt1 = new DataTable();
                dt1 = utility.Display("Exec Proc_Insights 'MedianCenterYearbyTitle',0,'" + dr["Id"].ToString() + "'");
                if (dt1.Rows.Count > 0)
                {
                    int count = 0;
                    foreach (DataRow dr1 in dt1.Rows)
                    {
                        if (count == 0)
                        {
                            strDesc.Append("<div class='heading'>" + dr1["Year"].ToString() + "<i class='fa fa-minus' aria-hidden='true'></i></div>");


                            strDesc.Append("<div class='content' style='display:block'>");
                            strDesc.Append("<ul class='newsLetter'>");

                            DataTable dt2 = new DataTable();
                            dt2 = utility.Display("Exec Proc_Insights 'MediaCenterbyTitleAndYear',0,'" + dr["Id"].ToString() + "','" + dr1["Year"].ToString() + "'");
                            if (dt2.Rows.Count > 0)
                            {
                                foreach (DataRow dr2 in dt2.Rows)
                                {
                                    strDesc.Append("<li class='newsData'>");
                                    if (dr2["Type"].ToString() == "Url")
                                    {
                                        strDesc.Append("<a href='" + dr2["Url"].ToString() + "' target='_blank'>");
                                        count++;
                                    }
                                    else
                                    {
                                        strDesc.Append("<a href='Content/uploads/Pdf/" + dr2["Pdf"].ToString() + "' target='_blank'>");
                                    }
                                    strDesc.Append("<div class='newsDate'>" + dr2["PostDate"].ToString() + "</div>");
                                    strDesc.Append("<span> <strong>" + dr2["Title"].ToString() + "</strong></span>");
                                    strDesc.Append("</a>");
                                    strDesc.Append("</li>");
                                }

                            }
                            strDesc.Append("</ul>");
                            strDesc.Append("</div>");
                            count++;
                        }
                        else
                        {
                            strDesc.Append("<div class='heading'>" + dr1["Year"].ToString() + "<i class='fa fa-plus' aria-hidden='true'></i></div>");
                        

                            strDesc.Append("<div class='content'>");
                            strDesc.Append("<ul class='newsLetter'>");

                            DataTable dt2 = new DataTable();
                            dt2 = utility.Display("Exec Proc_Insights 'MediaCenterbyTitleAndYear',0,'" + dr["Id"].ToString() + "','" + dr1["Year"].ToString() + "'");
                            if (dt2.Rows.Count > 0)
                            {
                                foreach (DataRow dr2 in dt2.Rows)
                                {
                                    strDesc.Append("<li class='newsData'>");
                                    if (dr2["Type"].ToString() == "Url")
                                    {
                                        strDesc.Append("<a href='" + dr2["Url"].ToString() + "' target='_blank'>");
                                        count++;
                                    }
                                    else
                                    {
                                        strDesc.Append("<a href='Content/uploads/Pdf/" + dr2["Pdf"].ToString() + "' target='_blank'>");
                                    }
                                    strDesc.Append("<div class='newsDate'>" + dr2["PostDate"].ToString() + "</div>");
                                    strDesc.Append("<span> <strong>" + dr2["Title"].ToString() + "</strong></span>");
                                    strDesc.Append("</a>");
                                    strDesc.Append("</li>");
                                }

                            }
                            strDesc.Append("</ul>");
                            strDesc.Append("</div>");
                        }
                    }

                }

                strDesc.Append("</div>");

            }

        }
        ltrMediaTab.Text = strTitle.ToString();
        ltrDescription.Text = strDesc.ToString();
    }
}