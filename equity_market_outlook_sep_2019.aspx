﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="equity_market_outlook_sep_2019.aspx.cs" Inherits="equity_market_outlook_sep_2019" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="banner" class="position-relative mb-5">
        <h1>Thought Leadership</h1>
        <%--<img src="images/banner/thought_leadership.jpg" class="img-fluid" alt="">--%>

        <asp:Image ID="imgInnerBanner" runat="server" ImageUrl="images/banner/about_banner.jpg" class="img-fluid" alt="" />

    </div>

    <div class="container mb-4">

        <div class="row">
            <div class="col-12">

                <%--<ul class="breadcrumb">
                    <li><a href="index.aspx">Home</a></li>
                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                    <li><a href="thought_leadership.aspx">Thought Leadership</a></li>
                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                    <li>Equity Market Outlook - September 2019</li>
                </ul>--%>

                <asp:Literal ID="ltrBreakcum" runat="server"></asp:Literal>


            </div>
        </div>

        <asp:Literal ID="ltrContent" runat="server"></asp:Literal>


        <%--<div class="row mb-5 innerPageData" id="blog">
            <div class="col-12 mb-4">

                <h4 class="brownColor">EQUITY MARKET OUTLOOK - SEPTEMBER 2019 </h4>

                <p><strong>Fear of unknown risks  </strong></p>



                <p>The 7% correction† in the market (with poor inter-nals) since April 2019 has created a better sync between expectations and reality. The momentum in the economy remains poor and gives little reason for comfort, but some government action has created the hope for a recovery later this year. The risk, however, still comes from unforeseen events, especially with credit incidents like the ones we saw over the last 12-15 months. </p>

                <p><strong>Economy and earnings </strong></p>
                <p>We lead with the positives, as the negatives have been  discussed threadbare. </p>
                <ul class="bullet">
                    <li>Interest rates are continuing to fall, and we expect  transmission by the banks to improve in the next 3-4 months of CY19. Deposit  rates are already falling, and the banks are cutting MCLR to the extent  possible. The external benchmark-ing of loans should help faster transmission  from 4QCY19 onwards. Of course, we do expect the RBI to continue cutting policy  rates, given the weak growth impulses and benign inflation. </li>
                    <li>The government has responded over the last month with a  combination of short-term measures and structural changes. The com-bined effect  of these measures - FPI tax withdrawal, auto sector stimuli, PSU bank recap -  may decelerate the slowdown and create the environment for the recovery. We,  however, believe that there are not magic bullets in the government's armoury  as some market participants unrealistically expect. </li>
                    <li>Some stressed credits are slowly getting resolved. We have  seen some leveraged groups willing to sell assets to pay back lenders, even if  they are the so-called &quot;family crown jewels&quot;. In other cases, the  banks need to accept the necessary haircuts and the judicial process should  reach their logical conclusions. There is still a mountain to climb on this  front, but some recent progress is a positive. </li>
                    <li>After a slow start, the monsoons have delivered a better  outcome than initially thought. The overall rainfall has been normal and kharif  sowing is 1.7% below last year. This is a significant relief as rural  consumption has been weaker than urban over the last year. A turn in agri  sentiment could be a major catalyst. </li>
                    </ul>

                    <p>The silver linings are getting thicker, but the dark clouds  are yet to be dispelled. We believe that the stress points persist on multiple  fronts. </p>
                <ul>
                    <li>The real estate market is caught in a vicious cycle. Slow  sales have put balance sheets under stress, especially in a post-RERA world.  This has affected the ability to complete projects and put pressure on existing  credit to the sector. Consequently, sales are slowing even further as buyers  risk appetite with developers is shrinking. The cycle can be broken with liquidity  but the mountain of stressed debts is deterring fresh lenders to this sector. </li>

                    <li>Falling interest rates are not transmitting to Lower-rated  borrowers, as the bond markets are still in deep-freeze. The multiple credit  events have destroyed investors appetite for bonds and pushed up credit  spreads. The biggest impact of this is on NBFCs and HFCs, whose loan  disbursements are very sluggish. </li>

                    <li>The NBFC stress (and other factors) has frozen  liquidity to the SME sector. This has cascaded into a demand slowdown across  categories, both via lower employment and through slower flow of products  through channel pipes. The government and the RBI have stepped in to ease  credit flow to the sector. It will take a little time for this to permeate to  the weaker NBFCs, who face the bigger challenge.</li>
                </ul>

                <p>† Performance of S&amp;P BSE 500 Index since 31 Mar 2019. </p>

                <p><strong>Source : Bloomberg</strong></p>

                <div class="brown-dashed-border mb-5"></div>

                <ul class="bullet">
                    <li>The current grovvth scenario remains weak. Auto sales are collapsing, most consumer categories are witnessing sluggish demand and capital goods companies see very little order- book joy. There is no visible sign of a recovery, despite all the positive changes in lead indica- tors in recent months.</li>

                    <li>The earnings scenario remains challenging. The stress has shifted from the banks to the consumer sector, especially among the discre- tionary sub segments. Aggregate earnings for the Nifty has remained challenged for some years now, and the continued pressure on the economy has sustained pressure on analyst earnings estimates.</li>

                </ul>

                <p><strong>Future risks</strong></p>

                <p>The recovery would be hurt by any further credit incidents, like we saw in the last 12-18 months. There are multiple pockets of vulnerability that we worry about</p>
                <ul class="bullet">
                    <li><strong>Real estate:</strong> the sector has been very sluggish, and the nature of the business model depends on continuous flow to keep credit healthy. Leverage levels are generally high and this makes the sector vulnerable to accidents.</li>
                    <li>Many NBFCs/HFCs have large and lumpy exposure to the Real estate sector. The security could turn very illiquid and, in times of tight liquidity such as this, could impact these balance sheets.</li>
                    <li>Promoter leverage has already created a number of accidents in the recent past. The worst should be over, but any more such incidents would trigger a fresh round of credit aversion. The worry is that NPAs from this segment is procyclical with a falling market.</li>
                </ul>

                <p><strong>Conclusion</strong></p>

                <ul class="bullet">
                    <li>We remain cautious on deployment and are not accelerating our deployment to any significant degree. We would prefer the macro risks to be priced in better.</li>
                    <li>The mid cap correction has given us a larger pool of stocks to choose from. Our bottom-up strategy of building a quality, high-growth strategy does not change.</li>
                    <li>The correction in the market has made equities more attractive. Instant gratification is unlikely but longer-term returns have become more attractive now.</li>
                </ul>

                <p><strong>Source - Alchemy Group Research</strong></p>

            </div>




        </div>--%>

        <div class="brown-dashed-border mb-5"></div>


    </div>

    <script>
        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

    </script>
</asp:Content>

