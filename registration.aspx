﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="registration.aspx.cs" Inherits="registration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="banner" class="position-relative mb-5">
        <h1>Registration</h1>
        <asp:Image ID="imgInnerBanner" runat="server" ImageUrl="images/banner/about_banner.jpg" class="img-fluid" alt="" />

        <%--<img src="images/banner/login_banner.jpg" class="img-fluid" alt="">--%>
    </div>

    <div class="container mb-4">
        <div id="divMsg"></div>
        <div class="row mb-5 innerPageData" id="loginForm">
            <div class="col-12">
                <h4>Fill Your Details</h4>
                <p id="pAll">All <strong class="text-danger">*</strong> fields are mandatory</p>
                <div class="alert alert-warning alert-dismissible fade show" role="alert" style="display: none;">
                    <strong>Following fileds are required!</strong><br />
                    <label id="lblmsg"></label>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="alert alert-danger alert-dismissible fade show" role="alert" id="divError" runat="server" visible="false">
                    <strong>
                        <asp:Label ID="lblError" runat="server"></asp:Label>
                    </strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="alert alert-success alert-dismissible fade show" role="alert" id="divSuccess" runat="server" visible="false">
                    <strong>Thank You!</strong>
                    <br>
                    Thank you registration with us. Your username and password sent to your registered email id.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="row">

                    <div class="col-md-6 col-lg-4">
                        <label>Name<span class="text-danger">*</span></label>
                        <asp:TextBox ID="txtName" runat="server" CssClass="form-control" autocomplete="off"></asp:TextBox>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <label>Phone Number</label>
                        <asp:TextBox ID="txtLandline" runat="server" CssClass="form-control" autocomplete="off"></asp:TextBox>
                    </div>

                    <div class="col-md-6 col-lg-4">
                        <label>Mobile<span class="text-danger">*</span></label>
                        <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control" onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                    </div>

                    <div class="col-md-6 col-lg-4">
                        <label>Email<span class="text-danger">*</span></label>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" autocomplete="off"></asp:TextBox>
                    </div>

                    <div class="col-md-6 col-lg-4">
                        <label>Country<span class="text-danger">*</span></label>
                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control">
                            <asp:ListItem Value="0">Select Country</asp:ListItem>
                            <asp:ListItem Value="AF">Afghanistan</asp:ListItem>
                            <asp:ListItem Value="AL">Albania</asp:ListItem>
                            <asp:ListItem Value="DZ">Algeria</asp:ListItem>
                            <asp:ListItem Value="AS">American Samoa</asp:ListItem>
                            <asp:ListItem Value="AD">Andorra</asp:ListItem>
                            <asp:ListItem Value="AO">Angola</asp:ListItem>
                            <asp:ListItem Value="AI">Anguilla</asp:ListItem>
                            <asp:ListItem Value="AQ">Antarctica</asp:ListItem>
                            <asp:ListItem Value="AG">Antigua And Barbuda</asp:ListItem>

                            <asp:ListItem Value="AR">Argentina</asp:ListItem>

                            <asp:ListItem Value="AM">Armenia</asp:ListItem>

                            <asp:ListItem Value="AW">Aruba</asp:ListItem>

                            <asp:ListItem Value="AU">Australia</asp:ListItem>

                            <asp:ListItem Value="AT">Austria</asp:ListItem>

                            <asp:ListItem Value="AZ">Azerbaijan</asp:ListItem>

                            <asp:ListItem Value="BS">Bahamas</asp:ListItem>

                            <asp:ListItem Value="BH">Bahrain</asp:ListItem>

                            <asp:ListItem Value="BD">Bangladesh</asp:ListItem>

                            <asp:ListItem Value="BB">Barbados</asp:ListItem>

                            <asp:ListItem Value="BY">Belarus</asp:ListItem>

                            <asp:ListItem Value="BE">Belgium</asp:ListItem>

                            <asp:ListItem Value="BZ">Belize</asp:ListItem>

                            <asp:ListItem Value="BJ">Benin</asp:ListItem>

                            <asp:ListItem Value="BM">Bermuda</asp:ListItem>

                            <asp:ListItem Value="BT">Bhutan</asp:ListItem>

                            <asp:ListItem Value="BO">Bolivia</asp:ListItem>

                            <asp:ListItem Value="BA">Bosnia And Herzegowina</asp:ListItem>

                            <asp:ListItem Value="BW">Botswana</asp:ListItem>

                            <asp:ListItem Value="BV">Bouvet Island</asp:ListItem>

                            <asp:ListItem Value="BR">Brazil</asp:ListItem>

                            <asp:ListItem Value="IO">British Indian Ocean Territory</asp:ListItem>

                            <asp:ListItem Value="BN">Brunei Darussalam</asp:ListItem>

                            <asp:ListItem Value="BG">Bulgaria</asp:ListItem>

                            <asp:ListItem Value="BF">Burkina Faso</asp:ListItem>

                            <asp:ListItem Value="BI">Burundi</asp:ListItem>

                            <asp:ListItem Value="KH">Cambodia</asp:ListItem>

                            <asp:ListItem Value="CM">Cameroon</asp:ListItem>

                            <asp:ListItem Value="CA">Canada</asp:ListItem>

                            <asp:ListItem Value="CV">Cape Verde</asp:ListItem>

                            <asp:ListItem Value="KY">Cayman Islands</asp:ListItem>

                            <asp:ListItem Value="CF">Central African Republic</asp:ListItem>

                            <asp:ListItem Value="TD">Chad</asp:ListItem>

                            <asp:ListItem Value="CL">Chile</asp:ListItem>

                            <asp:ListItem Value="CN">China</asp:ListItem>

                            <asp:ListItem Value="CX">Christmas Island</asp:ListItem>

                            <asp:ListItem Value="CC">Cocos (Keeling) Islands</asp:ListItem>

                            <asp:ListItem Value="CO">Colombia</asp:ListItem>

                            <asp:ListItem Value="KM">Comoros</asp:ListItem>

                            <asp:ListItem Value="CG">Congo</asp:ListItem>

                            <asp:ListItem Value="CK">Cook Islands</asp:ListItem>

                            <asp:ListItem Value="CR">Costa Rica</asp:ListItem>

                            <asp:ListItem Value="CI">Cote D'Ivoire</asp:ListItem>

                            <asp:ListItem Value="HR">Croatia (Local Name: Hrvatska)</asp:ListItem>

                            <asp:ListItem Value="CU">Cuba</asp:ListItem>

                            <asp:ListItem Value="CY">Cyprus</asp:ListItem>

                            <asp:ListItem Value="CZ">Czech Republic</asp:ListItem>

                            <asp:ListItem Value="DK">Denmark</asp:ListItem>

                            <asp:ListItem Value="DJ">Djibouti</asp:ListItem>

                            <asp:ListItem Value="DM">Dominica</asp:ListItem>

                            <asp:ListItem Value="DO">Dominican Republic</asp:ListItem>

                            <asp:ListItem Value="TP">East Timor</asp:ListItem>

                            <asp:ListItem Value="EC">Ecuador</asp:ListItem>

                            <asp:ListItem Value="EG">Egypt</asp:ListItem>

                            <asp:ListItem Value="SV">El Salvador</asp:ListItem>

                            <asp:ListItem Value="GQ">Equatorial Guinea</asp:ListItem>

                            <asp:ListItem Value="ER">Eritrea</asp:ListItem>

                            <asp:ListItem Value="EE">Estonia</asp:ListItem>

                            <asp:ListItem Value="ET">Ethiopia</asp:ListItem>

                            <asp:ListItem Value="FK">Falkland Islands (Malvinas)</asp:ListItem>

                            <asp:ListItem Value="FO">Faroe Islands</asp:ListItem>

                            <asp:ListItem Value="FJ">Fiji</asp:ListItem>

                            <asp:ListItem Value="FI">Finland</asp:ListItem>

                            <asp:ListItem Value="FR">France</asp:ListItem>

                            <asp:ListItem Value="GF">French Guiana</asp:ListItem>

                            <asp:ListItem Value="PF">French Polynesia</asp:ListItem>

                            <asp:ListItem Value="TF">French Southern Territories</asp:ListItem>

                            <asp:ListItem Value="GA">Gabon</asp:ListItem>

                            <asp:ListItem Value="GM">Gambia</asp:ListItem>

                            <asp:ListItem Value="GE">Georgia</asp:ListItem>

                            <asp:ListItem Value="DE">Germany</asp:ListItem>

                            <asp:ListItem Value="GH">Ghana</asp:ListItem>

                            <asp:ListItem Value="GI">Gibraltar</asp:ListItem>

                            <asp:ListItem Value="GR">Greece</asp:ListItem>

                            <asp:ListItem Value="GL">Greenland</asp:ListItem>

                            <asp:ListItem Value="GD">Grenada</asp:ListItem>

                            <asp:ListItem Value="GP">Guadeloupe</asp:ListItem>

                            <asp:ListItem Value="GU">Guam</asp:ListItem>

                            <asp:ListItem Value="GT">Guatemala</asp:ListItem>

                            <asp:ListItem Value="GN">Guinea</asp:ListItem>

                            <asp:ListItem Value="GW">Guinea-Bissau</asp:ListItem>

                            <asp:ListItem Value="GY">Guyana</asp:ListItem>

                            <asp:ListItem Value="HT">Haiti</asp:ListItem>

                            <asp:ListItem Value="HM">Heard And Mc Donald Islands</asp:ListItem>

                            <asp:ListItem Value="VA">Holy See (Vatican City State)</asp:ListItem>

                            <asp:ListItem Value="HN">Honduras</asp:ListItem>

                            <asp:ListItem Value="HK">Hong Kong</asp:ListItem>

                            <asp:ListItem Value="HU">Hungary</asp:ListItem>

                            <asp:ListItem Value="IS">Iceland</asp:ListItem>

                            <asp:ListItem Value="IN">India</asp:ListItem>

                            <asp:ListItem Value="ID">Indonesia</asp:ListItem>

                            <asp:ListItem Value="IR">Iran (Islamic Republic Of)</asp:ListItem>

                            <asp:ListItem Value="IQ">Iraq</asp:ListItem>

                            <asp:ListItem Value="IE">Ireland</asp:ListItem>

                            <asp:ListItem Value="IL">Israel</asp:ListItem>

                            <asp:ListItem Value="IT">Italy</asp:ListItem>

                            <asp:ListItem Value="JM">Jamaica</asp:ListItem>

                            <asp:ListItem Value="JP">Japan</asp:ListItem>

                            <asp:ListItem Value="JO">Jordan</asp:ListItem>

                            <asp:ListItem Value="KZ">Kazakhstan</asp:ListItem>

                            <asp:ListItem Value="KE">Kenya</asp:ListItem>

                            <asp:ListItem Value="KI">Kiribati</asp:ListItem>

                            <asp:ListItem Value="KP">Korea, Dem People'S Republic</asp:ListItem>

                            <asp:ListItem Value="KR">Korea, Republic Of</asp:ListItem>

                            <asp:ListItem Value="KW">Kuwait</asp:ListItem>

                            <asp:ListItem Value="KG">Kyrgyzstan</asp:ListItem>

                            <asp:ListItem Value="LA">Lao People'S Dem Republic</asp:ListItem>

                            <asp:ListItem Value="LV">Latvia</asp:ListItem>

                            <asp:ListItem Value="LB">Lebanon</asp:ListItem>

                            <asp:ListItem Value="LS">Lesotho</asp:ListItem>

                            <asp:ListItem Value="LR">Liberia</asp:ListItem>

                            <asp:ListItem Value="LY">Libyan Arab Jamahiriya</asp:ListItem>

                            <asp:ListItem Value="LI">Liechtenstein</asp:ListItem>

                            <asp:ListItem Value="LT">Lithuania</asp:ListItem>

                            <asp:ListItem Value="LU">Luxembourg</asp:ListItem>

                            <asp:ListItem Value="MO">Macau</asp:ListItem>

                            <asp:ListItem Value="MK">Macedonia</asp:ListItem>

                            <asp:ListItem Value="MG">Madagascar</asp:ListItem>

                            <asp:ListItem Value="MW">Malawi</asp:ListItem>

                            <asp:ListItem Value="MY">Malaysia</asp:ListItem>

                            <asp:ListItem Value="MV">Maldives</asp:ListItem>

                            <asp:ListItem Value="ML">Mali</asp:ListItem>

                            <asp:ListItem Value="MT">Malta</asp:ListItem>

                            <asp:ListItem Value="MH">Marshall Islands</asp:ListItem>

                            <asp:ListItem Value="MQ">Martinique</asp:ListItem>

                            <asp:ListItem Value="MR">Mauritania</asp:ListItem>

                            <asp:ListItem Value="MU">Mauritius</asp:ListItem>

                            <asp:ListItem Value="YT">Mayotte</asp:ListItem>

                            <asp:ListItem Value="MX">Mexico</asp:ListItem>

                            <asp:ListItem Value="FM">Micronesia, Federated States</asp:ListItem>

                            <asp:ListItem Value="MD">Moldova, Republic Of</asp:ListItem>

                            <asp:ListItem Value="MC">Monaco</asp:ListItem>

                            <asp:ListItem Value="MN">Mongolia</asp:ListItem>

                            <asp:ListItem Value="MS">Montserrat</asp:ListItem>

                            <asp:ListItem Value="MA">Morocco</asp:ListItem>

                            <asp:ListItem Value="MZ">Mozambique</asp:ListItem>

                            <asp:ListItem Value="MM">Myanmar</asp:ListItem>

                            <asp:ListItem Value="NA">Namibia</asp:ListItem>

                            <asp:ListItem Value="NR">Nauru</asp:ListItem>

                            <asp:ListItem Value="NP">Nepal</asp:ListItem>

                            <asp:ListItem Value="NL">Netherlands</asp:ListItem>

                            <asp:ListItem Value="AN">Netherlands Ant Illes</asp:ListItem>

                            <asp:ListItem Value="NC">New Caledonia</asp:ListItem>

                            <asp:ListItem Value="NZ">New Zealand</asp:ListItem>

                            <asp:ListItem Value="NI">Nicaragua</asp:ListItem>

                            <asp:ListItem Value="NE">Niger</asp:ListItem>

                            <asp:ListItem Value="NG">Nigeria</asp:ListItem>

                            <asp:ListItem Value="NU">Niue</asp:ListItem>

                            <asp:ListItem Value="NF">Norfolk Island</asp:ListItem>

                            <asp:ListItem Value="MP">Northern Mariana Islands</asp:ListItem>

                            <asp:ListItem Value="NO">Norway</asp:ListItem>

                            <asp:ListItem Value="OM">Oman</asp:ListItem>

                            <asp:ListItem Value="PK">Pakistan</asp:ListItem>

                            <asp:ListItem Value="PW">Palau</asp:ListItem>

                            <asp:ListItem Value="PA">Panama</asp:ListItem>

                            <asp:ListItem Value="PG">Papua New Guinea</asp:ListItem>

                            <asp:ListItem Value="PY">Paraguay</asp:ListItem>

                            <asp:ListItem Value="PE">Peru</asp:ListItem>

                            <asp:ListItem Value="PH">Philippines</asp:ListItem>

                            <asp:ListItem Value="PN">Pitcairn</asp:ListItem>

                            <asp:ListItem Value="PL">Poland</asp:ListItem>

                            <asp:ListItem Value="PT">Portugal</asp:ListItem>

                            <asp:ListItem Value="PR">Puerto Rico</asp:ListItem>

                            <asp:ListItem Value="QA">Qatar</asp:ListItem>

                            <asp:ListItem Value="RE">Reunion</asp:ListItem>

                            <asp:ListItem Value="RO">Romania</asp:ListItem>

                            <asp:ListItem Value="RU">Russian Federation</asp:ListItem>

                            <asp:ListItem Value="RW">Rwanda</asp:ListItem>

                            <asp:ListItem Value="KN">Saint K Itts And Nevis</asp:ListItem>

                            <asp:ListItem Value="LC">Saint Lucia</asp:ListItem>

                            <asp:ListItem Value="VC">Saint Vincent, The Grenadines</asp:ListItem>

                            <asp:ListItem Value="WS">Samoa</asp:ListItem>

                            <asp:ListItem Value="SM">San Marino</asp:ListItem>

                            <asp:ListItem Value="ST">Sao Tome And Principe</asp:ListItem>

                            <asp:ListItem Value="SA">Saudi Arabia</asp:ListItem>

                            <asp:ListItem Value="SN">Senegal</asp:ListItem>

                            <asp:ListItem Value="SC">Seychelles</asp:ListItem>

                            <asp:ListItem Value="SL">Sierra Leone</asp:ListItem>

                            <asp:ListItem Value="SG">Singapore</asp:ListItem>

                            <asp:ListItem Value="SK">Slovakia (Slovak Republic)</asp:ListItem>

                            <asp:ListItem Value="SI">Slovenia</asp:ListItem>

                            <asp:ListItem Value="SB">Solomon Islands</asp:ListItem>

                            <asp:ListItem Value="SO">Somalia</asp:ListItem>

                            <asp:ListItem Value="ZA">South Africa</asp:ListItem>

                            <asp:ListItem Value="GS">South Georgia , S Sandwich Is.</asp:ListItem>

                            <asp:ListItem Value="ES">Spain</asp:ListItem>

                            <asp:ListItem Value="LK">Sri Lanka</asp:ListItem>

                            <asp:ListItem Value="SH">St. Helena</asp:ListItem>

                            <asp:ListItem Value="PM">St. Pierre And Miquelon</asp:ListItem>

                            <asp:ListItem Value="SD">Sudan</asp:ListItem>

                            <asp:ListItem Value="SR">Suriname</asp:ListItem>

                            <asp:ListItem Value="SJ">Svalbard, Jan Mayen Islands</asp:ListItem>

                            <asp:ListItem Value="SZ">Sw Aziland</asp:ListItem>

                            <asp:ListItem Value="SE">Sweden</asp:ListItem>

                            <asp:ListItem Value="CH">Switzerland</asp:ListItem>

                            <asp:ListItem Value="SY">Syrian Arab Republic</asp:ListItem>

                            <asp:ListItem Value="TW">Taiwan</asp:ListItem>

                            <asp:ListItem Value="TJ">Tajikistan</asp:ListItem>

                            <asp:ListItem Value="TZ">Tanzania, United Republic Of</asp:ListItem>

                            <asp:ListItem Value="TH">Thailand</asp:ListItem>

                            <asp:ListItem Value="TG">Togo</asp:ListItem>

                            <asp:ListItem Value="TK">Tokelau</asp:ListItem>

                            <asp:ListItem Value="TO">Tonga</asp:ListItem>

                            <asp:ListItem Value="TT">Trinidad And Tobago</asp:ListItem>

                            <asp:ListItem Value="TN">Tunisia</asp:ListItem>

                            <asp:ListItem Value="TR">Turkey</asp:ListItem>

                            <asp:ListItem Value="TM">Turkmenistan</asp:ListItem>

                            <asp:ListItem Value="TC">Turks And Caicos Islands</asp:ListItem>

                            <asp:ListItem Value="TV">Tuvalu</asp:ListItem>

                            <asp:ListItem Value="UG">Uganda</asp:ListItem>

                            <asp:ListItem Value="UA">Ukraine</asp:ListItem>

                            <asp:ListItem Value="AE">United Arab Emirates</asp:ListItem>

                            <asp:ListItem Value="GB">United Kingdom</asp:ListItem>

                            <asp:ListItem Value="US">United States</asp:ListItem>

                            <asp:ListItem Value="UM">United States Minor Is.</asp:ListItem>

                            <asp:ListItem Value="UY">Uruguay</asp:ListItem>

                            <asp:ListItem Value="UZ">Uzbekistan</asp:ListItem>

                            <asp:ListItem Value="VU">Vanuatu</asp:ListItem>

                            <asp:ListItem Value="VE">Venezuela</asp:ListItem>

                            <asp:ListItem Value="VN">Viet Nam</asp:ListItem>

                            <asp:ListItem Value="VG">Virgin Islands (British)</asp:ListItem>

                            <asp:ListItem Value="VI">Virgin Islands (U.S.)</asp:ListItem>

                            <asp:ListItem Value="WF">Wallis And Futuna Islands</asp:ListItem>

                            <asp:ListItem Value="EH">Western Sahara</asp:ListItem>

                            <asp:ListItem Value="YE">Yemen</asp:ListItem>

                            <asp:ListItem Value="YU">Yugoslavia</asp:ListItem>

                            <asp:ListItem Value="ZR">Zaire</asp:ListItem>

                            <asp:ListItem Value="ZM">Zambia</asp:ListItem>

                            <asp:ListItem Value="ZW">Zimbabwe</asp:ListItem>
                        </asp:DropDownList>
                    </div>

<div class="col-12">
                        <label>Address</label>
                        <asp:TextBox ID="txtAddress" textmode="multiline" runat="server" CssClass="form-control"  autocomplete="off"></asp:TextBox>
                    </div>


                    <h4 class="col-12 mt-4">How were you referred: </h4>

                    <div class="col-md-4 col-lg-4">
                        <label class="radioContainer">
                            Existing Client
                            <asp:RadioButton ID="rdbExistingClient" runat="server" GroupName="h" />
                            <span class="checkmark"></span>
                        </label>
                    </div>

                    <div class="col-md-4 col-lg-4">
                        <label class="radioContainer">
                            By a Distributor
                            <asp:RadioButton ID="rdbByADistributor" runat="server" GroupName="h" />
                            <span class="checkmark"></span>
                        </label>
                    </div>

                    <div class="col-md-4 col-lg-4">
                        <div class="radioBtn">
                            <label class="radioContainer otherErr">
                                Others
                            <asp:RadioButton ID="rdbOther" runat="server" GroupName="h" />
                                <span class="checkmark"></span>
                            </label>
                        </div>

                        <div class="radioBtnField firstField">
                            <asp:TextBox ID="txtOtherPleaseSpecify" runat="server" CssClass="form-control" placeholder="Please Specify"></asp:TextBox>

                        </div>
                    </div>

                    <h4 class="col-12 mt-4">Nature of Relationship desired:</h4>

                    <div class="col-md-4 col-lg-4">
                        <div class="radioBtn">
                            <label class="radioContainer">
                                Investor
                                <asp:RadioButton ID="rdbInvestor" runat="server" GroupName="N" />
                                <span class="checkmark"></span>
                            </label>
                        </div>

                        <div class="radioBtnField secondField">
                            <div>
                                <label class="radioContainer">
                                    Individual
                                <asp:RadioButton ID="rdbIndividual" runat="server" GroupName="i" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>

                            <div>
                                <label class="radioContainer">
                                    Non Individual
                                <asp:RadioButton ID="rdbNonIndividual" runat="server" GroupName="i" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-4">
                        <label class="radioContainer">
                            Distributor / Agent
                                <asp:RadioButton ID="rdbDistributorAgent" runat="server" GroupName="N" />
                            <span class="checkmark"></span>
                        </label>
                    </div>

                    <div class="col-md-4 col-lg-4">
                        <div class="radioBtn">
                            <label class="radioContainer">
                                Service Provider – Administrator / Broker / Prime Broker
                                <asp:RadioButton ID="rdbServiceProvider" runat="server" GroupName="N" />
                                <span class="checkmark"></span>
                            </label>
                        </div>

                        <div class="radioBtnField secondField">
                            <asp:TextBox ID="txtNatureOfRelationship" runat="server" CssClass="form-control" placeholder="Nature of relationship envisaged"></asp:TextBox>

                        </div>
                    </div>

                    <%--<p class="col-12"><strong>Type of Person:<span>*</span></strong></p>--%>
                    <h4 class="col-12 mt-4">Type of Person:<span>*</span></h4>
                    <div class="col-lg-4 col-md-4 col-sm-6 mb-3 usPersonField">
                        <div class="radioBtn">
                            <label class="radioContainer">
                                US Person
                                <asp:RadioButton ID="rdbUSPerson" runat="server" GroupName="T" />
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        

                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-6 mb-3 nonUsPersonField">
                        <div class="radioBtn">
                            <label class="radioContainer">
                                Non US Person
                                <asp:RadioButton ID="rdbNonUSPerson" runat="server" GroupName="T" />
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>

                    
                        
                        <div class="radioBtnField thirdField col-12">
                            <p><strong><a href="popup.html" 
                              target="popup" 
                              onclick="window.open('popup.html','popup','width=600,height=600'); return false;">Read Policy Terms</a></strong></p>
                            <p>I qualify to be an "accredited investor" under the Securities Act of 1933.</p>
                            <div class="d-flex">    
                            <label class="radioContainer mr-5">
                                Yes                                
                                <asp:RadioButton ID="rdbUSPersonYes" runat="server" GroupName="U" />
                                <span class="checkmark"></span>
                            </label>

                            <label class="radioContainer">
                                No
                                <asp:RadioButton ID="rdbUSPersonNo" runat="server" GroupName="U" />

                                <span class="checkmark"></span>
                            </label>
                            </div>

                            <asp:TextBox ID="txtUSPersonCondition" runat="server" Text="Securities Act of 1933
Rule 501 -- Definitions and Terms Used in Regulation D
________________________________________
    
As used in Regulation D, the following terms shall have the meaning indicated:
Accredited investor. Accredited investor shall mean any person who comes within any of the following categories, or who the issuer reasonably believes comes within any of the following categories, at the time of the sale of the securities to that person:
Any bank as defined in section 3(a)(2) of the Act, or any savings and loan association or other institution as defined in section 3(a)(5)(A) of the Act whether acting in its individual or fiduciary capacity; any broker or dealer registered pursuant to section 15 of the Securities Exchange Act of 1934; any insurance company as defined in section 2(a)(13) of the Act; any investment company registered under the Investment Company Act of 1940 or a business development company as defined in section 2(a)(48) of that Act; any Small Business Investment Company licensed by the U.S. Small Business Administration under section 301(c) or (d) of the Small Business Investment Act of 1958; any plan established and maintained by a state, its political subdivisions, or any agency or instrumentality of a state or its political subdivisions, for the benefit of its employees, if such plan has total assets in excess of $5,000,000; any employee benefit plan within the meaning of the Employee Retirement Income Security Act of 1974 if the investment decision is made by a plan fiduciary, as defined in section 3(21) of such act, which is either a bank, savings and loan association, insurance company, or registered investment adviser, or if the employee benefit plan has total assets in excess of $5,000,000 or, if a self-directed plan, with investment decisions made solely by persons that are accredited investors;
Any private business development company as defined in section 202(a)(22) of the Investment Advisers Act of 1940;
Any organization described in section 501(c)(3) of the Internal Revenue Code, corporation, Massachusetts or similar business trust, or partnership, not formed for the specific purpose of acquiring the securities offered, with total assets in excess of $5,000,000;
Any director, executive officer, or general partner of the issuer of the securities being offered or sold, or any director, executive officer, or general partner of a general partner of that issuer;
Any natural person whose individual net worth, or joint net worth with that person's spouse, at the time of his purchase exceeds $1,000,000;
Any natural person who had an individual income in excess of $200,000 in each of the two most recent years or joint income with that person's spouse in excess of $300,000 in each of those years and has a reasonable expectation of reaching the same income level in the current year;
Any trust, with total assets in excess of $5,000,000, not formed for the specific purpose of acquiring the securities offered, whose purchase is directed by a sophisticated person as described in Rule 506(b)(2)(ii) and
Any entity in which all of the equity owners are accredited investors.
YOU ARE REQUESTED TO CONFIRM THE APPLICABILITY WITH THE PREVAILING LAWS"
                                TextMode="MultiLine" CssClass="form-control bigTextArea"></asp:TextBox>
                        </div>

                           


                    <div class="col-12">
                        <p>Enter the text from the given image in the text box below</p>
                        <div class="row">
                            <div class="col-sm-4 col-md-3 col-lg-2 mb-3">
                                <%--<img src="images/captcha.jpg" class="img-fluid">--%>
                                <asp:Image ID="imgCaptcha" runat="server" class="img-fluid" />

                            </div>
                            <div class="col-sm-8 col-md-9 col-lg-5 mb-3">
                                <asp:TextBox ID="txtCaptcha" runat="server" CssClass="form-control" autocomplete="off" onkeypress="return isNumberKey1(event)"></asp:TextBox>
                            </div>
                        </div>
                    </div>


                    <div class="col-12">
                        <div class="row align-items-center">
                            <div class="col-12 mb-4">
                                <label class="radioContainer">
                                    I confirm the veracity and accuracy of information provided herein.
                                    <asp:CheckBox ID="chkConfirm" runat="server" />
                                    <span class="checkmark_arrow"></span>
                                </label>

                            </div>

                            <div class="col-12">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="submitBtn" Text="Register" OnClick="btnSubmit_Click" OnClientClick="return CheckValidation()" />
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="brown-dashed-border mb-5"></div>

    </div>

    <script>

        $('input[name="ctl00$ContentPlaceHolder1$h"]').click(function () {
            if ($(this).is(':checked')) {
                $('.firstField').slideUp()
                $(this).parents('.radioBtn').siblings('.firstField').slideDown();
            }
        })

        /*****************************************************************************/

        $('input[name="ctl00$ContentPlaceHolder1$N"]').click(function () {
            if ($(this).is(':checked')) {
                $('.secondField').slideUp()
                $(this).parents('.radioBtn').siblings('.secondField').slideDown();
            }
        })
        /*****************************************************************************/

        $('.usPersonField input[name="ctl00$ContentPlaceHolder1$T"]').click(function () {
            if ($(this).is(':checked')) {
                $('.thirdField').slideUp()
                $(this).parents('.usPersonField').siblings('.thirdField').slideDown();
            }
        })

        $('.nonUsPersonField input[name="ctl00$ContentPlaceHolder1$T"]').click(function () {
            if ($(this).is(':checked')) {
                $('.thirdField').slideUp()
                //$(this).parents('.usPersonField').siblings('.thirdField').slideDown();
            }
        })

        
        /*****************************************************************************/


        $('.windowOpen').click(function () {
            $('.policyDiv').show();
        })

        //$('.radioContainer').click(function () {
        //    //alert($(this).checked);
        //    if ($(this).children('input').is(':checked')) {
        //        $('.radioBtnField').slideUp()
        //        $(this).parents('.radioBtn').siblings('.radioBtnField').slideDown();
        //    }
        //})

        /**************************************************************************/


        if (window.location.hash) {
            var hash = window.location.hash;
            console.log(hash);
            $(hash).trigger('click');
            $('html, body').animate({ scrollTop: $('#verticalTab').offset().top - 80 }, 1500);
        }

        /**************************************************************************/

        var body = $("html, body");

        $('h2.resp-accordion').click(function () {
            var scrollTopVert = $(this).parent().offset().top;
            console.log(scrollTopVert);
            body.animate({ scrollTop: scrollTopVert }, 500, 'swing');
        });

        $('.offering').click(function () {
            var scrollTopVert = $('#alchemy_offering').offset().top - 100;
            body.animate({ scrollTop: scrollTopVert }, 500, 'swing');
        })

        /*******************************************************************/

        $('form select').change(function () {
            var selectedText = $(this).find("option:selected").text();
            console.log(selectedText);

            if (selectedText == 'Others') {

                $('.otherField').slideDown();
            } else {
                $('.otherField').slideUp();
            }

        })

        /*******************************************************************/

        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function (event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });

        /*******************************************************************/

        //$('.heading').click(function () {
        //    if ($(this).children('.fa').hasClass('fa-plus')) {
        //        $(this).children('.fa').removeClass('fa-plus');
        //        $(this).siblings('.heading').children('.fa').addClass('fa-plus');
        //        $(this).children('.fa').addClass('fa-minus');
        //        $(this).siblings('.heading').children('.fa').removeClass('fa-minus');
        //    } else {
        //        $(this).children('.fa').addClass('fa-plus');
        //        //$(this).siblings('.heading').children('.fa').removeClass('fa-plus');
        //        $(this).children('.fa').removeClass('fa-minus');
        //        //$(this).siblings('.heading').children('.fa').addClass('fa-minus');
        //    }
        //    $(this).next().siblings('.content').slideUp();
        //    $(this).next().slideToggle();

        //})

        /*******************************************************************/

        function CheckValidation() {
            var name = $('#<%=txtName.ClientID%>').val();
            var mobile = $('#<%=txtMobile.ClientID%>').val();
            var email = $('#<%=txtEmail.ClientID%>').val();
            var country = $('#<%=ddlCountry.ClientID%>').val();
            var address = $('#<%=txtAddress.ClientID%>').val();

            var ExistingClient = $('#<%=rdbExistingClient.ClientID%>').is(':checked');
            var ByADistributor = $('#<%=rdbByADistributor.ClientID%>').is(':checked');
            var Other = $('#<%=rdbOther.ClientID%>').is(':checked');
            var OtherPleaseSpecify = $('#<%=txtOtherPleaseSpecify.ClientID%>').val();

            var Investor = $('#<%=rdbInvestor.ClientID%>').is(':checked');
            var Individual = $('#<%=rdbIndividual.ClientID%>').is(':checked');
            var NonIndividual = $('#<%=rdbNonIndividual.ClientID%>').is(':checked');
            var DistributorAgent = $('#<%=rdbDistributorAgent.ClientID%>').is(':checked');
            var ServiceProvider = $('#<%=rdbServiceProvider.ClientID%>').is(':checked');
            var NatureOfRelationship = $('#<%=txtNatureOfRelationship.ClientID%>').val();

            var USPerson = $('#<%=rdbUSPerson.ClientID%>').is(':checked');
            var USPersonYes = $('#<%=rdbUSPersonYes.ClientID%>').is(':checked');
            var USPersonNo = $('#<%=rdbUSPersonNo.ClientID%>').is(':checked');
            var NonUSPerson = $('#<%=rdbNonUSPerson.ClientID%>').is(':checked');

            var Captcha = $('#<%=txtCaptcha.ClientID%>').val();
            var Confirm = $('#<%=chkConfirm.ClientID%>').is(':checked');

            var msg = "";
            var blank = false;

            if (name == '') {
                $('#<%=txtName.ClientID%>').css('border-color', 'red');
                msg += "Name, ";
                blank = true;
            } else {
                $('#<%=txtName.ClientID%>').css('border-color', '');
            }

            if (mobile == '') {
                $('#<%=txtMobile.ClientID%>').css('border-color', 'red');
                msg += "Mobile No., ";
                blank = true;
            } else {
                $('#<%=txtMobile.ClientID%>').css('border-color', '');
            }

            if (email == '') {
                $('#<%=txtEmail.ClientID%>').css('border-color', 'red');
                msg += "Email address, ";
                blank = true;
            }
            else {
                var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                if (!EmailText.test(email)) {
                    $('#<%=txtEmail.ClientID%>').css('border-color', 'red');
                    msg += "Invalid email address, ";
                    blank = true;
                }
                else {
                    $('#<%=txtEmail.ClientID%>').css('border-color', '');
                }
            }

            if (country == '0') {
                $('#<%=ddlCountry.ClientID%>').css('border-color', 'red');
                msg += "Country, ";
                blank = true;
            } else {
                $('#<%=ddlCountry.ClientID%>').css('border-color', '');
            }

           <%-- if (address == '') {
                $('#<%=txtAddress.ClientID%>').css('border-color', 'red');
                msg += "Address, ";
                blank = true;
            } else {
                $('#<%=txtAddress.ClientID%>').css('border-color', '');
            }--%>

            if (!ExistingClient && !ByADistributor && !Other) {
                msg += "Choose referral, ";
                blank = true;
            }

            if (Other) {
                if (OtherPleaseSpecify == '') {
                    $('#<%=txtOtherPleaseSpecify.ClientID%>').css('border-color', 'red');
                    msg += "Enter referral comment, ";
                    blank = true;
                } else {
                    $('#<%=txtOtherPleaseSpecify.ClientID%>').css('border-color', '');
                }
            }

            if (!Investor && !DistributorAgent && !ServiceProvider) {
                msg += "Choose nature of relationship, ";
                blank = true;
            }

            if (Investor) {
                if (!Individual && !NonIndividual) {
                    msg += "Choose investor, ";
                    blank = true
                }
            }

            if (ServiceProvider) {
                if (NatureOfRelationship == '') {
                    $('#<%=txtNatureOfRelationship.ClientID%>').css('border-color', 'red');
                    msg += "Enter service provider comment, ";
                    blank = true;
                } else {
                    $('#<%=txtNatureOfRelationship.ClientID%>').css('border-color', '');
                }
            }

            if (!USPerson && !NonUSPerson) {
                msg += "Choose type of person, ";
                blank = true;
            }

            if (USPerson) {
                if (!USPersonYes && !USPersonNo) {
                    msg += "Choose US Person type, ";
                    blank = true
                }
            }

            if (Captcha == '') {
                $('#<%=txtCaptcha.ClientID%>').css('border-color', 'red');
                msg += "Captcha, ";
                blank = true;
            } else {
                $('#<%=txtCaptcha.ClientID%>').css('border-color', '');
            }

            if (!Confirm) {
                msg += "Select terms & condition, ";
                blank = true
            }

            if (msg != "") {
                msg = msg.substring(0, msg.length - 2);
                $('#lblmsg').text(msg);
                $('.alert').slideDown('slow');
            } else {
                $('.alert').hide('slow');
            }

            if (blank) {
                $("html, body").animate({
                    scrollTop: 350
                }, 1500);
                return false;
            } else {
                return true;
            }
        }


        function isNumberKey(evt) {

            var charCode = (evt.which) ? evt.which : event.keyCode;
            var textBox = document.getElementById('<%=txtMobile.ClientID%>').value.length;
            console.log(textBox)
            if (charCode > 31 && (charCode < 48 || charCode > 57) || textBox > 11)
                return false;

            return true;
        }

        function isNumberKey1(evt) {

            var charCode = (evt.which) ? evt.which : event.keyCode;
            var textBox = document.getElementById('<%=txtCaptcha.ClientID%>').value.length;
            console.log(textBox)
            if (charCode > 31 && (charCode < 48 || charCode > 57) || textBox > 5)
                return false;

            return true;
        }

        $(".close").click(function () {
            $('.alert').slideUp('slow');
        });

        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }


    </script>

    <style>
        .ErrorValidation {
            color: #dc3545 !important;
            margin-bottom: 10px;
        }
    </style>
</asp:Content>

