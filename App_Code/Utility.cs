﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for Utility
/// </summary>
public class Utility
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
    public Utility()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable Display(string sql)
    {
        SqlDataAdapter da = new SqlDataAdapter(sql, con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
    }

    public DataTable Display(SqlCommand cmd)
    {
        cmd.Connection = con;
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
    }

    public bool Execute(SqlCommand cmd)
    {
        try
        {
            cmd.Connection = con;
            con.Open();
            int n = cmd.ExecuteNonQuery();

            return (n > 0);
        }
        catch (Exception ex)
        {
            return false;
        }
        finally
        {
            if (con != null && con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataSet Display1(string query) //using overloading methord
    {
        try
        {
            con.Open();

            SqlDataAdapter da = new SqlDataAdapter(query, con); //get data into adapter

            DataSet ds = new DataSet();
            da.Fill(ds);    //store into table

            con.Close();
            return ds;
        }
        catch (Exception ex)
        {

            return null;
            //throw ex;
        }
        finally
        {
            con.Close();
        }
    }

    #region Send Email
    public bool SendEmail(string Emailbody, string[] Toemailids, string Subject, string FilePath, Stream input)
    {
        bool result = false;
        #region Send Mail
        try
        {
            string EmailUserName = ConfigurationManager.AppSettings["EmailUsername"].ToString();
            string EmailPassword = ConfigurationManager.AppSettings["EmailPassword"].ToString();
            string EmailHost = ConfigurationManager.AppSettings["EmailHost"].ToString();
            string EmailPort = ConfigurationManager.AppSettings["EmailPort"].ToString();
            bool EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"].ToString());
            string subject = Subject;

            string ToEmailid = string.Empty;
            for (int i = 0; i < Toemailids.Length; i++)
            {
                ToEmailid += Toemailids[i] + ",";
            }
            if (ToEmailid != "")
            {
                ToEmailid = ToEmailid.Substring(0, ToEmailid.Length - 1);
            }

            string toEmail = ToEmailid;
            string body = Emailbody;


            System.Net.Mail.MailMessage Msg = new System.Net.Mail.MailMessage();
            // Sender e-mail address.
            Msg.From = new MailAddress(EmailUserName, "Alchemy Singapore");
            // Recipient e-mail address.
            Msg.To.Add(toEmail);
            Msg.Subject = subject;
            Msg.Body = body;
            Msg.IsBodyHtml = true;

            //file upload
            if (FilePath != "")
            {
                Attachment attach = new Attachment(input, FilePath);
                Msg.Attachments.Add(attach);
            }


            // your remote SMTP server IP.
            SmtpClient smtp = new SmtpClient();

            smtp.Host = EmailHost;

            smtp.Port = int.Parse(EmailPort);
            // smtp.Port = 25;
            smtp.Credentials = new System.Net.NetworkCredential(EmailUserName, EmailPassword);

            smtp.EnableSsl = EnableSsl;
            //smtp.UseDefaultCredentials = false;

            smtp.Send(Msg);
            // strSuccess = "Success";

            result = true; //success
        }
        catch (Exception ex)
        {

        }

        return result;
        #endregion
    }
    #endregion

    public string Slugify(string phrase, int maxLength)
    {
        string str = RemoveAccent(phrase).ToLower();

        str = Regex.Replace(str, @"[^a-z0-9\s-]", "");                      // REMOVE INVALID CHARS
        str = Regex.Replace(str, @"\s+", " ").Trim();                       // CONVERT MULTIPLE SPACES INTO ONE SPACE
        str = str.Substring(0, str.Length <= maxLength ? str.Length : maxLength).Trim();  // CUT AND TRIM
        str = Regex.Replace(str, @"\s", "-");                               // CONVERT SPACE INTO HYPHEN

        return str;
    }

    public string Slugify(string phrase)
    {
        int maxLength = 200;
        return Slugify(phrase, maxLength);
    }

    public string RemoveAccent(string txt)
    {
        byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
        return System.Text.Encoding.ASCII.GetString(bytes);
    }

    public void FillDropDownList(DropDownList ddl, string sql, string text, string value)
    {
        ddl.DataSource = Display(sql);
        ddl.DataTextField = text;
        ddl.DataValueField = value;
        ddl.DataBind();
    }

    public void FillRadioList(RadioButtonList ddl, string sql, string text, string value)
    {
        ddl.DataSource = Display(sql);
        ddl.DataTextField = text;
        ddl.DataValueField = value;
        ddl.DataBind();
    }

    public void FillCheckBoxList(CheckBoxList chk, string sql, string text, string value)
    {
        chk.DataSource = Display(sql);
        chk.DataTextField = text;
        chk.DataValueField = value;
        chk.DataBind();
    }

    public DataTable Search(string table, string field, string term)
    {
        string SEARCH_TEXT = "SELECT * FROM {0} WHERE {1} LIKE @term";

        string FINAL_QUERY = string.Format(SEARCH_TEXT, table, field);
        using (SqlCommand cmd = new SqlCommand(FINAL_QUERY))
        {
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.AddWithValue("@term", "%" + term + "%");
            return Display(cmd);
        }
    }

    public DataTable Search(string table, string field, string term, string order_by)
    {
        string SEARCH_TEXT = "SELECT * FROM {0} WHERE {1} LIKE @term ORDER BY {2}";

        string FINAL_QUERY = string.Format(SEARCH_TEXT, table, field, order_by);
        using (SqlCommand cmd = new SqlCommand(FINAL_QUERY))
        {
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.AddWithValue("@term", "%" + term + "%");
            return Display(cmd);
        }
    }

    public string GetUniqueName(string path, string initial, string ext, System.Web.UI.Page page, bool returnExtension)
    {
        //string uniquePart = Guid.NewGuid().ToString().Substring(0, 18);
        string uniquePart = DateTime.Now.ToFileTime().ToString().Substring(0, 18);
        string filename = string.Format("{0}{1}-{2}{3}", path, initial, uniquePart, ext);
        while (System.IO.File.Exists(page.Server.MapPath(filename)))
        {
            //uniquePart = Guid.NewGuid().ToString().Substring(0, 18);
            uniquePart = DateTime.Now.ToFileTime().ToString().Substring(0, 18);
            filename = string.Format("{0}{1}-{2}{3}", path, initial, uniquePart, ext);
        }
        if (returnExtension)
        {
            return string.Format("{0}-{1}{2}", initial, uniquePart, ext);
        }
        else
        {
            return string.Format("{0}-{1}", initial, uniquePart);
        }
    }

    public string GetUniqueName(string path, string initial, string ext, System.Web.UI.Page page)
    {
        return GetUniqueName(path, initial, ext, page, true);
    }

    public bool IsNumeric(string strToCheck)
    {
        return Regex.IsMatch(strToCheck, "^\\d+(\\.\\d+)?$");
    }

    public bool IsValidImageFileExtension(string extension)
    {
        return (extension == ".jpg" || extension == ".jpeg" || extension == ".png" || extension == ".bmp");
    }
    public bool IsValidPDFFileExtension(string extension)
    {
        return (extension == ".pdf");
    }
    public bool IsValidResumeFileExtension(string extension)
    {
        return (extension == ".pdf" || extension == ".doc" || extension == ".docx");
    }
    public bool IsValidPPTFileExtension(string extension)
    {
        return (extension == ".ppt" || extension == ".pptx");
    }
    public bool IsValidExcelFileExtension(string extension)
    {
        return (extension == ".xls" || extension == ".xlsx");
    }

}