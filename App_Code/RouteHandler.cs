﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Compilation;
using System.Web.Routing;
using System.Web.UI;

/// <summary>
/// Summary description for RouteHandler
/// </summary>
public class RouteHandler : IRouteHandler
{
    string _virtualPath;
 
    public RouteHandler(string virtualPath)
    {
        _virtualPath = virtualPath;
    }
    public IHttpHandler GetHttpHandler(RequestContext requestContext)
    {
        HttpContext.Current.Items["RouteData"] = requestContext.RouteData;
        return BuildManager.CreateInstanceFromVirtualPath(_virtualPath, typeof(Page)) as Page;

        //HttpContext.Current.Items("requestContext") = requestContext;
        //return BuildManager.CreateInstanceFromVirtualPath(_virtualPath, typeof(Page)) as IDisplay;
        //foreach (var value in requestContext.RouteData.Values)
        //{
        //    requestContext.HttpContext.Items[value.Key] = value.Value;
        //}
        //return (Page)BuildManager.CreateInstanceFromVirtualPath(_virtualPath, typeof(Page));
    }
}