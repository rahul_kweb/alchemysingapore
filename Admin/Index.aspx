﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Admin_Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link href="../Content/Admin/Resource/Css/AdminStyle.css" rel="stylesheet" />
<div class="right_col" role="main" style="min-height: 1381px;">
    <div class="">
        <h2 style="align-content:center;">Welcome to Alchemy Admin Panel</h2>
        <div class="row top_tiles" style="display:none">
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a href="CurrentOpening.aspx">
                    <div class="tile-stats">
                        <div class="count">
                            <asp:Literal ID="ltrGetCurrentOpening" runat="server"></asp:Literal>
                        </div>
                        <h6>Current Opening</h6>
                    </div>
                </a>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a href="ApplyForJob.aspx">
                    <div class="tile-stats">
                        <div class="count">
                            <asp:Literal ID="ltrGetApplyingForJob" runat="server"></asp:Literal>
                        </div>
                        <h6>Applying For Job</h6>
                    </div>
                </a>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a href="ContactUs.aspx">
                    <div class="tile-stats">
                        <div class="count">
                             <asp:Literal ID="ltrGetContactUs" runat="server"></asp:Literal>
                        </div>
                        <h6>Contact-Us</h6>
                    </div>
                </a>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a href="RequestACallBack.aspx">
                    <div class="tile-stats">
                        <div class="count">
                             <asp:Literal ID="ltrGetRequestCallback" runat="server"></asp:Literal>
                        </div>
                        <h6>Request A Callback</h6>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
</asp:Content>

