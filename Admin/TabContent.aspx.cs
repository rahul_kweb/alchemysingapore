﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class Admin_TabContent : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            if (Request.QueryString["TabId"] != null && Request.QueryString["TabId"] != "")
            {
                int testInt;
                if (int.TryParse(Request.QueryString["TabId"], out testInt))
                {
                    hdnTabId.Value = Request.QueryString["TabId"];

                    BindContent();

                }
            }
        }
    }

    public void BindContent()
    {
        DataSet ds = new DataSet();
        ds = utility.Display1("Execute Proc_TabContent 'get',0,'" + hdnTabId.Value + "'");
        if (ds.Tables.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.DataSource = ds;
            gdView.DataBind();

            if (ds.Tables[1].Rows.Count > 0)
            {
                hdnPreviousPage.Value = "TabMaster.aspx?pageId=" + ds.Tables[1].Rows[0]["PageId"].ToString();
                if (ds.Tables[1].Rows[0]["PageId"].ToString() == "1")
                {
                    lblPageName.Text = "About US - " + ds.Tables[1].Rows[0]["Title"].ToString();
                }
                else if(ds.Tables[1].Rows[0]["PageId"].ToString() == "2")
                {
                    lblPageName.Text = "Our Product - " + ds.Tables[1].Rows[0]["Title"].ToString();

                }

            }
            else
            {
                hdnPreviousPage.Value = "TabMaster.aspx?pageId=1";
            }
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }



    protected void btnsumbit_Click(object sender, EventArgs e)
    {

        if (btnsumbit.Text == "Save")
        {
            if (CheckSave())
            {

                using (SqlCommand cmd = new SqlCommand("Proc_TabContent"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "add");
                    cmd.Parameters.AddWithValue("@TabId", hdnTabId.Value);
                    cmd.Parameters.AddWithValue("@Title", txtTitle.Text.Trim());
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindContent();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {
                using (SqlCommand cmd = new SqlCommand("Proc_TabContent"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "update");
                    cmd.Parameters.AddWithValue("@TabId", hdnTabId.Value);
                    cmd.Parameters.AddWithValue("@ContentId", hdnContentId.Value);
                    cmd.Parameters.AddWithValue("@Title", txtTitle.Text.Trim());
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindContent();
                        MyMessageBox1.ShowSuccess("Successfully updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;



        if (txtTitle.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "Title, ";
        }


        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;



        if (txtTitle.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "Title, ";
        }

        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    private void Reset()
    {

        hdnContentId.Value = string.Empty;
        txtTitle.Text = string.Empty;
        txtDescription.Text = string.Empty;
        btnsumbit.Text = "Save";

    }


    protected void btnreset_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_TabContent"))
        {
            int Id = (int)gdView.DataKeys[e.RowIndex].Value;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "delete");
            cmd.Parameters.AddWithValue("@ContentId", Id);
            if (utility.Execute(cmd))
            {
                Reset();
                BindContent();
                MyMessageBox1.ShowSuccess("Delete Successfully");
            }
            else
            {
                MyMessageBox1.ShowWarning("Unable to Delete");
            }
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            int ContentId = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);
            dt = utility.Display("Exec Proc_TabContent 'getbyId','" + ContentId + "'");
            if (dt.Rows.Count > 0)
            {
                hdnContentId.Value = dt.Rows[0]["ContentId"].ToString();
                txtTitle.Text = dt.Rows[0]["Title"].ToString();
                txtDescription.Text = dt.Rows[0]["Description"].ToString();

                btnsumbit.Text = "Update";

            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindContent();
    }

}