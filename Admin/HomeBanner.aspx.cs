﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_HomeBanner : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }



    private void BindGrid()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = utility.Display("Execute Proc_Banner 'get'");

            if (dt.Rows.Count > 0)
            {
                gdView.Columns[0].Visible = false;
                gdView.DataSource = dt;
                gdView.DataBind();
                //lblmsg.Visible = false;

            }
            else
            {
                gdView.DataSource = null;
                gdView.DataBind();
                //lblmsg.Visible = true;

            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Some error occurred, While fetching records.");
        }
    }

    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/Content/uploads/HomeBanner/";

        if (btnsumbit.Text == "Save")
        {
            if (CheckSave())
            {
                if (fileUploadImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Banner-", ext, this, false);
                    fileUploadImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_Banner"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "add");
                    cmd.Parameters.AddWithValue("@Image", MainImage + ext);
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@DisplayOrder", txtDisplayOrder.Text.Trim());


                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {

                if (fileUploadImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Banner-", ext, this, false);
                    fileUploadImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_Banner"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "update");
                    cmd.Parameters.AddWithValue("@BannerId", hdnId.Value);
                    cmd.Parameters.AddWithValue("@Image", MainImage + ext);
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@DisplayOrder", txtDisplayOrder.Text.Trim());

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

        if (!fileUploadImage.HasFile)
        {
            message = "Banner Image, ";
            isOK = false;
        }


        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }

        if (txtDisplayOrder.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Display Order, ";
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;



        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "Description, ";
        }

        if (txtDisplayOrder.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "DisplayOrder, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    protected void btnreset_Click(object sender, EventArgs e)
    {
        Reset();
    }

    private void Reset()
    {

        hdnId.Value = string.Empty;
        txtDescription.Text = string.Empty;
        txtDisplayOrder.Text = string.Empty;
        btnsumbit.Text = "Save";
        ImagePreview.Visible = false;

    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("Proc_Banner"))
            {
                int Id = Convert.ToInt32(gdView.DataKeys[e.RowIndex].Values[0]);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "DELETE");
                cmd.Parameters.AddWithValue("@BannerId", Id);
                utility.Execute(cmd);
                BindGrid();
                MyMessageBox1.ShowSuccess("Record Deleted Successfully.");
            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Unable to delete record.");
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {

            int Id = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);
            DataTable dt = new DataTable();
            dt = utility.Display("Exec Proc_Banner 'getbyId','" + Id + "'");
            hdnId.Value = dt.Rows[0]["BannerId"].ToString();
            txtDescription.Text = dt.Rows[0]["Description"].ToString();
            txtDisplayOrder.Text = dt.Rows[0]["DisplayOrder"].ToString();
            ImagePreview.Visible = true;
            ImagePreview.ImageUrl = string.Format("~/Content/uploads/HomeBanner/" + dt.Rows[0]["Image"].ToString());

            btnsumbit.Text = "Update";

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }



}