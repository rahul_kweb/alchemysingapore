﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="CMS.aspx.cs" Inherits="Admin_CMS" %>

<%@ Register Src="~/Admin/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                <asp:Label ID="lblHeading" style="color: #73879B" runat="server"></asp:Label></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
                            <br>
                            <div id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">


                                <div class="form-group">
                                    <asp:HiddenField ID="hdnId" runat="server" />
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Description <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12" id="ckid" runat="server">
                                        <%-- <asp:TextBox ID="txtDescription" CssClass="form-control col-md-7 col-xs-12" runat="server"></asp:TextBox>--%>
                                        <CKEditor:CKEditorControl ID="txtDescription" runat="server"></CKEditor:CKEditorControl>
                                    </div>
                                </div>


                                <div class="ln_solid"></div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-success StoreDDLValueinhihhenfields" OnClick="btnUpdate_Click" />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>

