﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_RequestACallBack : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRequestACallBack();
        }
    }


    public void BindRequestACallBack()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec stp_RequestACallBack 'get'");
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.DataSource = dt;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }


    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindRequestACallBack();
           if (check1())
        {
            GetFilterDatafromDatabase();
        }
    }



    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int Id = (int)gdView.DataKeys[e.RowIndex].Value;
        using (SqlCommand cmd = new SqlCommand("update RequestACallBack set IsActive=0 where Id='" + Id + "'"))
        {
            
            if (utility.Execute(cmd))
            {
                BindRequestACallBack();
                MyMessageBox1.ShowSuccess("Delete Successfully");
            }
            else
            {
                MyMessageBox1.ShowWarning("Unable to Delete");
            }
        }
    }


    protected void btnExport_Click(object sender, EventArgs e)
    {

        if (check())
        {

            using (XLWorkbook wb = new XLWorkbook())
            {
                DataSet ds = new DataSet();
                ds = GetDatafromDatabase();
                wb.Worksheets.Add(ds);
                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                wb.Style.Font.Bold = true;

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= RequestACallBack.xlsx");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
               
            }
        }
    }
    protected DataSet GetDatafromDatabase()
    {
        DataSet ds = new DataSet();
        string postDatefrom = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
        string postDateto = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");

        ds = utility.Display1("execute stp_RequestACallBack 'GetForExportToExcel','0','','','','','','','','','','','','','','" + postDatefrom + "','" + postDateto + "'");
        return ds;
    }

    public bool check()
    {
        bool isOK = true;
        string message = string.Empty;
        if (txtFrom.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "From Date, ";
        }

        if (txtTo.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "To Date, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    protected void btnDirectExport_Click(object sender, EventArgs e)
    {
            using (XLWorkbook wb = new XLWorkbook())
            {
                DataSet ds = new DataSet();
                ds = utility.Display1("Execute stp_RequestACallBack 'GetForExportToExcelDirect'");
                wb.Worksheets.Add(ds);
                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                wb.Style.Font.Bold = true;

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= RequestACallBack.xlsx");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }
    
    
    protected void btnFilterGrid_Click1(object sender, EventArgs e)
    {
        if (check())
        {
            GetFilterDatafromDatabase();

        }
    }

    protected void GetFilterDatafromDatabase()
    {
        //DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        string postDatefrom = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
        string postDateto = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");

        ds = utility.Display1("execute stp_RequestACallBack 'GetForExportToExcel','0','','','','','','','','','','','','','','" + postDatefrom + "','" + postDateto + "'");
        if (ds.Tables.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.DataSource = ds;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }
    public bool check1()
    {
        bool isOK = true;
        string message = string.Empty;
        if (txtFrom.Text.Trim().Equals(string.Empty))
        {
            isOK = false;

        }

        if (txtTo.Text.Trim().Equals(string.Empty))
        {
            isOK = false;

        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {

        }
        return isOK;
    }
    
}