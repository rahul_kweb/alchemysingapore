﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="RequestACallBack.aspx.cs" Inherits="Admin_RequestACallBack" %>

<%@ Register Src="~/Admin/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Request A CallBack</h3>
                </div>

                <div class="title_right">
                    <div class="col-xs-12 form-group text-right ">
                        <asp:Button ID="btnDirectExport" Text="Export All to Excel" runat="server" CssClass="fa-hover  btn btn-success exporttoexcel" OnClick="btnDirectExport_Click" />

                    </div>
                    <div class="col-xs-12 text-right ">

                      <div class="row">
                            <div class="col-xs-3 form-group">
                                <asp:TextBox ID="txtFrom" placeholder="From" runat="server" autocomplete="off" CssClass="calender form-control"></asp:TextBox>
                            </div>

                            <div class="col-xs-3 form-group">
                                <asp:TextBox ID="txtTo" placeholder="To" runat="server" autocomplete="off" CssClass="calender form-control"></asp:TextBox>
                            </div>

                            <%--     <div class="col-xs-4 form-group">
                                <asp:Button ID="btnExport" Text="Export to Excel" runat="server" CssClass="fa-hover  btn btn-success exporttoexcel" OnClick="btnExport_Click" /></div>--%>

                            <div class="col-xs-3 form-group">
                                <asp:Button ID="btnFilterGrid" Text="Filter Data" runat="server" CssClass="fa-hover  btn btn-success exporttoexcel" OnClick="btnFilterGrid_Click1" />
                            </div>

                            <div class="col-xs-3 form-group">
                                <asp:Button ID="btnExport" Text="Export to Excel" runat="server" CssClass="fa-hover  btn btn-success exporttoexcel" OnClick="btnExport_Click" />
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
                            <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
                                OnRowDeleting="gdView_RowDeleting" DataKeyNames="Id"
                                class="table table-striped table-bordered" PageSize="10" AllowPaging="true">
                                <Columns>
                                    <asp:BoundField DataField="Id" HeaderText="Id" />
                                    <asp:BoundField DataField="Sr" HeaderText="Sr No." />
                                    <asp:BoundField DataField="Name" HeaderText="Name" />
                                    <asp:BoundField DataField="Email" HeaderText="Email" />
                                    <asp:BoundField DataField="PhoneNo" HeaderText="Phone No." />
                                    <asp:BoundField DataField="Location" HeaderText="Location" />
                                    <asp:BoundField DataField="Message" HeaderText="Message" />
                                    <asp:BoundField DataField="Date" HeaderText="Date" />
                                    <asp:BoundField DataField="Time" HeaderText="Time" />
                                    <asp:BoundField DataField="utm_source" HeaderText="utm_source" />
                                    <asp:BoundField DataField="utm_medium" HeaderText="utm_medium" />
                                    <asp:BoundField DataField="utm_campaign" HeaderText="utm_campaign" />
                                    <asp:BoundField DataField="utm_device" HeaderText="utm_device" />
                                    <asp:BoundField DataField="utm_content" HeaderText="utm_content" />
                                    <asp:BoundField DataField="utm_term" HeaderText="utm_term" />
                                    <asp:BoundField DataField="Referrer_Website_SEO" HeaderText="Referrer (Website & SEO)" />
                                <%--    <asp:BoundField DataField="Interested" HeaderText="I'm Intrested" />
                                    <asp:BoundField DataField="Date" HeaderText="Date" />
                                    
                                    <asp:BoundField DataField="Request_From_Pop_Up" HeaderText="Request from Ascent Pop-up" />--%>


                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                                                CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <%-- <PagerStyle CssClass="pagination"></PagerStyle>--%>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%--  --%>
    </div>
    <script>
        $(function () {
            $('.calender').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-mm-yy'
            });
        });
    </script>

    <style>
        /* MODIFIED DATE PICKER */
        .ui-datepicker {
            width: 250px !important;
            padding: 2px !important;
            font-size: 14px !important;
        }

            .ui-datepicker table {
                margin: 0 !important;
            }

            .ui-datepicker .ui-widget-header {
                background: #04869a !important;
                /*color: #fff;*/
                font-weight: normal;
            }

        .ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
            background: #04869a !important;
            color: #fff !important;
            border: 1px solid #04869a !important;
        }

        .ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {
            margin: 0px 4px !important;
            font-size: 13px !important;
        }

        .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
            background: #04869a !important;
            color: #fff !important;
            border: 1px solid #04869a !important;
        }
    </style>
</asp:Content>

