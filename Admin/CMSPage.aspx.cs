﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_CMSPage : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["cmspage"] != null && Request.QueryString["cmspage"] != "")
        {
            int testInt;
            if (int.TryParse(Request.QueryString["cmspage"], out testInt))
            {
                hdnPageId.Value = Request.QueryString["cmspage"];
                if (!Page.IsPostBack)
                {
                    Bind();
                }
            }
        }
    }

    private void Bind()
    {
        DataSet ds = new DataSet();
        try
        {
            hdnPageId.Value = Request.QueryString["cmspage"].ToString();


            ds = utility.Display1("Execute Proc_PageCMSMaster 'get',0,'" + hdnPageId.Value + "'");
            if (ds.Tables.Count > 0)
            {

                

            lblHeading.Text = ds.Tables[1].Rows[0]["PageName"].ToString();

              

                if (ds.Tables[0].Rows.Count> 0)
                {

                    gdView.Columns[0].Visible = false;
                    gdView.DataSource = ds;
                    gdView.DataBind();


                }

                else
                {
                    gdView.DataSource = null;
                    gdView.DataBind();


                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/Content/uploads/Icon/";


        if (btnsumbit.Text == "Save")
        {
            if (CheckSave())
            {
                if (fileUploadIconImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadIconImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Icon-", ext, this, false);
                    fileUploadIconImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_PageCMSMaster"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "add");
                    cmd.Parameters.AddWithValue("@PageId", hdnPageId.Value);
                    cmd.Parameters.AddWithValue("@Title", txtTitle.Text);
                    cmd.Parameters.AddWithValue("@Icon", MainImage + ext);
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text);
                    cmd.Parameters.AddWithValue("@DisplayOrder", txtDisplayOrder.Text);
                    if (utility.Execute(cmd))
                    {
                        Reset();
                        Bind();
                        MyMessageBox1.ShowSuccess("Successfully saved");
                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }

        else
        {
            if (CheckUpdate())
            {

                if (fileUploadIconImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadIconImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Icon-", ext, this, false);
                    fileUploadIconImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_PageCMSMaster"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "update");
                    cmd.Parameters.AddWithValue("@CmsId", hdnCmsId.Value);
                    cmd.Parameters.AddWithValue("@PageId", hdnPageId.Value);
                    cmd.Parameters.AddWithValue("@Title", txtTitle.Text);
                    cmd.Parameters.AddWithValue("@Icon", MainImage + ext);
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text);
                    cmd.Parameters.AddWithValue("@DisplayOrder", txtDisplayOrder.Text);
                    if (utility.Execute(cmd))
                    {
                        Reset();
                        Bind();
                        MyMessageBox1.ShowSuccess("Successfully updated");
                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }

        }
    }

    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

        if (txtTitle.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "Title, ";
        }


        //if (!fileUploadIconImage.HasFile)
        //{
        //    message+= "Icon, ";
        //    isOK = false;
        //}

        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }

        if (txtDisplayOrder.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Display Order, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;


        if (txtTitle.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "Title, ";
        }

        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }

        if (txtDisplayOrder.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Display Order, ";
        }
        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }


    private void Reset()
    {
        txtTitle.Text = string.Empty;
        IconImagePreview.Visible = false;
        hdnCmsId.Value = string.Empty;
        txtDescription.Text = string.Empty;
        txtDisplayOrder.Text = string.Empty;
        btnsumbit.Text = "Save";

    }





    protected void btnreset_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        Bind();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_PageCMSMaster"))
        {
            int Id = (int)gdView.DataKeys[e.RowIndex].Value;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "delete");
            cmd.Parameters.AddWithValue("@PageId",hdnPageId.Value);
            cmd.Parameters.AddWithValue("@CmsId", Id);
            if (utility.Execute(cmd))
            {
                Reset();
                Bind();
                MyMessageBox1.ShowSuccess("Delete Successfully");
            }
            else
            {
                MyMessageBox1.ShowWarning("Unable to Delete");
            }
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            int Id = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);
            dt = utility.Display("Exec Proc_PageCMSMaster 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                hdnCmsId.Value = dt.Rows[0]["CmsId"].ToString();
                hdnPageId.Value = dt.Rows[0]["PageId"].ToString();
                txtTitle.Text = dt.Rows[0]["Title"].ToString();
                IconImagePreview.Visible = true;
                IconImagePreview.ImageUrl = string.Format("~/Content/uploads/Icon/" + dt.Rows[0]["Icon"].ToString());
                txtDescription.Text = dt.Rows[0]["Description"].ToString();
                txtDisplayOrder.Text = dt.Rows[0]["DisplayOrder"].ToString();

                btnsumbit.Text = "Update";

            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }
}