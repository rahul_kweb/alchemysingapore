﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_FundReportDetails : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            if (Request.QueryString["Id"] != null && Request.QueryString["Id"] != "")
            {
                int testInt;
                if (int.TryParse(Request.QueryString["Id"], out testInt))
                {
                    hdnId.Value = Request.QueryString["Id"];

                    BindPeopleCulture();

                }
            }
        }
    }


    public void BindPeopleCulture()
    {

        DataSet ds = new DataSet();
        ds = utility.Display1("Execute stp_FundReportDetails 'get',0,'" + hdnId.Value + "'");
        ltrCategory.Text = ds.Tables[1].Rows[0]["Title"].ToString();
        if (ds.Tables.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.DataSource = ds;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }
    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/Content/uploads/PDF/";

        if (btnsumbit.Text == "Save")
        {
            if (CheckSave())
            {
                if (fileUploadImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
                    if (!utility.IsValidPDFFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only pdf file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "pdf-", ext, this, false);
                    fileUploadImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("stp_FundReportDetails"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "add");
                    cmd.Parameters.AddWithValue("@Pdf", MainImage + ext);
                    cmd.Parameters.AddWithValue("@FundId", hdnId.Value);
                    cmd.Parameters.AddWithValue("@Heading", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@DisplayOrder", txtDisplayOrder.Text.Trim());

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindPeopleCulture();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {

                if (fileUploadImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
                    if (!utility.IsValidPDFFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only pdf file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "pdf-", ext, this, false);
                    fileUploadImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("stp_FundReportDetails"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "update");
                    cmd.Parameters.AddWithValue("@FDetailId", hdnPeopleCultureId.Value);
                    cmd.Parameters.AddWithValue("@Pdf", MainImage + ext);
                    cmd.Parameters.AddWithValue("@FundId", hdnId.Value);
                    cmd.Parameters.AddWithValue("@Heading", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@DisplayOrder", txtDisplayOrder.Text.Trim());

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindPeopleCulture();
                        MyMessageBox1.ShowSuccess("Successfully updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

        if (txtName.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Heading, ";
        }

        if (!fileUploadImage.HasFile)
        {
            message += "Pdf file, ";
            isOK = false;
        }


        if (txtDisplayOrder.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "DisplayOrder, ";
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;


        if (txtName.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Heading, ";
        }



        if (txtDisplayOrder.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Display Order, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    private void Reset()
    {

        hdnPeopleCultureId.Value = string.Empty;
        txtName.Text = string.Empty;
        txtDisplayOrder.Text = string.Empty;
        btnsumbit.Text = "Save";
        ImagePreview.Visible = false;

    }

    protected void btnreset_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindPeopleCulture();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("stp_FundReportDetails"))
        {
            int Id = (int)gdView.DataKeys[e.RowIndex].Value;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "delete");
            cmd.Parameters.AddWithValue("@FDetailId", Id);
            if (utility.Execute(cmd))
            {
                Reset();
                BindPeopleCulture();
                MyMessageBox1.ShowSuccess("Delete Successfully");
            }
            else
            {
                MyMessageBox1.ShowWarning("Unable to Delete");
            }
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            int Id = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);
            dt = utility.Display("Exec stp_FundReportDetails 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                hdnId.Value = dt.Rows[0]["FundId"].ToString();
                hdnPeopleCultureId.Value = dt.Rows[0]["FDetailId"].ToString();
                txtName.Text = dt.Rows[0]["Heading"].ToString();
                txtDisplayOrder.Text = dt.Rows[0]["DisplayOrder"].ToString();
                ImagePreview.Visible = true;
                ImagePreview.NavigateUrl = string.Format("~/Content/uploads/PDF/" + dt.Rows[0]["Pdf"].ToString());
                btnsumbit.Text = "Update";

            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    public string Generateslg(string strtext)
    {
        string str = strtext.ToString().ToLower();
        str = Regex.Replace(str, @"[^0-9a-zA-Z]+", "-");
        return str;
    }
}