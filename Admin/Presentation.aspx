﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Presentation.aspx.cs" Inherits="Admin_Presentation" %>
<%@ Register Src="~/Admin/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-9 col-sm-12 col-xs-12 col-md-offset-1">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                <asp:Label ID="lblHeading" runat="server" Style="color: #73879B"></asp:Label></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
                            <br />
                            <div id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

                                <asp:HiddenField ID="hdnId" runat="server" />
                                <asp:HiddenField ID="hdnItsId" runat="server" />
                      

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Heading <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtHeading" CssClass="form-control col-md-7 col-xs-12" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>



                                <div class="form-group" id="divPdf">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Pdf <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:FileUpload ID="fileUploadPdf" runat="server" CssClass="form-control col-md-7 col-xs-12" />
                                        <asp:HyperLink ID="PdfPreview" runat="server" Text="View Pdf" Target="_blank"></asp:HyperLink>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Visible <span class="required" style="color: red"></span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 15px;">
                                        <asp:CheckBox ID="ckstatus" CssClass="checkmark" runat="server"  />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <asp:Button ID="btnsumbit" type="Save" Text="Save" value="Submit" class="btn btn-success StoreDDLValueinhihhenfields" title="Submit" runat="server" OnClick="btnsumbit_Click" />
                                        <asp:Button ID="btnreset" value="Cancel" Text="Cancel" class="btn btn-primary" title="Cancel" runat="server" OnClick="btnreset_Click" />
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
                                 OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging" DataKeyNames="Id"
                                class="table table-striped table-bordered" PageSize="10" AllowPaging="true">

                                <Columns>
                                    <asp:BoundField DataField="Id" HeaderText="Id" />
                                    <asp:BoundField DataField="Sr" HeaderText="Sr No." />
                                    <asp:BoundField DataField="Heading" HeaderText="Heading" />

                                    <asp:TemplateField HeaderText="Pdf/Url" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                         <a href='<%# Eval("Pdf", "../Content/uploads/presentation/{0}") %>' target="_blank">View</a>
                                            <br />
                                     <p> <%# Request.ServerVariables["SERVER_NAME"] +""+ Eval("Pdf","/Content/uploads/presentation/{0}").ToString() %></p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>                                          
                                           <img src='<%#Eval("Status").ToString()=="1"?"/Content/uploads/checked.jpg":"/Content/uploads/button-cancel.png"%>' style="width:25px;height:25px"/>      
                                         <%--   <img src="/Content/uploads/checked.jpg" />     --%>                            
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton2" CssClass="btn" runat="server" CausesValidation="False"
                                                CommandName="Select">
                            <i class="icon-edit"></i> Edit
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                                                CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <%--<PagerStyle CssClass="pagination"></PagerStyle>--%>
                            </asp:GridView>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>
</asp:Content>


