﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class Admin_Presentation : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindGrid();
        
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_Presentation 'get'");
        if (dt.Rows.Count > 0)
        {
            //lblHeading.Text = dt.Rows[0]["Heading"].ToString();
            //ViewBag.Id = ds.Tables[1].Rows[0]["Id"].ToString();
            gdView.Columns[0].Visible = false;
            gdView.DataSource = dt;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();


        }
    }

    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/Content/uploads/presentation/";

        if (btnsumbit.Text == "Save")
        {
            if (CheckSave())
            {
                if (fileUploadPdf.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadPdf.FileName).ToLower();
                    if (!utility.IsValidPDFFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Pdf file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Pdf-", ext, this, false);
                    fileUploadPdf.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }
                using (SqlCommand cmd = new SqlCommand("Proc_Presentation"))
                {
                    int val;
                    if (ckstatus.Checked)
                    {
                        val = 1;
                    }
                    else
                    {
                        val = 0;
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "add");
                    cmd.Parameters.AddWithValue("@Heading", txtHeading.Text.Trim());
                    cmd.Parameters.AddWithValue("@Pdf", MainImage + ext);
                    cmd.Parameters.AddWithValue("@Status", val);

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {

                if (fileUploadPdf.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadPdf.FileName).ToLower();
                    if (!utility.IsValidPDFFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Pdf file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Pdf-", ext, this, false);
                    fileUploadPdf.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_Presentation"))
                {

                    int val;
                    if (ckstatus.Checked)
                    {
                        val = 1;
                    }
                    else
                    {
                        val = 0;
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "update");
                    cmd.Parameters.AddWithValue("@Id", hdnId.Value);
                    cmd.Parameters.AddWithValue("@Heading", txtHeading.Text.Trim());
                    cmd.Parameters.AddWithValue("@Pdf", MainImage + ext);
                    cmd.Parameters.AddWithValue("@Status", val);

                    if (utility.Execute(cmd))
                    {

                        Reset();
                        BindGrid();

                        MyMessageBox1.ShowSuccess("Successfully updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }

    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;



        if (txtHeading.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Heading, ";
        }

 
                if (!fileUploadPdf.HasFile)
                {
                    message += "pdf,";
                    isOK = false;
                }
     



        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;

 
        if (txtHeading.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Heading, ";
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    private void Reset()
    {

        hdnItsId.Value = string.Empty;
        txtHeading.Text = string.Empty;
        ckstatus.Checked = false;
        btnsumbit.Text = "Save";


    }
    protected void btnreset_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("Proc_Presentation"))
            {
                int Id = (int)gdView.DataKeys[e.RowIndex].Value;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "delete");
                cmd.Parameters.AddWithValue("@Id", Id);
                utility.Execute(cmd);
                BindGrid();
                MyMessageBox1.ShowSuccess("Record Deleted Successfully.");
            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Unable to delete record.");
        }

    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            int Id = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);

            DataTable dt = new DataTable();
            dt = utility.Display("Exec Proc_Presentation 'getbyId', '" + Id + "'");
            hdnId.Value = dt.Rows[0]["Id"].ToString();
            txtHeading.Text = dt.Rows[0]["Heading"].ToString();
            PdfPreview.NavigateUrl = string.Format("../Content/uploads/presentation/" + dt.Rows[0]["Pdf"].ToString());
            if (int.Parse(dt.Rows[0]["Status"].ToString()) == 1)
            {
                ckstatus.Checked = true;
            }
            else
            {
                ckstatus.Checked = false;
            }


            btnsumbit.Text = "Update";

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }

    }
}