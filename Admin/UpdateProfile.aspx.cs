﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_UpdateProfile : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        { 
        UpdateProfile();
        }
    }



    public void UpdateProfile()
    {
        DataTable dt = GetAdminDataForProfileUpdate();
        Login log = new Login();
        hdnid.Value = dt.Rows[0]["Id"].ToString();
        txtname.Text = dt.Rows[0]["Name"].ToString();
        txtemailid.Text= dt.Rows[0]["Emailid"].ToString();
        txtmobileno.Text= dt.Rows[0]["Mobileno"].ToString();
    }

    public DataTable GetAdminDataForProfileUpdate()
    {
        DataTable dt = new DataTable();
        try
        {
            dt = utility.Display("Execute Proc_SuperAdminLogin 'GETDETAIL_FOR_UPDATE'");
        }
        catch (Exception ex)
        {

            throw ex;
        }
        return dt;
    }


    protected void btnUpdateDetails_Click(object sender, EventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_SuperAdminLogin"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATEPROFILEDETAILS");
            cmd.Parameters.AddWithValue("@NAME", txtname.Text.ToString().Trim());
            cmd.Parameters.AddWithValue("@EMAILID", txtemailid.Text.ToString().Trim());
            cmd.Parameters.AddWithValue("@MOBILENO",txtmobileno.Text.ToString().Trim());
            cmd.Parameters.AddWithValue("@ID", hdnid.Value);
            if (utility.Execute(cmd))
            {
                lblstatus.Visible = true;
                lblstatus.Text = "Success";
                GetAdminDataForProfileUpdate();
            }
            else
            {
                lblstatus.Visible = true;
                lblstatus.Text = "Failed";
                GetAdminDataForProfileUpdate();
            }
        }

    }
}