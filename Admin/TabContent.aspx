﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="TabContent.aspx.cs" Inherits="Admin_TabContent" %>
<%@ Register Src="~/Admin/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><asp:Label ID="lblPageName" runat="server" style="color:#7387b6"></asp:Label></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
                            <br>
                            <div id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                                 <asp:HiddenField ID="hdnTabId" runat="server" />
                                     <asp:HiddenField ID="hdnContentId" runat="server" />

                                   <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Title  <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtTitle" CssClass="form-control col-md-7 col-xs-12" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Description <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                         <CKEditor:CKEditorControl ID="txtDescription" runat="server"  CssClass="form-control col-md-7 col-xs-12" ></CKEditor:CKEditorControl>
                                    </div>
                                </div>
                                  

                                <div class="ln_solid"></div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <asp:Button ID="btnsumbit" type="submit" Text="Save" value="Submit" class="btn btn-success StoreDDLValueinhihhenfields" title="Submit" runat="server" OnClick="btnsumbit_Click" />
                                        <asp:Button ID="btnreset" value="Cancel" Text="Cancel" class="btn btn-primary" title="Cancel" runat="server" OnClick="btnreset_Click" />
                                    <input type="button" value="Back" id="btnBack" class="btn btn-dark" title="Back">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
                                OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging" DataKeyNames="ContentId"
                                class="table table-striped table-bordered" PageSize="10" AllowPaging="true">
                                <Columns>
                                    <asp:BoundField DataField="ContentId" HeaderText="ContentId" />
                                     <asp:BoundField DataField="Sr" HeaderText="Sr No." />
                                  <asp:TemplateField HeaderText="Title">
                                        <ItemTemplate>
                                            <%#Eval("Title") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <%#Eval("Description")%>                                          
                                        </ItemTemplate>
                                    </asp:TemplateField>                                 
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton2" CssClass="btn" runat="server" CausesValidation="False"
                                                CommandName="Select">
                            <i class="icon-edit"></i> Edit
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                                                CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <%-- <PagerStyle CssClass="pagination"></PagerStyle>--%>
                            </asp:GridView>
                        </div>

                    </div>
                </div>
            </div>

        </div>
            <asp:HiddenField ID="hdnPreviousPage" runat="server" />

    </div>


        <style>
    .counter {
        width: 45px;
        border-radius: 0px !important;
        text-align: center;
    }

    .up_count {
        margin-bottom: 10px;
        margin-left: -4px;
        border-top-left-radius: 0px;
        border-bottom-left-radius: 0px;
    }

    .down_count {
        margin-bottom: 10px;
        margin-right: -4px;
        border-top-right-radius: 0px;
        border-bottom-right-radius: 0px;
    }
</style>

    <script>
        $(document).ready(function () {
            $('button').click(function (e) {
                var button_classes, value = +$('.counter').val();
                button_classes = $(e.currentTarget).prop('class');
                if (button_classes.indexOf('up_count') !== -1) {
                    value = (value) + 1;
                } else {
                    value = (value) - 1;
                }
                value = value < 0 ? 0 : value;
                $('.counter').val(value);
            });
            $('.counter').click(function () {
                $(this).focus().select();
            });
        });

        $('#btnBack').click(function () {
            window.location.href = $('#<%=hdnPreviousPage.ClientID%>').val();
        })
    </script>
</asp:Content>

