﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="InnerBanner.aspx.cs" Inherits="Admin_InnerBanner" %>

<%@ Register Src="~/Admin/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-9 col-sm-12 col-xs-12 col-md-offset-1">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Inner Banner</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
                            <br>
                            <asp:HiddenField ID="hdnId" runat="server" />
                            <div id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Page Name <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtPageName" CssClass="form-control col-md-7 col-xs-12" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Banner Image <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:FileUpload ID="fileUploadImage" CssClass="form-control col-md-7 col-xs-12" runat="server" />
                                        <asp:Image ID="ImagePreview" runat="server" Text="View Image" Visible="false" Target="_blank" Width="200" Height="150"></asp:Image>
                                    </div>
                                </div>

                                <div class="ln_solid"></div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <asp:Button ID="btnsumbit" type="submit" Text="Update" value="Submit" class="btn btn-success StoreDDLValueinhihhenfields" title="Submit" runat="server" OnClick="btnsumbit_Click" />
                                         <asp:Button ID="btnreset" type="reset" Text="Cancel" value="Reset" class="btn btn-primary" title="Reset" runat="server" OnClick="btnreset_Click" />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
                                OnSelectedIndexChanging="gdView_SelectedIndexChanging" DataKeyNames="Id"
                                class="table table-striped table-bordered" PageSize="10" AllowPaging="true">
                                <Columns>
                                    <asp:BoundField DataField="Id" HeaderText="Id" />
                                     <asp:BoundField DataField="Sr" HeaderText="Sr No." />
                                    <asp:BoundField DataField="PageName" HeaderText="Page Name" />
                                    <asp:TemplateField HeaderText="Banner" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <img src='<%# Eval("Image", "../Content/uploads/InnerBanner/{0}") %>' style="height: 90px;"></img>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton2" CssClass="btn" runat="server" CausesValidation="False"
                                                CommandName="Select">
                            <i class="icon-edit"></i> Edit
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pagination"></PagerStyle>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

