﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="MediaCenterPage.aspx.cs" Inherits="Admin_MediaCenterPage" %>

<%@ Register Src="~/Admin/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-9 col-sm-12 col-xs-12 col-md-offset-1">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                <asp:Label ID="lblHeading" runat="server" Style="color: #73879B"></asp:Label></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
                            <br />
                            <div id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

                                <asp:HiddenField ID="hdnId" runat="server" />
                                <asp:HiddenField ID="hdnItsId" runat="server" />

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Select Year <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <%--   <select name="year" class="form-control">
                                            @for (var i = 0; i < 21; i++)
                                            {

                                                if (Convert.ToInt32(Model.Year) == (DateTime.Now.AddYears(i).Year - 20))
                                                {
                                                    <option selected>@(DateTime.Now.AddYears(i).Year - 20)</option>
                                            }
                                                else
                                                {
                                                    <option>@(DateTime.Now.AddYears(i).Year - 20)</option>
                                            }

                                            }
                                        </select>--%>

                                        <asp:DropDownList ID="ddlYear" runat="server" class="form-control">
                                            <%--  <asp:ListItem Value=" " Selected="True" Text="Select Year"></asp:ListItem>--%>
                                        </asp:DropDownList>


                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Heading <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtHeading" CssClass="form-control col-md-7 col-xs-12" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Type <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control col-md-7 col-xs-12 type">
                                            <asp:ListItem Value="select Type" Text="Select Type"></asp:ListItem>
                                            <asp:ListItem Value="Pdf" Text="Pdf">Pdf</asp:ListItem>
                                            <asp:ListItem Value="Url" Text="Url">Url</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>


                                <div class="form-group" id="divPdf" style="display: none;">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Pdf <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:FileUpload ID="fileUploadPdf" runat="server" CssClass="form-control col-md-7 col-xs-12" />
                                        <asp:HyperLink ID="PdfPreview" runat="server" Text="View Pdf" Target="_blank"></asp:HyperLink>
                                    </div>
                                </div>

                                <div class="form-group" id="divUrl" style="display: none;">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Url <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtUrl" runat="server" CssClass="form-control col-md-7 col-xs-12" placeholder="https://www.google.co.in/" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Post Date <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtDate" autocomplete="off" CssClass="form-control col-md-7 col-xs-12 calender" runat="server"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Visible <span class="required" style="color: red"></span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 15px;">
                                        <asp:CheckBox ID="ckstatus" CssClass="checkmark" runat="server"  />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <asp:Button ID="btnsumbit" type="Save" Text="Save" value="Submit" class="btn btn-success StoreDDLValueinhihhenfields" title="Submit" runat="server" OnClick="btnsumbit_Click" />
                                        <asp:Button ID="btnreset" value="Cancel" Text="Cancel" class="btn btn-primary" title="Cancel" runat="server" OnClick="btnreset_Click" />
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
                                OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging" DataKeyNames="ItsId"
                                class="table table-striped table-bordered" PageSize="10" AllowPaging="true">

                                <Columns>
                                    <asp:BoundField DataField="ItsId" HeaderText="ItsId" />
                                    <asp:BoundField DataField="Sr" HeaderText="Sr No." />
                                    <asp:BoundField DataField="Year" HeaderText="Year" />
                                    <asp:BoundField DataField="Heading" HeaderText="Heading" />

                                    <asp:TemplateField HeaderText="Pdf/Url" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>

                                            <a href='<%#Eval("Type").ToString()=="Pdf"?Eval("Pdf","../Content/uploads/Pdf/{0}").ToString():Eval("Url") %>' target="_blank">View</a>
                                            <br />
                                            <%#Eval("Type").ToString()=="Pdf"?Request.ServerVariables["SERVER_NAME"] +""+ Eval("Pdf","/Content/uploads/Pdf/{0}").ToString():Eval("Url") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>                                          
                                           <img src='<%#Eval("Status").ToString()=="1"?"/Content/uploads/checked.jpg":"/Content/uploads/button-cancel.png"%>' style="width:25px;height:25px"/>      
                                         <%--   <img src="/Content/uploads/checked.jpg" />     --%>                            
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton2" CssClass="btn" runat="server" CausesValidation="False"
                                                CommandName="Select">
                            <i class="icon-edit"></i> Edit
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                                                CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <%--<PagerStyle CssClass="pagination"></PagerStyle>--%>
                            </asp:GridView>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>


    <script src="../Content/Admin/datepicker/jquery-ui.js" type="text/javascript"></script>




    <script>

        $(function () {
            $('.calender').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-mm-yy'
            });
        });



        var type = $('#<%=ddlType.ClientID%>').val();
        if (type == 'Pdf') {
            $('#divPdf').show();
            $('#divUrl').hide();
        }
        else if (type == 'Url') {
            $('#divUrl').show();
            $('#divPdf').hide();
        }
        else {
            $('#divUrl').hide();
            $('#divPdf').hide();
        }



        $(document).ready(function () {
            //var type = $('#<%=ddlType.ClientID%>').val();
            $('#<%=ddlType.ClientID%>').change(function () {
                var type = $('#<%=ddlType.ClientID%>').val();
                if (type == 'Pdf') {
                    $('#divPdf').show();
                    $('#divUrl').hide();
                }
                else if (type == 'Url') {
                    $('#divUrl').show();
                    $('#divPdf').hide();
                }
                else {
                    $('#divUrl').hide();
                    $('#divPdf').hide();
                }
            });
        });
    </script>


    <link href="../Content/Admin/datepicker/jquery-ui.css" rel="stylesheet" />


    <style>
        /* MODIFIED DATE PICKER */
        .ui-datepicker {
            width: 250px !important;
            padding: 2px !important;
            font-size: 14px !important;
        }

            .ui-datepicker table {
                margin: 0 !important;
            }

            .ui-datepicker .ui-widget-header {
                background: #04869a !important;
                /*color: #fff;*/
                font-weight: normal;
            }

        .ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
            background: #04869a !important;
            color: #fff !important;
            border: 1px solid #04869a !important;
        }

        .ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {
            margin: 0px 4px !important;
            font-size: 13px !important;
        }

        .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
            background: #04869a !important;
            color: #fff !important;
            border: 1px solid #04869a !important;
        }
    </style>
</asp:Content>

