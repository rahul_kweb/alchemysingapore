﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_MediaCenterPage : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["Id"] != null && Request.QueryString["Id"] != "")
        {
            int testInt;
            if (int.TryParse(Request.QueryString["Id"], out testInt))
            {
                hdnId.Value = Request.QueryString["Id"];
                if (!Page.IsPostBack)
                {
                    BindGrid();
                    BindDropdown();
                }
            }
        }
    }


    public void BindDropdown()
    {
        //for (int i = 0; i < 9; i++)
        //{
        //    if (ddlYear.Text == (DateTime.Now.AddYears(i).Year - 8).ToString())
        //    {
        //        ddlYear.Items.Insert(0, new ListItem((DateTime.Now.AddYears(i).Year - 8).ToString()));
        //    }
        //    else
        //    {
        //        ddlYear.Items.Add(new ListItem((DateTime.Now.AddYears(i).Year - 8).ToString()));
        //    }
        //}

        int currentYear = DateTime.Now.Year;
        ddlYear.Items.Insert(0, new ListItem("Select Year", "0"));
        for (int i = 2011; i <= currentYear; i++)
        {

            ddlYear.Items.Add(i.ToString());
        }

    }



    public void BindGrid()
    {
        DataSet ds = new DataSet();
        ds = utility.Display1("Execute Proc_Insights 'get',0,'" + hdnId.Value + "'");
        if (ds.Tables.Count > 0)
        {
            lblHeading.Text = ds.Tables[1].Rows[0]["Title"].ToString();
            //ViewBag.Id = ds.Tables[1].Rows[0]["Id"].ToString();
            gdView.Columns[0].Visible = false;
            gdView.DataSource = ds;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();


        }
    }

    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/Content/uploads/Pdf/";

        if (btnsumbit.Text == "Save")
        {
            if (CheckSave())
            {
                if (fileUploadPdf.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadPdf.FileName).ToLower();
                    if (!utility.IsValidPDFFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Pdf file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }
                                       
                    MainImage = utility.GetUniqueName(VirtualPart, "Pdf-", ext, this, false);
                    fileUploadPdf.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }
                string postDate = DateTime.ParseExact(txtDate.Text, "dd-MM-yyyy", null).ToString("yyyy/MM/dd");
                using (SqlCommand cmd = new SqlCommand("Proc_Insights"))
                {
                    int val;
                    if (ckstatus.Checked)
                    {
                        val = 1;
                    }
                    else
                    {
                        val = 0;
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "add");
                    cmd.Parameters.AddWithValue("@Id", hdnId.Value);
                    cmd.Parameters.AddWithValue("@Year", ddlYear.SelectedValue.Trim());
                    cmd.Parameters.AddWithValue("@Heading", txtHeading.Text.Trim());
                    cmd.Parameters.AddWithValue("@Pdf", MainImage + ext);
                    cmd.Parameters.AddWithValue("@Url", txtUrl.Text.Trim());
                    cmd.Parameters.AddWithValue("@PostDate", postDate);
                    cmd.Parameters.AddWithValue("@Type", ddlType.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@Status", val);

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {

                if (fileUploadPdf.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadPdf.FileName).ToLower();
                    if (!utility.IsValidPDFFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Pdf file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Pdf-", ext, this, false);
                    fileUploadPdf.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_Insights"))
                {
                    string postDate = DateTime.ParseExact(txtDate.Text, "dd-MM-yyyy", null).ToString("yyyy/MM/dd");

                    int val;
                    if (ckstatus.Checked)
                    {
                        val = 1;
                    }
                    else
                    {
                        val = 0;
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "update");
                    cmd.Parameters.AddWithValue("@Id", hdnId.Value);
                    cmd.Parameters.AddWithValue("@ItsId", hdnItsId.Value);
                    cmd.Parameters.AddWithValue("@Year", ddlYear.SelectedValue.Trim());
                    cmd.Parameters.AddWithValue("@Heading", txtHeading.Text.Trim());
                    cmd.Parameters.AddWithValue("@Pdf", MainImage + ext);
                    cmd.Parameters.AddWithValue("@Url", txtUrl.Text.Trim());
                    cmd.Parameters.AddWithValue("@PostDate", postDate);
                    cmd.Parameters.AddWithValue("@Type", ddlType.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@Status", val);

                    if (utility.Execute(cmd))
                    {
                       
                        Reset();
                        BindGrid();
                     
                        MyMessageBox1.ShowSuccess("Successfully updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

        //if (!fileUploadImage.HasFile)
        //{
        //    message = "Banner,";
        //    isOK = false;
        //}

        if (ddlYear.SelectedValue == "0")
        {
            message = "Select Year, ";
            isOK = false;
        }

        if (txtHeading.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Heading, ";
        }

        if (ddlType.SelectedValue != "select Type")
        { 

        if (ddlType.SelectedValue == "Pdf")
        {
                if (!fileUploadPdf.HasFile)
                {
                    message += "pdf,";
                    isOK = false;
                }
            }
        else
        {
            if (txtUrl.Text.Trim().Equals(string.Empty))
                {
                message += "Url,";
                isOK = false;
            }
        }
        }

        if (ddlType.SelectedValue == "select Type")
        {
            message += "Select Type ,";
        }
   

        if (txtDate.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Post Date, ";
        }



        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;

        if (ddlYear.SelectedValue == "0")
        {
            message = "Select Year, ";
            isOK = false;
        }

        if (txtHeading.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Heading, ";
        }

        //if (ddlType.SelectedValue != "select Type")
        //{

        //    if (ddlType.SelectedValue == "Pdf")
        //    {
        //        if (!fileUploadPdf.HasFile)
        //        {
        //            message += "pdf, ";
        //            isOK = false;
        //        }              
        //    }
        //    else
        //    {
        //        if (txtUrl.Text.Trim().Equals(string.Empty))
        //        {
        //            message += "Url, ";
        //            isOK = false;
        //        }
        //    }
        //}

        if (ddlType.SelectedValue == "select Type")
        {
            isOK = false;
            message += "Select Type, ";

        }
        else
        {
            if (ddlType.SelectedValue == "Url")
            { 
                if (txtUrl.Text.Trim().Equals(string.Empty))
              {
                 message += "Url, ";
               isOK = false;
            }
            }
        }


        if (txtDate.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Post Date, ";
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    private void Reset()
    {
        ddlYear.SelectedValue = "0";
        hdnItsId.Value = string.Empty;
        txtHeading.Text = string.Empty;
        txtDate.Text = string.Empty;
        //ddlYear.SelectedIndex = -1;
        txtUrl.Text = string.Empty;
        ddlType.SelectedValue = "select Type";
        ckstatus.Checked = false;
        //PdfPreview.Visible = false;
        btnsumbit.Text = "Save";


    }
    protected void btnreset_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("Proc_Insights"))
            {
                int Id = (int)gdView.DataKeys[e.RowIndex].Value;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "delete");
                cmd.Parameters.AddWithValue("@ItsId", Id);
                utility.Execute(cmd);
                BindGrid();
                MyMessageBox1.ShowSuccess("Record Deleted Successfully.");
            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Unable to delete record.");
        }

    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            int Id = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);

            DataTable dt = new DataTable();
            dt = utility.Display("Exec Proc_Insights 'getbyId', '" + Id + "'");
            hdnId.Value = dt.Rows[0]["Id"].ToString();
            hdnItsId.Value = dt.Rows[0]["ItsId"].ToString();
            ddlYear.SelectedValue = dt.Rows[0]["Year"].ToString();
            txtHeading.Text = dt.Rows[0]["Heading"].ToString();
            txtUrl.Text = dt.Rows[0]["Url"].ToString();
            txtDate.Text = dt.Rows[0]["PostDate"].ToString();
            ddlType.Text = dt.Rows[0]["Type"].ToString();
            PdfPreview.NavigateUrl = string.Format("../Content/uploads/Pdf/" + dt.Rows[0]["Pdf"].ToString());
            if (int.Parse(dt.Rows[0]["Status"].ToString()) == 1)
            {
                ckstatus.Checked = true;             
            }
            else
            {
                ckstatus.Checked = false;
            }


            btnsumbit.Text = "Update";

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }

    }
}