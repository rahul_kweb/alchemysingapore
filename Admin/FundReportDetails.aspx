﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="FundReportDetails.aspx.cs" Inherits="Admin_FundReportDetails" %>

<%@ Register Src="~/Admin/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Fund Report -
                                <asp:Literal ID="ltrCategory" runat="server"></asp:Literal></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
                            <br>
                            <div id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

                                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Heading <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtName" CssClass="form-control col-md-7 col-xs-12" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                               <div class="form-group">
                                    <asp:HiddenField ID="hdnId" runat="server" />
                                    <asp:HiddenField ID="hdnPeopleCultureId" runat="server" />
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Pdf  <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:FileUpload ID="fileUploadImage" CssClass="form-control col-md-7 col-xs-12" runat="server" />
                                        <asp:HyperLink ID="ImagePreview" runat="server" Text="View Pdf" Visible="false" Target="_blank"></asp:HyperLink>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Display Order
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">

                                        <button type="button" class='down_count btn btn-info' title='Down'><i class='fa fa-minus icon-minus'></i></button>
                                        <asp:TextBox ID="txtDisplayOrder" CssClass="counter" runat="server" Style="width: 45px" autocomplete="off"></asp:TextBox>
                                        <button type="button" class='up_count btn btn-info' title='Up'><i class='fa fa-plus icon-plus'></i></button>

                                    </div>
                                </div>

                                <div class="ln_solid"></div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <asp:Button ID="btnsumbit" type="submit" Text="Save" value="Submit" class="btn btn-success StoreDDLValueinhihhenfields" title="Submit" runat="server" OnClick="btnsumbit_Click" />
                                        <asp:Button ID="btnreset" value="Cancel" Text="Cancel" class="btn btn-primary" title="Cancel" runat="server" OnClick="btnreset_Click" />
                                        <input type="button" value="Back" id="btnBack" class="btn btn-dark" title="Back">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
                                OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging" DataKeyNames="FDetailId"
                                class="table table-striped table-bordered" PageSize="10" AllowPaging="true">
                                <Columns>
                                    <asp:BoundField DataField="FDetailId" HeaderText="FDetailId" />
                                    <asp:BoundField DataField="Sr" HeaderText="Sr No." />
                                    <asp:BoundField DataField="Heading" HeaderText="Heading" />
                                    <asp:TemplateField HeaderText="Pdf" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <a href='<%# Eval("Pdf", "../Content/uploads/PDF/{0}") %>' target="_blank">View</a>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                   
                                    <asp:BoundField DataField="DisplayOrder" HeaderText="Order" />                                   

                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton2" CssClass="btn" runat="server" CausesValidation="False"
                                                CommandName="Select">
                            <i class="icon-edit"></i> Edit
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                                                CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <%-- <PagerStyle CssClass="pagination"></PagerStyle>--%>
                            </asp:GridView>
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>

    <style>
        .counter {
            width: 45px;
            border-radius: 0px !important;
            text-align: center;
        }

        .up_count {
            margin-bottom: 10px;
            margin-left: -4px;
            border-top-left-radius: 0px;
            border-bottom-left-radius: 0px;
        }

        .down_count {
            margin-bottom: 10px;
            margin-right: -4px;
            border-top-right-radius: 0px;
            border-bottom-right-radius: 0px;
        }
    </style>

    <script>
        $(document).ready(function () {
            $('button').click(function (e) {
                var button_classes, value = +$('.counter').val();
                button_classes = $(e.currentTarget).prop('class');
                if (button_classes.indexOf('up_count') !== -1) {
                    value = (value) + 1;
                } else {
                    value = (value) - 1;
                }
                value = value < 0 ? 0 : value;
                $('.counter').val(value);
            });
            $('.counter').click(function () {
                $(this).focus().select();
            });
        });

        $('#btnBack').click(function () {
            window.location.href = "FundReport.aspx";
        })
    </script>
</asp:Content>

