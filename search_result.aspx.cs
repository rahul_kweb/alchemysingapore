﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class search_result : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        //BindSearch();

        if (Request.QueryString["SearchKey"] != null)
        {
            string value = Request.QueryString["SearchKey"].ToString();

            BindSearch_Result(value);
        }
    }

    public void BindSearch()
    {
        if (Session["Search"] != null)
        {
            ltrSearch.Text = Session["Search"].ToString();
            Session["Search"] = null;
        }
    }

    public void BindSearch_Result(string value)
    {
        lblTitle.Text = value;
        StringBuilder strHeading = new StringBuilder();
        
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_SearchResult 'GET_SEARCH','" + value + "'");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                strHeading.Append("<div class=\"col-12 brown-dashed-border\">");
                strHeading.Append("<a href=" + dr["PageName"] + "><h2>" + dr["Heading"].ToString() + "</h2></a>");

                string your_String = dr["Description"].ToString();
                string pattern = @"</?\w+((\s+\w+(\s*=\s*(?:"".*?""|'.*?'|[^'"">\s]+))?)+\s*|\s*)/?>";
                string Output = Regex.Replace(your_String, pattern, string.Empty).Replace("\r\n\t", "");

                strHeading.Append("<p>" + Output + "</p>");
                strHeading.Append("</div>");

                pError.Visible = false;
            }
        }
        else
        {
            pError.Visible = true;
        }
        ltrSearch.Text = strHeading.ToString();
    }

}