﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="equity_market_outlook_dec_2019.aspx.cs" Inherits="equity_market_outlook_dec_2019" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="banner" class="position-relative mb-5">
        <h1>Thought Leadership</h1>
        <%--<img src="images/banner/thought_leadership.jpg" class="img-fluid" alt="">--%>
        <asp:Image ID="imgInnerBanner" runat="server" ImageUrl="images/banner/about_banner.jpg" class="img-fluid" alt="" />

    </div>

    <div class="container mb-4">
        <div class="row">
            <div class="col-12">

                <%-- <ul class="breadcrumb">
                    <li><a href="index.aspx">Home</a></li>
                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                    <li><a href="thought_leadership.aspx">Thought Leadership</a></li>
                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                    <li>Equity Market Outlook - December 2019</li>
                </ul>--%>

                <asp:Literal ID="ltrBreakcum" runat="server"></asp:Literal>


            </div>
        </div>

        <asp:Literal ID="ltrContent" runat="server"></asp:Literal>


        <%--<div class="row mb-5 innerPageData" id="blog">
            <div class="col-12 mb-4">

                <h4 class="brownColor">EQUITY MARKET OUTLOOK - DECEMBER 2019 </h4>
                <p><strong>Markets pausing for breath... </strong></p>

                <p>The markets remained resilient through November 2019 with the Nifty rising 2.2% and the NSE Midcap rising 5.7%. The NSE Bank was one of the key sectoral indices, rising >8% this month. </p>

                <p>The concurrent indicators for the economy remained weak. Industrial growth was sluggish and the Index of Industrial Production (IIP) delivered a negative print for October 2019. Exports also remained sluggish with a 1.1% contraction for October 2019. The worrying sign is that bank credit growth remains stubbornly low at 8.1 % y/y. Compounded with the weak growth for NBFCs and the bond market; this remains an impediment to growth. This weakness is reflected in the GDP growth for 2QFY20, which came in at 4.5%.</p>

                <p>Some positives are, however, coming through. As we discussed last month, interest rates are continuing to head southwards, aided by both RBI rate cuts and transmission. The monsoons have been strong and this should aid a recovery in rural sentiment. The reflation of food prices would also help in improving cash flows to the farming and rural segments. Additionally, you may also read the views from HDFC Bank in The Print (<a href="https://theprint.in/economy/hdfc-bank-sees-signs-of-economy-reviving-in-rural-and-semi-urba n-a reas/329435/">https://theprint.in/economy/hdfc-bank-sees-signs-of-economy-reviving-in-rural-and-semi-urba n-a reas/329435/<a>)* </p>

                <p>The policy environment is improving with some significant events this month. </p>

                <ul class="bullet">
                    <li>The Supreme Court judgment on the Essar Steel insolvency was a major positive for the banking sector. It resolves many of the uncertainties surrounding the IBC process, and this will help banks recover future NPAs faster, with lower losses. This should be positive for corporate banks like ICICI Bank and Axis Bank.</li>

                    <li>The Cabinet approved the privatization of Air India and BPCL this month. These have twin benefits – not only does the stressed fiscal get a boost but it also helps improve efficiencies in the economy as ownership transitions from the state to private hands. There are still some obstacles in execution but the intent of the government is a good sign.</li>

                    <li>The Lok Sabha took up the Industrial Relations Code Bill – this could form the basis for significant labour reform. Rigid labour laws are seen as an impediment to the investment cycle.</li>

                    <li>The key risks remain unchanged. The real estate sector is a pain point and the source for much of the stress in the financial sector. The govern-ment's proposed Alternative Investment Fund (AIF) is a good step but would take time to impact. The other source of pain is the fiscal deficit – weak growth is translating to sluggish revenues, which are now lagging behind the budget forecasts.</li>
                </ul>

                <p>We are slowing our deployment as the post-September 2019 rally has stretched valuations for the portfolio companies. On the other hand, the continued weakness in the economy does not give us confidence to go down the quality curve. We will watch the market till the next Budget in end-January 2020 for further cues.</p>

                <p>(* The information contained in this link has been obtained from public domain and is for informational purposes only. Alchemy does not make any representations or warranty, express or implied, that such information is accurate or complete.)</p>
                <p><strong>Source — Alchemy Group Research</strong></p>
            </div>




        </div>--%>

        <div class="brown-dashed-border mb-5"></div>


    </div>
    <script>
        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

    </script>
</asp:Content>

