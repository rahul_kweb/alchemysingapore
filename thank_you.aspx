﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="thank_you.aspx.cs" Inherits="thank_you" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="banner" class="position-relative mb-5">
        <h1>Thank You</h1>
        <img src="images/banner/thank_you.jpg" class="img-fluid" alt="">
    </div>

    <div class="container mb-4 py-5 text-center">

		<h1 style="font-weight:bold">Thank you for sharing your details with us. <br /> Our representative will contact you soon.</h1>

        
    </div>

</asp:Content>

