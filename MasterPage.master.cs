﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SocialMedia(1);

            if (Session["UserId"] != null)
            {
                lblWelcome.Text = "Welcome " + Session["Name"].ToString();
                aLogin.Visible = false;
                divLogOut.Visible = true;
                lblWelcome.Visible = true;

                //aTagOffering.HRef = "our_product.aspx";
            }
            else
            {
                aLogin.Visible = true;
                divLogOut.Visible = false;
                lblWelcome.Visible = false;
            }

            string PageName = System.IO.Path.GetFileNameWithoutExtension(Request.Path);

            if (PageName == "alchemy_india_long_term_fund_limited")
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("login.aspx");
                }
            }

      
        }
    }

    //protected void btnSearch_Click(object sender, EventArgs e)
    //{

    //    string dirScanner = Server.MapPath("~/");

    //    if (string.IsNullOrEmpty(txtSerach.Text))
    //        return;

    //    string[] allFiles = Directory.GetFiles(dirScanner, "*.aspx");

    //    StringBuilder strBuild = new StringBuilder();

    //    foreach (string file in allFiles)
    //    {
    //        StreamReader sr = new StreamReader(file);
    //        string sContentsa = sr.ReadToEnd();

    //        foreach (Match match in Regex.Matches(sContentsa, @"<p.*?>\s*(.+?)\s*</p>"))
    //        {
    //            if (match.Success)
    //            {
    //                string page_content = match.Groups[1].Value.ToUpper();

    //                if (page_content.Contains(txtSerach.Text.ToUpper()))
    //                {


    //                    int maxlen = 500;

    //                    if (file.Contains("index.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/index.aspx'><h2>HOME</h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }




    //                    if (file.Contains("about.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/about.aspx'><h2>ABOUT US</h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);

    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {

    //                            strBuild.Append("<p>" + s + "</p>");

    //                        }

    //                        strBuild.Append("</div>");
    //                    }

    //                    if (file.Contains("advantage.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/advantage.aspx'><h2>ADVANTAGE</h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }

    //                    if (file.Contains("amit_nadekar.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/amit_nadekar.aspx'><h2>ALCHEMY TEAM </h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }

    //                    if (file.Contains("ashwin_kedia.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/ashwin_kedia.aspx'><h2>ALCHEMY TEAM </h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }

    //                    if (file.Contains("atul_sharma.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/atul_sharma.aspx'><h2>ALCHEMY TEAM </h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }

    //                    if (file.Contains("hiren_ved.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/hiren_ved.aspx'><h2>ALCHEMY TEAM </h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }

    //                    if (file.Contains("lashit_sanghvi.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/lashit_sanghvi.aspx'><h2>ALCHEMY TEAM </h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }

    //                    if (file.Contains("rakesh_jhunjhunwala.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/rakesh_jhunjhunwala.aspx'><h2>ALCHEMY TEAM </h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }

    //                    if (file.Contains("seshadri_sen.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/seshadri_sen.aspx'><h2>ALCHEMY TEAM </h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }

    //                    if (file.Contains("vikas_kumar.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/vikas_kumar.aspx'><h2>ALCHEMY TEAM </h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }


    //                    if (file.Contains("contact.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/contact.aspx'><h2>CONTACT US</h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }

    //                    if (file.Contains("equity_market_outlook_august_2019.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/equity_market_outlook_august_2019.aspx'><h2>THOUGHT LEADERSHIP</h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }

    //                    if (file.Contains("equity_market_outlook_dec_2019.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/equity_market_outlook_dec_2019.aspx'><h2>THOUGHT LEADERSHIP</h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }

    //                    if (file.Contains("equity_market_outlook_june_2019.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/equity_market_outlook_june_2019.aspx'><h2>THOUGHT LEADERSHIP</h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }

    //                    if (file.Contains("equity_market_outlook_nov_2019.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/equity_market_outlook_nov_2019.aspx'><h2>THOUGHT LEADERSHIP</h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }

    //                    if (file.Contains("equity_market_outlook_sep_2019.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/equity_market_outlook_sep_2019.aspx'><h2>THOUGHT LEADERSHIP</h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }


    //                    if (file.Contains("long_term_fund_ltd.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/long_term_fund_ltd.aspx'><h2>ALCHEMY INDIA LONG TERM FUND LTD</h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }

    //                    if (file.Contains("our_process.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/our_process.aspx'><h2>OUR PROCESS </h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }

    //                    if (file.Contains("our_product.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/our_product.aspx'><h2>OUR PRODUCTS</h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }

    //                    if (file.Contains("our_team.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/our_team.aspx'><h2>ALCHEMY TEAM </h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }


    //                    if (file.Contains("thought_leadership.aspx"))
    //                    {
    //                        strBuild.Append("<div class='col-12 brown-dashed-border'>");
    //                        strBuild.Append("<a href='/thought_leadership.aspx'><h2>THOUGHT LEADERSHIP</h2></a>");
    //                        string replacement = string.Format("<text style='background-color:yellow'>{0}</text>", "$0");
    //                        string s = Regex.Replace(match.Groups[1].Value, txtSerach.Text, replacement, RegexOptions.IgnoreCase);
    //                        if (s.Length > maxlen)
    //                        {
    //                            strBuild.Append("<p>" + s.Substring(0, maxlen) + "</p>");
    //                        }
    //                        else
    //                        {
    //                            strBuild.Append("<p>" + s + "</p>");
    //                        }
    //                        strBuild.Append("</div>");
    //                    }
    //                }
    //            }

    //        }

    //    }
    //    Session["Search"] = strBuild.ToString();
    //    Response.Redirect("/search_result.aspx");
    //}

    public void SocialMedia(int Id)
    {
        DataTable dt = new DataTable();
        Dictionary<string, string> obj = new Dictionary<string, string>();
        dt = utility.Display("Exec Proc_SocialMedia 'getbyId','" + Id + "'");
        if (dt.Rows.Count > 0)
        {

            ltrInvestorContact.Text = dt.Rows[0]["FindUs"].ToString();
            ltrRegisteredAddress.Text = dt.Rows[0]["CallUs2"].ToString();
            ltrOfficeAddress.Text = dt.Rows[0]["Clients"].ToString();
        }

    }
}
