﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class thought_leadership : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindInnerBanner(17);
            BindCsr();
        }
    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            imgInnerBanner.ImageUrl = "Content/uploads/InnerBanner/" + dt.Rows[0]["Image"].ToString();
        }
        else
        {
            imgInnerBanner.ImageUrl = "images/banner/about_banner.jpg";
        }


    }

    public void BindCsr()
    {
        StringBuilder strTitle = new StringBuilder();
        StringBuilder strDesc = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_ThoughtLeadership 'bindThoughtLeadership'");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                strDesc.Append("<div class=\"col-md-6 col-lg-4 mb-4\">");
                strDesc.Append("<div class=\"whiteBox h-100\">");
                strDesc.Append("<h4 class=\"brownColor mb-2\">" + dr["Heading"].ToString() + "</h4>");
                strDesc.Append("<p><strong>" + dr["PostDate"].ToString() + "</strong></p>");
                strDesc.Append(dr["Description"].ToString());
                strDesc.Append("<a href=\"" + dr["slug"].ToString() + "\" class=\"readMore brownBg\">Read More</a>");
                strDesc.Append("</div>");
                strDesc.Append("</div>");
            }
            ltrDescription.Text = strDesc.ToString();
        }
    }
}