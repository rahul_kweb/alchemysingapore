﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class detailsPage : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (RouteData.Values["slug"] != null)
            {
                string slug = RouteData.Values["slug"].ToString();
                TeamDetails(slug);
            }
        }
    }

    public System.Web.Routing.RouteData RouteData
    {
        get
        {
            return HttpContext.Current.Items["RouteData"] as System.Web.Routing.RouteData;
        }
    }


    public void TeamDetails(string strSlug)
    {
        StringBuilder strBreadCum = new StringBuilder();
        StringBuilder strProfileDetail = new StringBuilder();

        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        dt = utility.Display("Exec Proc_PeopleCultureMaster 'bindTeamDetailById','0','','0','','" + strSlug + "'");
        dt1 = utility.Display("Exec Proc_ThoughtLeadership 'bindThoughtLeadershipbyId','0','','','','','','" + strSlug + "'");
        if (dt.Rows.Count > 0)
        {
            //bread cum
            strBreadCum.Append("<ul class=\"breadcrumb\">");
            strBreadCum.Append("<li><a href=\"index.aspx\">Home</a></li>");
            strBreadCum.Append("<li><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></li>");
            strBreadCum.Append("<li><a href=\"our_team.aspx\">Our Team</a></li>");
            strBreadCum.Append("<li><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></li>");
            strBreadCum.Append("<li>" + dt.Rows[0]["Name"].ToString() + "</li>");
            strBreadCum.Append("</ul>");

            //profile details
            strProfileDetail.Append("<div class=\"row justify-content-center\">");
            strProfileDetail.Append("<div class=\"col-xl-12\">");

            strProfileDetail.Append("<div>");
            strProfileDetail.Append("<div class=\"innerPageData\" id=\"personData\">");
            strProfileDetail.Append("<div>");
            strProfileDetail.Append("<h4>" + dt.Rows[0]["Category"].ToString() + "</h4>");
            strProfileDetail.Append("<div class=\"whiteBox\">");
            strProfileDetail.Append("<div class=\"container p-0\">");
            strProfileDetail.Append("<div class=\"row align-items-center\">");
            strProfileDetail.Append("<div class=\"col-sm-6 col-md-4 col-lg-3\">");
            strProfileDetail.Append("<img src=\"Content/uploads/Team/" + dt.Rows[0]["Image"].ToString() + "\" class=\"img-fluid\" alt=\"\">");
            strProfileDetail.Append("</div>");
            strProfileDetail.Append("<div class=\"col-sm-6 col-md-8 col-lg-9\">");
            strProfileDetail.Append("<h4>" + dt.Rows[0]["Name"].ToString() + " - <span class=\"brownColor\">" + dt.Rows[0]["Designation"].ToString() + "</span></h4>");
            strProfileDetail.Append(dt.Rows[0]["Description"].ToString());
            strProfileDetail.Append("</div>");
            strProfileDetail.Append("</div>");
            strProfileDetail.Append("</div>");
            strProfileDetail.Append("</div>");
            strProfileDetail.Append("</div>");
            strProfileDetail.Append("</div>");
            strProfileDetail.Append("</div>");


            strProfileDetail.Append("</div>");
            strProfileDetail.Append("</div>");

            ltrBreakcum.Text = strBreadCum.ToString();
            ltrContent.Text = strProfileDetail.ToString();

            //banner
            BindInnerBanner(2);
            lblTitle.Text = "Our Team";
        }
        else if (dt1.Rows.Count > 0)
        {
            //bread cum
            strBreadCum.Append("<ul class=\"breadcrumb\">");
            strBreadCum.Append("<li><a href=\"index.aspx\">Home</a></li>");
            strBreadCum.Append("<li><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></li>");
            strBreadCum.Append("<li><a href=\"thought_leadership.aspx\">Thought Leadership</a></li>");
            strBreadCum.Append("<li><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></li>");
            strBreadCum.Append("<li>" + dt1.Rows[0]["Heading"].ToString() + " - " + dt1.Rows[0]["PostDate"].ToString() + "</li>");
            strBreadCum.Append("</ul>");

            strProfileDetail.Append("<div class=\"row mb-5 innerPageData\" id=\"blog\">");
            strProfileDetail.Append("<div class=\"col-12 mb-4\">");
            strProfileDetail.Append("<h4 class=\"brownColor\">" + dt1.Rows[0]["Heading"].ToString() + " - " + dt1.Rows[0]["PostDate"].ToString() + " </h4>");
            strProfileDetail.Append(dt1.Rows[0]["Description"].ToString());
            strProfileDetail.Append("</div>");
            strProfileDetail.Append("</div>");


            ltrBreakcum.Text = strBreadCum.ToString();
            ltrContent.Text = strProfileDetail.ToString();

            //banner
            BindInnerBanner(17);
            lblTitle.Text = "Thought Leadership";
        }
        else
        {
            Response.Redirect("page_not_found.aspx");
        }
    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            imgInnerBanner.ImageUrl = "Content/uploads/InnerBanner/" + dt.Rows[0]["Image"].ToString();
        }
        else
        {
            imgInnerBanner.ImageUrl = "images/banner/about_banner.jpg";
        }
    }
}