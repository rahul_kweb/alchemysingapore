﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class our_product : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["prevUrl"] = Request.Url.AbsolutePath;

            BindContent(3);
            BindInnerBanner(4);
            BindCsr(2);
        }
    }

    public string BindContent(int Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS'," + Id + "");
        if (dt.Rows.Count > 0)
        {
            ltrContent.Text = dt.Rows[0]["Description"].ToString();
        }
        return ltrContent.Text;
    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            imgInnerBanner.ImageUrl = "Content/uploads/InnerBanner/" + dt.Rows[0]["Image"].ToString();
        }
        else
        {
            imgInnerBanner.ImageUrl = "images/banner/about_banner.jpg";
        }


    }

    public void BindCsr(int Id)
    {
        StringBuilder strTitle = new StringBuilder();
        StringBuilder strDesc = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_TabMaster 'get',0,'" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                strTitle.Append("<li>" + dr["Title"].ToString() + "</li>");

                DataTable dt1 = new DataTable();
                dt1 = utility.Display("Exec Proc_TabContent 'get',0,'" + dr["TabId"].ToString() + "'");

                strDesc.Append("<div>");
                foreach (DataRow dr1 in dt1.Rows)
                {
                    strDesc.Append("<div class=\"whiteBox p-4 mb-4\">");
                    
                    strDesc.Append("<h4>" + dr1["Title"].ToString() + "</h4>");                   
                    strDesc.Append(dr1["Description"].ToString());

                    strDesc.Append("</div>");


                }
                strDesc.Append("</div>");

            }
            ltrHeading.Text = strTitle.ToString();
            ltrDescription.Text = strDesc.ToString();
        }
    }
}