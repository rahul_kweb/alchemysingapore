﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class login : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            if (Request.QueryString["PageName"] == "Default")
            {
                Session.Abandon();
                Response.Redirect("index.aspx");
            }

            BindInnerBanner(10);

        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec stp_Registration 'Login','','','','" + txtUsername.Text.Trim() + "','','0','0','0','','0','0','0','0','0','','0','0','0','','0','" + txtPassword.Text.Trim() + "'");
        if (dt.Rows.Count > 0)
        {
            Session["UserId"] = dt.Rows[0]["RegId"].ToString();
            Session["Name"] = dt.Rows[0]["Name"].ToString();
            //Response.Redirect("index.aspx");

            if (Session["prevUrl"] != null)
            {
                Response.Redirect((string)Session["prevUrl"]); //Will redirect to previous page
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }
        else
        {
            lblError.Text = "Invalid username and password.";
            divError.Visible = true;
        }
    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            imgInnerBanner.ImageUrl = "Content/uploads/InnerBanner/" + dt.Rows[0]["Image"].ToString();
        }
        else
        {
            imgInnerBanner.ImageUrl = "images/banner/about_banner.jpg";
        }


    }
}