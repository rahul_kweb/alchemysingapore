﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="banner" class="position-relative mb-5">
        <h1>Login</h1>
        <asp:Image ID="imgInnerBanner" runat="server" ImageUrl="images/banner/about_banner.jpg" class="img-fluid" alt="" />

        <%--<img src="images/banner/login_banner.jpg" class="img-fluid" alt="">--%>
    </div>

    <div class="container mb-4">

        <div class="row mb-5 innerPageData" id="loginForm">
            <h4 class="col-12 brownColor">Log in / <a href="registration.aspx" class="brownColor">Register</a> to access and learn more about Alchemy Investment's Funds</h4>
            <div class="col-12 col-md-8 col-lg-5">
            <div class="alert alert-warning alert-dismissible fade show" role="alert" style="display: none;">
                    <strong>Following fileds are required!</strong><br />
                    <label id="lblmsg"></label>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="alert alert-danger alert-dismissible fade show" role="alert" id="divError" runat="server" visible="false">
                    <strong>
                        <asp:Label ID="lblError" runat="server"></asp:Label>
                    </strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="row">
                    <div class="col-12">
                        <label>Username</label>

                        <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control" autocomplete="off"></asp:TextBox>
                    </div>
                    <div class="col-12">
                        <label>Password</label>
                        <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" autocomplete="off" TextMode="Password"></asp:TextBox>
                    </div>
                    <div class="col-12">
                        <div class="row align-items-center">
                            <div class="col-12 col-md-5">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="submitBtn" Text="Login" OnClick="btnSubmit_Click" OnClientClick="return checkValidation()" />

                            </div>

                            <div class="col-12 col-md-7  logLinks">
                                <span>
                                    <a href="forgot_password.aspx">Forgot Password</a> | <a href="registration.aspx">New User Registration</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="brown-dashed-border mb-5"></div>


    </div>

    <script>

        function checkValidation() {
            var username = $('#<%=txtUsername.ClientID%>').val();
            var password = $('#<%=txtPassword.ClientID%>').val();
            var msg = "";
            var blank = false;

            if (username == '') {
                $('#<%=txtUsername.ClientID%>').css('border-color', 'red');
                msg += "Username, ";
                blank = true;
            } else {
                $('#<%=txtUsername.ClientID%>').css('border-color', '');
            }

            if (password == '') {
                $('#<%=txtPassword.ClientID%>').css('border-color', 'red');
                msg += "Password, ";
                blank = true;
            } else {
                $('#<%=txtPassword.ClientID%>').css('border-color', '');
            }

            if (msg != "") {
                msg = msg.substring(0, msg.length - 2);
                $('#lblmsg').text(msg);
                $('.alert').slideDown('slow');
            } else {
                $('.alert').hide('slow');
            }

            if (blank) {
                return false;
            } else {
                return true;
            }
        }

        $(".close").click(function () {
            $('.alert').slideUp('slow');
        });

        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

    </script>
</asp:Content>

