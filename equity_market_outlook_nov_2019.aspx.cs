﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class equity_market_outlook_nov_2019 : System.Web.UI.Page
{

    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ThoughtAndLeadershipDetails();
        }
    }

    public void ThoughtAndLeadershipDetails()
    {
        StringBuilder strBreadCum = new StringBuilder();
        StringBuilder strProfileDetail = new StringBuilder();

        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        dt1 = utility.Display("Exec Proc_ThoughtLeadership 'bindThoughtLeadershipbyThoughtId','4'");
        if (dt1.Rows.Count > 0)
        {
            //bread cum
            strBreadCum.Append("<ul class=\"breadcrumb\">");
            strBreadCum.Append("<li><a href=\"index.aspx\">Home</a></li>");
            strBreadCum.Append("<li><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></li>");
            strBreadCum.Append("<li><a href=\"thought_leadership.aspx\">Thought Leadership</a></li>");
            strBreadCum.Append("<li><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></li>");
            strBreadCum.Append("<li>" + dt1.Rows[0]["Heading"].ToString() + " - " + dt1.Rows[0]["PostDate"].ToString() + "</li>");
            strBreadCum.Append("</ul>");

            strProfileDetail.Append("<div class=\"row mb-5 innerPageData\" id=\"blog\">");
            strProfileDetail.Append("<div class=\"col-12 mb-4\">");
            strProfileDetail.Append("<h4 class=\"brownColor\">" + dt1.Rows[0]["Heading"].ToString() + " - " + dt1.Rows[0]["PostDate"].ToString() + " </h4>");
            strProfileDetail.Append(dt1.Rows[0]["Description"].ToString());
            strProfileDetail.Append("</div>");
            strProfileDetail.Append("</div>");


            ltrBreakcum.Text = strBreadCum.ToString();
            ltrContent.Text = strProfileDetail.ToString();

            //banner
            BindInnerBanner(17);
        }
        else
        {
            Response.Redirect("page_not_found.aspx");
        }
    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            imgInnerBanner.ImageUrl = "Content/uploads/InnerBanner/" + dt.Rows[0]["Image"].ToString();
        }
        else
        {
            imgInnerBanner.ImageUrl = "images/banner/about_banner.jpg";
        }
    }
}