﻿
$(document).ready(function () {
    $('#<%=btnUpdateDetails.ClientID%>').click(function () {
        ValidateFields();
    });
});
function ValidateFields() {
    var name = $('#<%=txtname.ClientID%>').val();
    var emailid = $('#<%=txtemailid.ClientID%>').val();
    var mobileno = $('#<%=#txtmobileno%>').val();
    var ID = $('#<%=hdnid.ClientID%>').val();
    var blank = true;

    if (emailid != '' && mobileno != '' && name != '') {

        blank = false;
    }
    else {

        blank = true
    }
    if (blank != true) {
       

        var Login = {}; //Creating Class List
        Login.Id = ID;
        Login.Emailid = emailid;
        Login.Mobileno = mobileno;
        Login.Name = name;

        $.ajax({
            type: "POST",
            url: "/Admin/UpdateProfile",
            data: JSON.stringify(Login),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
            
                if (data.Responsetype == "Success") {
                    alert('Profile updated successfully!');
                }
                else {
                    alert('Update Fail');
                }
            }
        });
        return true;
    }
    else {
        //alert('wrong');
        $('#validationmessage').attr('style', 'display:block');
        $('#lblmsg').attr('style', 'color:red');
        $('#lblmsg').html('Please Enter Username and Password!');

        $(".imgloader").hide();
        return false;
    }
};


function ChangePasswordValidation() {
   
    var oldPassword = $('#txtold').val();
    var password = $('#txtnew').val();
    var confpassword = $('#txtconfirm').val();
    var blank = false;

    if (oldPassword == '') {
        $('#lblold').html('Password is required.').css('color', 'red');
        $('#txtold').css('border-color', '#f2871e');
        blank = true;
    } else {
        $('#lblold').html('');
        $('#txtold').css('border-color', '#3fb34f');
    }

    if (password == '') {
        $('#lblnew').html('New Password is required.').css('color', 'red');
        $('#txtnew').css('border-color', '#f2871e');
        blank = true;
    } else {
        if (password.length < 6) {
            $('#lblnew').html('Your new password must be at least 6 characters. Please try again.').css('color', 'red');
            $('#txtnew').css('border-color', '#f2871e');
            blank = true;
        } else {
            $('#lblnew').html('');
            $('#txtnew').css('border-color', '#3fb34f');
        }
    }

    if (confpassword == '') {
        $('#lblconfirm').html('Confirm Password is required.').css('color', 'red');
        $('#txtconfirm').css('border-color', '#f2871e');
        blank = true;
    } else {
        if (confpassword != password) {
            $('#lblconfirm').html('Please enter same password as above.').css('color', 'red');
            $('#txtconfirm').css('border-color', '#f2871e');
            blank = true;
        } else {
            $('#lblconfirm').html('');
            $('#txtconfirm').css('border-color', '#3fb34f');
        }
    }

    if (blank) {
        return false;
    } else {
        return true;
    }
}
