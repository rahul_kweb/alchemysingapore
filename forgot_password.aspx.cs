﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class forgot_password : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindInnerBanner(16);
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec stp_Registration 'ForgotPassword','','','','" + txtEmail.Text.Trim() + "','','0','0','0','','0','0','0','0','0','','0','0','0','','0',''");
        if (dt.Rows.Count > 0)
        {
            SendEmail(dt.Rows[0]["Name"].ToString(), dt.Rows[0]["EmailAddress"].ToString(), dt.Rows[0]["Password"].ToString(), dt.Rows[0]["EmailAddress"].ToString());
            txtEmail.Text = "";
            divSuccess.Visible = true;
            divError.Visible = false;
        }
        else
        {
            lblError.Text = "Please enter register email id.";
            divError.Visible = true;
            divSuccess.Visible = false;
        }
    }

    public string SendEmail(string name, string userid, string password, string ToEmailid)
    {
        string response = string.Empty;
        if (name != "")
        {
            StringBuilder sb = new StringBuilder();
            string Name = name;

            sb.Append("<table border='0' cellspacing='0' cellpadding='10' width='500' align='center' style='border: 1px solid #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 14px; border-collapse: collapse; line-height: 20px;'>");
            sb.Append("<tr><td colspan='2' align='center'><img src='http://kwebmakerdigital.com/alchemy_singapore/html/v4/images/alchemy_logo.png' style='width: auto;max-height:100px;' /><br /><hr color='#04869a' /></td></tr>");
            sb.Append("<tr><td colspan='2'>Dear " + Name + ",<br /> Find your login details below :</td> </tr>");
            sb.Append("<tr bgcolor='#f4f4f4'><td width='90'>User Name:</td><td><strong> " + userid + " </strong> </td> </tr>");
            sb.Append("<tr bgcolor='#f4f4f4'><td>Password :</td><td><strong> " + password + " </strong> </td></tr>");
            sb.Append("<tr bgcolor='#f4f4f4'><td colspan='2' align='center' height='5'></td></tr>");
            sb.Append("<tr><td colspan='2' align='center' height='1'></td></tr>");
            sb.Append("<tr><td colspan='2'>Regards,<br /><strong><font color='#04869a'>Alchemy</font></strong></td></tr>");
            sb.Append("<tr><td colspan='2' align='center' height='1'></td></tr>");
            sb.Append("</table>");

            String[] emailid = new String[] { ToEmailid };


            bool result = utility.SendEmail(sb.ToString(), emailid, "Alchemy Forgot Password", "", null);

            if (result)
            {
                response = "sent";
            }
            else
            {
                response = "";
            }


        }
        else
        {
            response = "";
        }
        return response;
    }

    public void BindInnerBanner(int Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            imgInnerBanner.ImageUrl = "Content/uploads/InnerBanner/" + dt.Rows[0]["Image"].ToString();
        }
        else
        {
            imgInnerBanner.ImageUrl = "images/banner/about_banner.jpg";
        }
    }
}