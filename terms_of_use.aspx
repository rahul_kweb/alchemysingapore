﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="terms_of_use.aspx.cs" Inherits="terms_of_use" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

	<div id="banner" class="position-relative mb-5">
        <h1>Terms Of Use</h1>
        <%--<img src="images/banner/sitemap.jpg" class="img-fluid" alt="">--%>
        <asp:Image ID="imgInnerBanner" runat="server" ImageUrl="images/banner/about_banner.jpg" class="img-fluid" alt="" />

    </div>

    <div class="container mb-4">

                <asp:Literal ID="ltrContent" runat="server"></asp:Literal>

               <%--<h4 class="brownColor">General</h4>
            <p> Alchemy Investment Management Pte. Ltd. (“Alchemy” or the “Firm”) is a private limited company incorporated in the country of Singapore. Alchemy carries out investment management and advisory services and is registered with the U.S. Securities and Exchange Commission as well as  the Monetary Authority of Singapore for this purpose. </p>
            <p> This website does not constitute an offer to sell or a solicitation of an offer to buy any securities. Any such offer or solicitation will be made only by means of the appropriate confidential offering documents that will be furnished to prospective investors. Before making an investment decision, investors are advised to review carefully the appropriate confidential offering documents including the related subscription documents, and to consult with their tax, financial and legal advisors. This website contains a depiction of the activities of Alchemy and the funds to which Alchemy provides advisory services. This website is subject to further amendment or modification without notice. This depiction does not purport to be complete and is qualified in its entirety by the more detailed discussion contained in the confidential offering documents. Alchemy’s investment program for the funds is subject to significant risks. Certain of these risks will be described in the confidential offering documents. Past performance is no guarantee of future results and there can be no assurance that Alchemy will be able to achieve profitability.  Investing involves risk, including the potential loss of principal, and investors should be prepared to absorb potential losses.</p>
            <h4 class="brownColor"> Ownership of Site; Copyrights; Trademarks; Proprietary Notices</h4>
            <p>Alchemy owns and maintains this site. The trademarks, logos and service marks on this site are the property of Alchemy. </p>
            <p> The information provided on this site may not be republished or redistributed, in any format, without the express prior written consent of Alchemy. The copyright and other proprietary notices or legends on any information downloaded or printed from this site must be left intact. These Terms of Use are not intended to, and will not transfer or grant any rights in or to the information on this site and all such rights are reserved by Alchemy or the third party providers of such information.</p>
            <h4 class="brownColor"> Linked Sites</h4>
            <p> Alchemy has not reviewed any of the sites sponsored or maintained by third parties that are linked to this site and is not responsible for the content thereon or any other matter relating thereto. The fact that the link is available on this site does not constitute an endorsement, authorization, sponsorship or affiliation by Alchemy with respect to linked site or its owners, or providers. Anyone who leaves this site via a link contained in this site does so at his/her own risk.</p>
            <h4 class="brownColor"> Distribution</h4>
            <p> The information provided on this site or in any communication containing a link to this site is not intended for distribution to, or use by, any person or entity in any jurisdiction or country where such distribution or use would be contrary to law or regulation or which would require Alchemy or its affiliates to register within such jurisdiction or country.</p>
            <h4 class="brownColor"> No Reliance, No Solicitation or Recommendation</h4>
            <p> The information contained on this site was obtained from various sources that Alchemy believes to be reliable, but Alchemy does not guarantee its accuracy or completeness. The information and opinions contained on this site are subject to change without notice. Neither the information nor any opinion contained on this site constitutes an offer, or a solicitation of an offer, to buy or sell any securities or other financial instruments, including any securities mentioned in any report available on this site. </p>
            <p> The information contained on this site has been prepared and circulated for general information only and is not intended to and does not provide a recommendation with respect to any security. The information on this site does not take into account the financial position or particular needs or investment objectives of any individual or entity. Investors must make their own determinations of the appropriateness of an investment strategy and an investment in any particular securities based upon the legal, tax and accounting considerations applicable to such investors and their own investment objectives. Investors are cautioned that statements regarding future prospects may not be realized and that past performance is not necessarily indicative of future performance.</p>
            <h4 class="brownColor"> Security</h4>
            <p> Alchemy makes no warranty, express or implied, regarding the security of the site, including with respect to the ability of unauthorized persons to intercept or access information transmitted by or to you through this site.</p>
              --%>
              
            
       
        
    </div>


</asp:Content>

