﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class registration : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillCapctha();
            BindInnerBanner(11);
        }
    }

    void FillCapctha()
    {
        try
        {
            Random random = new Random();
            string combination = "0123456789";
            StringBuilder captcha = new StringBuilder();
            for (int i = 0; i < 6; i++)
                captcha.Append(combination[random.Next(combination.Length)]);
            Session["captcha"] = captcha.ToString();
            imgCaptcha.ImageUrl = "GenerateCaptcha.aspx?" + DateTime.Now.Ticks.ToString();
        }
        catch
        {

            throw;
        }
    }


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Session["captcha"] != null)
        {
            if (Session["captcha"].ToString() != txtCaptcha.Text.Trim())
            {
                FillCapctha();
                lblError.Text = "Invalid Captcha.";
                divError.Visible = true;
                divSuccess.Visible = false;
                return;
            }
        }
        else
        {
            FillCapctha();
            lblError.Text = "Invalid Captcha.";
            divError.Visible = true;
            divSuccess.Visible = false;
        }

        using (SqlCommand cmd = new SqlCommand("stp_Registration"))
        {
            var name = txtName.Text.Trim().Split(' ');
            var password = name[0] + RandomString();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Para", "Add");
            cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
            cmd.Parameters.AddWithValue("@Landline", txtLandline.Text.Trim());
            cmd.Parameters.AddWithValue("@Mobile", txtMobile.Text.Trim());
            cmd.Parameters.AddWithValue("@EmailAddress", txtEmail.Text.Trim());
            cmd.Parameters.AddWithValue("@Country", ddlCountry.SelectedItem.ToString());

            cmd.Parameters.AddWithValue("@ExistingClient", rdbExistingClient.Checked);
            cmd.Parameters.AddWithValue("@ByADistributor", rdbByADistributor.Checked);
            cmd.Parameters.AddWithValue("@Others", rdbOther.Checked);
            cmd.Parameters.AddWithValue("@OtherSpecify", txtOtherPleaseSpecify.Text.Trim());

            cmd.Parameters.AddWithValue("@Investor", rdbInvestor.Checked);
            cmd.Parameters.AddWithValue("@Individual", rdbIndividual.Checked);
            cmd.Parameters.AddWithValue("@NonIndividual", rdbNonIndividual.Checked);
            cmd.Parameters.AddWithValue("@DistributorAgent", rdbDistributorAgent.Checked);
            cmd.Parameters.AddWithValue("@ServiceProvider", rdbServiceProvider.Checked);
            cmd.Parameters.AddWithValue("@NatureOfRelationship", txtNatureOfRelationship.Text.Trim());

            cmd.Parameters.AddWithValue("@USPerson", rdbUSPerson.Checked);
            cmd.Parameters.AddWithValue("@USPersonYes", rdbUSPersonYes.Checked);
            cmd.Parameters.AddWithValue("@USPersonNo", rdbUSPersonNo.Checked);
            cmd.Parameters.AddWithValue("@USPersonComment", txtUSPersonCondition.Text.Trim());
            cmd.Parameters.AddWithValue("@NonUSPerson", rdbNonUSPerson.Checked);
            cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());

            cmd.Parameters.AddWithValue("@Password", password);

            if (utility.Execute(cmd))
            {
                SendEmail(txtName.Text.Trim(), txtEmail.Text.Trim(), password, txtEmail.Text.Trim());
                FillCapctha();
                //divSuccess.Visible = true;
                //divError.Visible = false;
                Response.Redirect("thank_you.aspx");
                Clear();
            }
            else
            {
                FillCapctha();
                lblError.Text = "Email id already registered.";
                divSuccess.Visible = false;
                divError.Visible = true;
            }

        }
    }

    public void Clear()
    {
        txtName.Text = "";
        txtLandline.Text = "";
        txtMobile.Text = "";
        txtEmail.Text = "";
        ddlCountry.SelectedIndex = -1;

        rdbExistingClient.Checked = false;
        rdbByADistributor.Checked = false;
        rdbOther.Checked = false;
        txtOtherPleaseSpecify.Text = "";

        rdbInvestor.Checked = false;
        rdbIndividual.Checked = false;
        rdbNonIndividual.Checked = false;
        rdbDistributorAgent.Checked = false;
        rdbServiceProvider.Checked = false;
        txtNatureOfRelationship.Text = "";

        rdbUSPerson.Checked = false;
        rdbUSPersonYes.Checked = false;
        rdbUSPersonNo.Checked = false;
        rdbNonUSPerson.Checked = false;

        chkConfirm.Checked = false;
        txtCaptcha.Text = "";
        txtAddress.Text = "";
    }

    private static Random random = new Random();
    public static string RandomString()
    {
        const string chars = "0123456789";
        return new string(Enumerable.Repeat(chars, 4)
          .Select(s => s[random.Next(s.Length)]).ToArray());
    }

    public string SendEmail(string name, string userid, string password, string ToEmailid)
    {
        string response = string.Empty;
        if (name != "")
        {
            StringBuilder sb = new StringBuilder();
            string Name = name;

            sb.Append("<table border='0' cellspacing='0' cellpadding='10' width='500' align='center' style='border: 1px solid #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 14px; border-collapse: collapse; line-height: 20px;'>");
            sb.Append("<tr><td colspan='2' align='center'><img src='http://kwebmakerdigital.com/alchemy_singapore/html/v4/images/alchemy_logo.png' style='width: auto;max-height:100px;' /><br /><hr color='#04869a' /></td></tr>");
            sb.Append("<tr><td colspan='2'>Dear " + Name + ",<br /> Find your login details below :</td> </tr>");
            sb.Append("<tr bgcolor='#f4f4f4'><td width='90'>User Name:</td><td><strong> " + userid + " </strong> </td> </tr>");
            sb.Append("<tr bgcolor='#f4f4f4'><td>Password :</td><td><strong> " + password + " </strong> </td></tr>");
            sb.Append("<tr bgcolor='#f4f4f4'><td colspan='2' align='center' height='5'></td></tr>");
            sb.Append("<tr><td colspan='2' align='center' height='1'></td></tr>");
            sb.Append("<tr><td colspan='2'>Regards,<br /><strong><font color='#04869a'>Alchemy</font></strong></td></tr>");
            sb.Append("<tr><td colspan='2' align='center' height='1'></td></tr>");
            sb.Append("</table>");

            String[] emailid = new String[] { ToEmailid };


            bool result = utility.SendEmail(sb.ToString(), emailid, "Alchemy Login Details", "", null);

            if (result)
            {
                response = "sent";
            }
            else
            {
                response = "";
            }


        }
        else
        {
            response = "";
        }
        return response;
    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            imgInnerBanner.ImageUrl = "Content/uploads/InnerBanner/" + dt.Rows[0]["Image"].ToString();
        }
        else
        {
            imgInnerBanner.ImageUrl = "images/banner/about_banner.jpg";
        }


    }
}