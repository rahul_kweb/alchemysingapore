﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class long_term_fund_ltd : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["prevUrl"] = Request.Url.AbsolutePath;

            BindStrategy(4);
            BindTerms(5);
            BindStructure(6);
            BindInnerBanner(5);
            BindFund();
            Bindpresentation();
        }
    }

    public string BindStrategy(int Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS'," + Id + "");
        if (dt.Rows.Count > 0)
        {
            ltrStrategy.Text = dt.Rows[0]["Description"].ToString();
        }
        return ltrStrategy.Text;
    }

    public string BindTerms(int Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS'," + Id + "");
        if (dt.Rows.Count > 0)
        {
            ltrTerms.Text = dt.Rows[0]["Description"].ToString();
        }
        return ltrTerms.Text;
    }

    public string BindStructure(int Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS'," + Id + "");
        if (dt.Rows.Count > 0)
        {
            ltrStructure.Text = dt.Rows[0]["Description"].ToString();
        }
        return ltrStructure.Text;
    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            imgInnerBanner.ImageUrl = "Content/uploads/InnerBanner/" + dt.Rows[0]["Image"].ToString();
        }
        else
        {
            imgInnerBanner.ImageUrl = "images/banner/about_banner.jpg";
        }
    }

    public void BindFund()
    {
        StringBuilder strDesc = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("Execute stp_FundReportDetails 'BindFund'");

        strDesc.Append("<div class=\"presentation\"><strong>Newsletters:</strong></div>");

        if (dt.Rows.Count > 0)
        {
            int i = 1;
            foreach (DataRow dr in dt.Rows)
            {

                DataTable dt1 = new DataTable();
                dt1 = utility.Display("Exec stp_FundReportDetails 'BindFundDetails',0,'" + dr["FundId"].ToString() + "'");

                if (i == 1)
                {
                    strDesc.Append("<div class=\"heading\">");
                    strDesc.Append("<span class=\"rounded-circle circle d-inline-block mr-3\">" + i + "</span>");
                    strDesc.Append("<strong>" + dr["Title"].ToString() + "</strong>");
                    strDesc.Append("<i class=\"fa fa-minus\" aria-hidden=\"true\"></i>");
                    strDesc.Append("</div>");

                    strDesc.Append("<div class=\"content newCont\" style=\"display:block;\">");
                }
                else
                {
                    strDesc.Append("<div class=\"heading\">");
                    strDesc.Append("<span class=\"rounded-circle circle d-inline-block mr-3\">" + i + "</span>");
                    strDesc.Append("<strong>" + dr["Title"].ToString() + "</strong>");
                    strDesc.Append("<i class=\"fa fa-plus\" aria-hidden=\"true\"></i>");
                    strDesc.Append("</div>");

                    strDesc.Append("<div class=\"content\">");
                }

                i++;
                foreach (DataRow dr1 in dt1.Rows)
                {
                    strDesc.Append("<div class=\"report\">");
                    strDesc.Append("<a href=\"Content/uploads/PDF/" + dr1["Pdf"].ToString() + "\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\" aria-hidden=\"true\"></i>" + dr1["Heading"].ToString() + "</a>");
                    strDesc.Append("</div>");
                }

                strDesc.Append("<div class=\"brown-dashed-border\"></div>");
                strDesc.Append("</div>");

            }
            ltrDescription.Text = strDesc.ToString();
        }
    }


    public void Bindpresentation()
    {
        StringBuilder strDesc = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_Presentation 'BindpresentaionFront'");

        strDesc.Append("<div class=\"presentation\"><strong>Presentation:</strong></div>");

        if (dt.Rows.Count > 0)
        {          

            foreach (DataRow dr in dt.Rows)
            {
                strDesc.Append("<div class=\"report\">");
                strDesc.Append("<a href=\"Content/uploads/Presentation/" + dr["Pdf"].ToString() + "\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\" style=\"margin-left: 5px;\" aria-hidden=\"true\"></i>" + dr["Heading"].ToString() + "</a>");
                strDesc.Append("</div>");
                strDesc.Append("<div class=\"brown-dashed-border\"></div>");

            }
        }

        ltrPresentation.Text = strDesc.ToString();

    }

}