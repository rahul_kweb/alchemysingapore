﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="equity_market_outlook_june_2020.aspx.cs" Inherits="equity_market_outlook_JUNE_2020" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="banner" class="position-relative mb-5">
        <h1>Thought Leadership</h1>
        <asp:Image ID="imgInnerBanner" runat="server" ImageUrl="images/banner/about_banner.jpg" class="img-fluid" alt="" />

    </div>

    <div class="container mb-4">
        <div class="row">
            <div class="col-12">


                <asp:Literal ID="ltrBreakcum" runat="server"></asp:Literal>


            </div>
        </div>

        <asp:Literal ID="ltrContent" runat="server"></asp:Literal>

        <div class="brown-dashed-border mb-5"></div>


    </div>
    <script>
        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

    </script>
</asp:Content>
