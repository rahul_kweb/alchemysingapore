﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class aboutUs : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindContent(1);
            BindInnerBanner(1);
            BindCsr(1);
        }

    }

    public string BindContent(int Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS'," + Id + "");
        if (dt.Rows.Count > 0)
        {
            ltrContent.Text = dt.Rows[0]["Description"].ToString();
        }
        return ltrContent.Text;
    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            imgInnerBanner.ImageUrl = "Content/uploads/InnerBanner/" + dt.Rows[0]["Image"].ToString();
        }
        else
        {
            imgInnerBanner.ImageUrl = "images/banner/about_banner.jpg";
        }


    }

    public void BindCsr(int Id)
    {
        StringBuilder strTitle = new StringBuilder();
        StringBuilder strDesc = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_PageCMSMaster 'bindPageContent',0,'" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                strTitle.Append("<li>" + dr["Title"].ToString() + "</li>");

                strDesc.Append(dr["Description"].ToString());

            }
            ltrHeading.Text = strTitle.ToString();
            ltrDescription.Text = strDesc.ToString();
        }
    }
}