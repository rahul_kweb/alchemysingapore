﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="contact_us.aspx.cs" Inherits="contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="banner" class="position-relative mb-5">
        <h1>Contact Us</h1>
        <asp:Image ID="imgInnerBanner" runat="server" ImageUrl="images/banner/about_banner.jpg" class="img-fluid" alt="" />

        <%--<img src="images/banner/contact_banner.jpg" class="img-fluid" alt="">--%>
    </div>



    <div class="container mb-4" id="contact">
        <div class="row innerPageData mb-4 justify-content-between">
            <div class="col-md-6">
                <div class="mb-5">
                    <h4 class="brownColor borderBottom">
                        <span class="mr-2">
                            <img src="images/investor_ico.png"></span>INVESTOR CONTACT
                    </h4>

                    <asp:Literal ID="ltrInvestorContact" runat="server"></asp:Literal>

                    <%-- <h5><i class="fa fa-phone" aria-hidden="true"></i><strong>+65 6829-7123/7124</strong></h5>

                    <h5><i class="fa fa-fax" aria-hidden="true"></i><strong>+65 6909-8562</strong></h5>

                    <h5>For existing Investors / Clients queries, please write to <strong><a href="mailto:clientservicing@alchemysingapore.com">clientservicing@alchemysingapore.com</a></strong></h5>
                    <h5>If you wish to take advantage of Alchemy Investment Management’s experience, please write to us at <strong><a href="mailto:contactus@alchemysingapore.com">contactus@alchemysingapore.com</a></strong></h5>
                    --%>
                </div>

                <div class="mb-5">
                    <h4 class="brownColor borderBottom">
                        <span class="mr-2">
                            <img src="images/map.png"></span>Registered Address
                    </h4>

                    <asp:Literal ID="ltrRegisteredAddress" runat="server"></asp:Literal>
                    <%--<h5 class="text-uppercase"><strong>Alchemy Investment<br>
                        Management Pte Ltd.</strong></h5>

                    <h5>30 Cecil Street, #19-08 Prudential Tower
Singapore 049712</h5>--%>
                </div>

                <div class="mb-5">
                    <h4 class="brownColor borderBottom">
                        <span class="mr-2">
                            <img src="images/map.png"></span>Office Address
                    </h4>

                    <asp:Literal ID="ltrOfficeAddress" runat="server"></asp:Literal>
                    <%--<h5 class="text-uppercase"><strong>Alchemy Investment<br>
                        Management Pte Ltd.</strong></h5>

                    <h5>37th Floor, Singapore Land Tower,
50 Raffles Place, Singapore 048623</h5>--%>
                </div>
            </div>

            <div class="col-md-6">
                <div id="map2" class="h-100"></div>
                <!--<img src="images/map.jpg" class="img-fluid" alt="">-->
            </div>

        </div>
    </div>

    <script>
        // Initialize and add the map
        function initMap() {
            var locations = [
                                ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
                                ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            // The location of Uluru
            var singapore_1 = { lat: -25.344, lng: 131.036 };
            var singapore_2 = { lat: -1.285006, lng: 103.851992 };
            var centerNew = { lat: 1.284309, lng: 103.851467 };

            // The map, centered at Uluru
            /*var map = new google.maps.Map(
                document.getElementById('map'), {zoom: 4, center: centerNew}
                );
            var map2 = new google.maps.Map(
                document.getElementById('map2'), {zoom: 4, center: centerNew}
                );*/

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });

            var map2 = new google.maps.Map(document.getElementById('map2'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });

            /*********************************************/



            var infowindow2 = new google.maps.InfoWindow();
            var image1 = "image";
            var marker1, i;
            for (i = 0; i < locations.length; i++) {
                marker1 = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map2,
                    icon: "images/map-icon.png"
                });

                google.maps.event.addListener(marker1, 'click', (function (marker1, i) {
                    return function () {
                        infowindow2.setContent(locations[i][0]);
                        infowindow2.open(map, marker1);
                    }
                })(marker1, i));
            }

            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });

                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));

            }

            /**********************************************/


            // The marker, positioned at Uluru
            //var marker = new google.maps.Marker({position: singapore_1, map: map});
            //var marker2 = new google.maps.Marker({position: singapore_2, map: map2});


        }

    </script>
</asp:Content>

