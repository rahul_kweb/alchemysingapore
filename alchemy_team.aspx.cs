﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ourTeam : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindInnerBanner(2);
            Team();
        }
    }


    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            imgInnerBanner.ImageUrl = "Content/uploads/InnerBanner/" + dt.Rows[0]["Image"].ToString();
        }
        else
        {
            imgInnerBanner.ImageUrl = "images/banner/about_banner.jpg";
        }
    }

    public void Team()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_PeopleCultureMaster 'bindPeopleCulture'");
        if (dt.Rows.Count > 0)
        {
            StringBuilder strTitle = new StringBuilder();
            StringBuilder strDesc = new StringBuilder();
            foreach (DataRow dr in dt.Rows)
            {
                strTitle.Append("<li>" + dr["Title"].ToString() + "</li>");


                strDesc.Append("<div class='content'>");

                    if (int.Parse(dr["PcmId"].ToString()) == 3)
                {
                    strDesc.Append("<h4>Alchemy Capital Management Pvt Ltd, India (Advisory Team)</h4>");

                    strDesc.Append("<p>Alchemy Investment Management Pte Ltd. receives non-binding advisory services from its parent entity, Alchemy Capital Management Pvt Ltd, India, (Alchemy India).</p>");
                }
                else
                {
                    strDesc.Append("<h4>" + dr["Title"].ToString() + "</h4>");
                }
                
                strDesc.Append("<div class='container p-0'>");

                DataTable dt1 = new DataTable();
                dt1 = utility.Display("Exec Proc_PeopleCultureMaster 'bindPeopleCultureById','" + dr["PcmId"].ToString() + "'");
                int i = 0;
                foreach (DataRow dr1 in dt1.Rows)
                {

                    if (i == 0)
                    {
                        strDesc.Append("<div class='row justify-content-between'>");
                    }

                    i++;

                    strDesc.Append("<div class='col-6 col-md-5 mb-5'>");

                    strDesc.Append("<div class='whiteBox mb-3'>");
                    strDesc.Append("<img src=\"Content/uploads/Team/" + dr1["Image"].ToString() + "\" class='img-fluid d-block' alt=''>");
                    strDesc.Append("</div>");

                    strDesc.Append("<h5>" + dr1["Name"].ToString() + " <span class='brownColor'>" + dr1["Designation"].ToString() + "</span></h5>");
                    strDesc.Append("<a href='" + dr1["slug"].ToString() + "' class='readMore brownBg'>Read More</a>");

                    strDesc.Append("</div>");

                    if (i == 2)
                    {
                        strDesc.Append("</div>");
                        i = 0;
                    }
                }

                string strChar = strDesc.ToString().Substring(strDesc.ToString().Length - 12);

                if (strChar != "</div></div>")
                {
                    strDesc.Append("</div>");
                }

              

                strDesc.Append("</div>");
                strDesc.Append("</div>");
            }
            ltrTabHeading.Text = strTitle.ToString();
            ltrFounder.Text = strDesc.ToString();
        }

    }
}