﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RequestACallback : System.Web.UI.UserControl
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {

        hdnutm_source.Value = Request.QueryString["utm_source"];

        hdnutm_medium.Value = Request.QueryString["utm_medium"];

        hdnutm_campaign.Value = Request.QueryString["utm_campaign"];

        hdnutm_term.Value = Request.QueryString["utm_term"];

        hdnutm_content.Value = Request.QueryString["utm_content"];

        hdnutm_device.Value = Request.QueryString["utm_device"];
    }

    protected void btnReqSubmit_Click(object sender, EventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("stp_RequestACallBack"))
        {
            string referrerVal = string.Empty;
            HttpCookie nameCookie = Request.Cookies["referrer"];
            if (nameCookie != null)
            {
                referrerVal = nameCookie["referrer"].ToString();
            }

            String ipAddress = System.Web.HttpContext.Current.Request.UserHostAddress;

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Para", "Add");
            cmd.Parameters.AddWithValue("@Name", txtName.Text);
            cmd.Parameters.AddWithValue("@PhoneNo", txtPhone.Text);
            cmd.Parameters.AddWithValue("@Email", txtEmail.Text);
            cmd.Parameters.AddWithValue("@Location", ddlLocation.SelectedValue);


            cmd.Parameters.AddWithValue("@Message", txtMessage.Text);

            cmd.Parameters.AddWithValue("@utm_source", hdnutm_source.Value);
            cmd.Parameters.AddWithValue("@utm_medium", hdnutm_medium.Value);
            cmd.Parameters.AddWithValue("@utm_campaign", hdnutm_campaign.Value);
            cmd.Parameters.AddWithValue("@utm_content", hdnutm_content.Value);
            cmd.Parameters.AddWithValue("@utm_device", hdnutm_device.Value);
            cmd.Parameters.AddWithValue("@utm_term", hdnutm_term.Value);
            cmd.Parameters.AddWithValue("@Referrer", referrerVal);
            cmd.Parameters.AddWithValue("@strUserIP", ipAddress);

            if (utility.Execute(cmd))
            {
                StringBuilder strbuild = new StringBuilder();
                strbuild.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ddd; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
                strbuild.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#2f72b4\"><strong><font color=\"#FFFFFF\">Request a callback </font></strong></td></tr>");
                strbuild.AppendFormat("<tr><td width=\"200px\"><strong>Name : </strong></td><td> " + txtName.Text.ToString().Trim() + " </td></tr>");
                strbuild.AppendFormat("<tr> <td><strong>Email Address : </strong></td> <td> " + txtEmail.Text.ToString().Trim() + " </td></tr>");
                strbuild.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Phone No: </strong></td> <td> " + txtPhone.Text.ToString().Trim() + " </td> </tr>");

                strbuild.AppendFormat("<tr> <td><strong>Continent / Country : </strong></td> <td> " + ddlLocation.SelectedItem.Text.ToString().Trim() + " </td></tr>");


                strbuild.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Message : </strong></td> <td> " + txtMessage.Text.ToString().Trim() + " </td> </tr>");
                //sb.AppendFormat("<tr> <td><strong>Request from Ascent Pop-up : </strong></td> <td> " + (string)(Session["popup_request"]) + " </td></tr>");

                strbuild.Append("</table>");

                string ToEmailId = ConfigurationManager.AppSettings["RequestUsEmail"].ToString();
                string[] emailid = new string[] { ToEmailId };
                utility.SendEmail(strbuild.ToString(), emailid, "Request A CallBack", "", null);
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "success", "alert('Your data is submitted,we will call you soon..')", true);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "success", "window.location = 'thank_you.aspx' ", true);
                Reset();

            }
        }
    }

    public void Reset()
    {
        txtName.Text = string.Empty;
        txtMessage.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtPhone.Text = string.Empty;
        ddlLocation.SelectedValue = "Continent / Country";
        //txtLocationOthers.Text = string.Empty;
    }
}