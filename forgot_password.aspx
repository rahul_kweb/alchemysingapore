﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="forgot_password.aspx.cs" Inherits="forgot_password" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="banner" class="position-relative mb-5">
        <h1>Forgot Password</h1>
        <asp:Image ID="imgInnerBanner" runat="server" ImageUrl="images/banner/about_banner.jpg" class="img-fluid" alt="" />

        <%--<img src="images/banner/login_banner.jpg" class="img-fluid" alt="">--%>
    </div>

    <div class="container mb-4">

        <div class="row mb-5" id="loginForm">
            <div class="col-12 col-md-8 col-lg-5">

                <div class="row">
                    <div class="col-12">
                        <div class="alert alert-warning alert-dismissible fade show" role="alert" style="display: none;">
                            <label id="lblmsg"></label>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="alert alert-danger alert-dismissible fade show" role="alert" id="divError" runat="server" visible="false">
                            <strong>
                                <asp:Label ID="lblError" runat="server"></asp:Label>
                            </strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="alert alert-success alert-dismissible fade show" role="alert" id="divSuccess" runat="server" visible="false">
                            Your username and password sent to your email id.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                        </div>
                    </div>
                    <div class="col-12">
                        <label>Your Email</label>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" autocomplete="off"></asp:TextBox>
                    </div>
                    <%--<div class="col-12">
                        <label>Mobile No</label>
                        <input type="text" class="form-control">
                    </div>--%>

                    <div class="col-12">
                        <div class="row align-items-center">
                            <div class="col-7">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="changePassBtn" Text="Submit" OnClick="btnSubmit_Click" OnClientClick="return checkValidation()" />

                            </div>
                            

                            <!--<div class="col-7 text-right logLinks">
            <span>
            <a href="#">Forgot Password</a> | <a href="registration.html">New Registration</a>
            </span>
        </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="brown-dashed-border mb-5"></div>


    </div>
    <script>
        function checkValidation() {
            var email = $('#<%=txtEmail.ClientID%>').val();
            var msg = "";
            var blank = false;

            if (email == '') {
                $('#<%=txtEmail.ClientID%>').css('border-color', 'red');
               msg += "Enter email address, ";
               blank = true;
           }
           else {
               var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

               if (!EmailText.test(email)) {
                   $('#<%=txtEmail.ClientID%>').css('border-color', 'red');
                    msg += "Invalid email address, ";
                    blank = true;
                }
                else {
                    $('#<%=txtEmail.ClientID%>').css('border-color', '');
                }
            }
            
            if (msg != "") {
                msg = msg.substring(0, msg.length - 2);
                $('#lblmsg').text(msg);
                $('.alert').slideDown('slow');
            } else {
                $('.alert').hide('slow');
            }

            if (blank) {
                return false;
            } else {
                return true;
            }
        }

        $(".close").click(function () {
            $('.alert').slideUp('slow');
        });

        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

    </script>
</asp:Content>

