﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="maintenance.aspx.cs" Inherits="maintenance" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
       <div class="row mb-5 innerPageData" id="loginForm">
            <div class="col-12 text-center pt-5 mb-4">
                <img src="images/undermaintainace.jpg" class="img-fluid m-auto" alt="">
            </div>

            <div class="col-12 text-center">
                <h4>Page Not Found</h4>

                <a href="index.aspx" class="readMore brownBg">Back to Home</a>
            </div>

        </div>
    </form>
</body>
</html>
