﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="vikas_kumar.aspx.cs" Inherits="vikas_kumar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="banner" class="position-relative mb-5">
        <h1>Alchemy
            Team</h1>
        <%--<img src="images/banner/about_banner.jpg" class="img-fluid" alt="">--%>
        <asp:Image ID="imgInnerBanner" runat="server" ImageUrl="images/banner/about_banner.jpg" class="img-fluid" alt="" />

    </div>

    <div class="container mb-4">
        <div class="row">
            <div class="col-12">

                <%--<ul class="breadcrumb">
                <li><a href="index.aspx">Home</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li><a href="our_team.aspx">Our Team</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li>Vikas Kumar</li>
                </ul>--%>

                <asp:Literal ID="ltrBreakcum" runat="server"></asp:Literal>


            </div>
        </div>

        <asp:Literal ID="ltrContent" runat="server"></asp:Literal>

        <%--<div class="row justify-content-center">
            <div class="col-xl-12">
                <div>
                    <!--<ul class="resp-tabs-list mb-4">
        <li>Group Founders</li>
        <li>Fund Management Team</li>
        <li>Advisory Team</li>

        </ul>-->

                    <div class="innerPageData" id="personData">
                        <div>
                            <h4>Advisory Team</h4>

                            <div class="whiteBox">
                                <div class="container p-0">
                                    <div class="row align-items-center">
                                        <div class="col-sm-6 col-md-4 col-lg-3">
                                            <img src="images/team/vikas_kumar.jpg" class="img-fluid" alt="">
                                        </div>

                                        <div class="col-sm-6 col-md-8 col-lg-9">
                                            <h4>Vikas Kumar     - <span class="brownColor">Portfolio Manager</span></h4>
                                            <p>
                                                With over 19 years of equity market experience, Mr. Vikas Kumar has expansive experience that includes equity analysis, private client fund management and strategy building on a sell-side institutional desk. He specializes in creating data-based quantitative algorithms 
 &amp; mathematically objective implementation strategies. His unique research paper on investment methodology, A Quantitative System for Reflexive Financial Markets, earned him a U.S. copyright. He pursued a BA in Math from Delhi University, qualified for Indian Institute of Technology Joint Entrance Examination (IIT-JEE), was a state-level National Talent Search Examination (NTSE) merit scholar and one of the national CBSE toppers. Prior to Alchemy, he worked with Dalal &amp; Broacha Stock Broking and collaborated at Reliance Capital. He was also nominated as one of the best Quant Analysts in Asia by Institutional Investor magazine in 2009.
                                            </p>
                                            <!--<a href="our_team.html" class="readMore brownBg">Back</a>-->
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>







                    </div>
                </div>
            </div>
        </div>--%>
    </div>

    <script>
        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

        /******************************************************************************/

        $(window).load(function () {

            var mainVal = sessionStorage.getItem("valueData");
            if (mainVal == "personData") {
                var scrollTop = $('#' + mainVal).offset().top;
                $('html, body').animate({
                    scrollTop: (scrollTop - 300)
                }, 500);
            }

            sessionStorage.removeItem("valueData");
            return false;

        })

    </script>
</asp:Content>

