﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="alchemy_team.aspx.cs" Inherits="ourTeam" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="banner" class="position-relative mb-5">
        <h1>Alchemy Team</h1>
        <asp:Image ID="imgInnerBanner" runat="server" ImageUrl="images/banner/about_banner.jpg" class="img-fluid" alt="" />
        <%--<img src="images/banner/about_banner.jpg" class="img-fluid" alt="">--%>
    </div>

    <div class="container mb-4">
        <div class="row">
            <div class="col-12">

                <ul class="breadcrumb">
                    <li><a href="index.aspx">Home</a></li>
                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                    <li><a href="about_us.aspx">About Us</a></li>
                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                    <li>Alchemy Team</li>
                </ul>

            </div>

        </div>

        <div class="row justify-content-center">
            <div class="col-xl-12">
                <div id="verticalTab">
                    <ul class="resp-tabs-list mb-4">
                        <asp:Literal ID="ltrTabHeading" runat="server"></asp:Literal>

                        <%--<li>Group Founders</li>
                        <li>Fund Management Team</li>
                        <li>Alchemy India (Advisory Team)</li>--%>
                    </ul>

                    <div class="resp-tabs-container innerPageData" id="teamData">
                        <asp:Literal ID="ltrFounder" runat="server"></asp:Literal>

                       <%-- <div class="content">
                            <h4>Group Founders</h4>

                            <div class="container p-0">
                                <div class="row justify-content-between">
                                    <div class="col-6 col-md-5 mb-5">
                                        <div class="whiteBox mb-3">
                                            <img src="images/team/lashit_sanghvi.jpg" class="img-fluid d-block" alt="">
                                        </div>
                                        <h5>Lashit Sanghvi <span class="brownColor">Co-Founder / CEO</span></h5>

                                        <a href="lashit_sanghvi.aspx" class="readMore brownBg">Read More</a>
                                    </div>

                                    <div class="col-6 col-md-5 mb-5">
                                        <div class="whiteBox mb-3">
                                            <img src="images/team/hiren_ved.jpg" class="img-fluid d-block" alt=""></div>
                                        <h5>Hiren Ved   <span class="brownColor">Co-Founder / CIO</span></h5>

                                        <a href="hiren_ved.aspx" class="readMore brownBg">Read More</a>
                                    </div>
                                </div>

                                <div class="row justify-content-between">
                                    <div class="col-6 col-md-5 mb-5">
                                        <div class="whiteBox mb-3">
                                            <img src="images/team/rakesh_jhunjhunwala.jpg" class="img-fluid d-block" alt=""></div>
                                        <h5>Rakesh Jhunjhunwala   <span class="brownColor">Co-Founder</span></h5>

                                        <a href="rakesh_jhunjhunwala.aspx" class="readMore brownBg">Read More</a>
                                    </div>

                                    <div class="col-6 col-md-5 mb-5">
                                        <div class="whiteBox mb-3">
                                            <img src="images/team/ashwin_kedia.jpg" class="img-fluid d-block" alt=""></div>
                                        <h5>Ashwin Kedia <span class="brownColor">Co-Founder</span></h5>

                                        <a href="ashwin_kedia.aspx" class="readMore brownBg">Read More</a>
                                    </div>



                                </div>
                            </div>

                        </div>

                        <div class="content">
                            <h4>Fund Management Team</h4>


                            <div class="container p-0">
                                <div class="row">
                                    <div class="col-6 col-md-5 mb-5">
                                        <div class="whiteBox mb-3">
                                            <img src="images/team/atul_sharma.jpg" class="img-fluid d-block" alt=""></div>
                                        <h5>Atul Sharma <span class="brownColor">Fund Manager</span></h5>
                                        <a href="atul_sharma.aspx" class="readMore brownBg">Read More</a>
                                    </div>

                                </div>
                            </div>




                        </div>

                        <div class="content">
                            <h4>Alchemy India (Advisory Team)</h4>

                            <div class="container p-0">
                                <div class="row justify-content-between">

                                    <div class="col-6 col-md-5 mb-5">
                                        <div class="whiteBox mb-3">
                                            <img src="images/team/hiren_ved.jpg" class="img-fluid d-block" alt=""></div>
                                        <h5>Hiren Ved   <span class="brownColor">Co-Founder / CIO</span></h5>

                                        <a href="hiren_ved_advisory_team.aspx" class="readMore brownBg">Read More</a>
                                    </div>

                                    <div class="col-6 col-md-5 mb-5">
                                        <div class="whiteBox mb-3">
                                            <img src="images/team/amit_nadekar.jpg" class="img-fluid d-block" alt=""></div>
                                        <h5>Amit Nadekar  <span class="brownColor">Portfolio Manager</span></h5>

                                        <a href="amit_nadekar.aspx" class="readMore brownBg">Read More</a>
                                    </div>

                                </div>

                                <div class="row justify-content-between">
                                    <div class="col-6 col-md-5 mb-5">
                                        <div class="whiteBox mb-3">
                                            <img src="images/team/seshadri_sen.jpg" class="img-fluid d-block" alt=""></div>
                                        <h5>Seshadri Sen   <span class="brownColor">Head of Research</span></h5>

                                        <a href="seshadri_sen.aspx" class="readMore brownBg">Read More</a>
                                    </div>



                                    <div class="col-6 col-md-5 mb-5">
                                        <div class="whiteBox mb-3">
                                            <img src="images/team/vikas_kumar.jpg" class="img-fluid d-block" alt=""></div>
                                        <h5>Vikas Kumar  <span class="brownColor">Portfolio Manager</span></h5>

                                        <a href="vikas_kumar.aspx" class="readMore brownBg">Read More</a>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-12 ">
                                        <div class="whiteBox">
                                            <p class="brownColor"><strong>ALCHEMY INDIA - Alchemy Capital Management Pvt Ltd</strong></p>
                                            <p class="brownColor"><strong>ALCHEMY SINGAPORE - Alchemy Investment Management Pte Ltd</strong></p>
                                            <p class="brownColor"><strong>ALCHEMY SINGAPORE receives non binding advisory services from its parent entity Alchemy Capital Management Pvt Ltd, India.</strong></p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>--%>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            //closed: 'accordion', // Start closed if in accordion view
            activate: function (event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });

        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }


        console.log(sessionStorage.getItem("valueData"))

    </script>
</asp:Content>

