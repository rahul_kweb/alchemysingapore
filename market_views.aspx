﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="market_views.aspx.cs" Inherits="market_views" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    	<style type="text/css">
	  .currentVid iframe{
	  	width: 100%; height: 400px;
	  }

	  ul.newsLetter a{
		color: #212529;	
	  }

	  ul.newsLetter a:hover{
	  	color: #a07628;
	  }

	  .newsDate{
	  	float: right; font-weight: bold; color: #1a8ad4;	
	  }
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       <div id="banner" class="position-relative mb-5">
        <h1>Market Views</h1>
        <asp:Image ID="imgInnerBanner" runat="server" ImageUrl="images/banner/about_banner.jpg" class="img-fluid" alt="" />

        <%--<img src="images/banner/process_banner.jpg" class="img-fluid" alt="">--%>
    </div>
        <div class="container mb-4">
               <div class="row">
            <div class="col-12">

                <ul class="breadcrumb">
                    <li><a href="index.aspx">Home</a></li>
                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                    <li>Insights</li>
                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                    <li>Market Views</li>
                </ul>

            </div>
        </div>

      <div class="row mb-5">
	<div class="col-12">
		<p>
		These materials discuss general market activity, industry or sector trends, or other broad-based economic, market or political conditions and should not be construed as research or investment advice.  Recipients are urged to consult with their financial advisors before buying or selling any securities.  Additionally, these materials do not constitute an offer to purchase interests in any private fund managed by Alchemy Investment Management Pte. Ltd. or its affiliates.
		</p>
	</div>
</div>

	<div class="row justify-content-center">
				<div class="resp-tabs-container-MarketViews innerPageData col-12">
                     <asp:Literal ID="ltrMarkets" runat="server"></asp:Literal>			
		</div>             
	</div>
            </div>




    <script>
        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            //closed: 'accordion', // Start closed if in accordion view
            activate: function (event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });


        var body = $("html, body");

        $('h2.resp-accordion').click(function () {
            var scrollTopVert = $(this).parent().offset().top;
            console.log(scrollTopVert);
            body.animate({ scrollTop: scrollTopVert }, 500, 'swing');
        });

        $('.offering').click(function () {
            var scrollTopVert = $('#alchemy_offering').offset().top - 100;
            body.animate({ scrollTop: scrollTopVert }, 500, 'swing');
        })

        /*******************************************************************/

        $('form select').change(function () {
            var selectedText = $(this).find("option:selected").text();
            console.log(selectedText);

            if (selectedText == 'Others') {

                $('.otherField').slideDown();
            } else {
                $('.otherField').slideUp();
            }

        })


        /*******************************************************************/

        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

	/**********************************/
 //$('.content').not(':first-child').hide();

 //$('.heading').click(function(){
 //	if($(this).children('.fa').hasClass('fa-plus')){
 //		$(this).children('.fa').removeClass('fa-plus');
 //		$(this).siblings('.heading').children('.fa').addClass('fa-plus');
 //		$(this).children('.fa').addClass('fa-minus');
 //		$(this).siblings('.heading').children('.fa').removeClass('fa-minus');
 //	}else{
 //		$(this).children('.fa').addClass('fa-plus');
 //   		 //$(this).siblings('.heading').children('.fa').removeClass('fa-plus');
 //   		 $(this).children('.fa').removeClass('fa-minus');
 //   		 //$(this).siblings('.heading').children('.fa').addClass('fa-minus');
 //   		} 	
 //   		$(this).next().siblings('.content').slideUp();
 //   		$(this).next().slideToggle();

 //   	})
 




	</script>




</asp:Content>



