﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlchemyProject
{
    public partial class RequestACallback : System.Web.UI.UserControl
    {
        Utility utility = new Utility();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnReqSubmit_Click(object sender, EventArgs e)
        {
            using (SqlCommand cmd = new SqlCommand("stp_RequestACallBack"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Add");
                cmd.Parameters.AddWithValue("@Name", txtName.Text);
                cmd.Parameters.AddWithValue("@PhoneNo", txtPhone.Text);
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text);
                if (ddlLocation.SelectedValue == "Others")
                {
                    cmd.Parameters.AddWithValue("@Location", txtLocationOthers.Text);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Location", ddlLocation.SelectedValue);
                }

                cmd.Parameters.AddWithValue("@Message", txtMessage.Text);

                if (utility.Execute(cmd))
                {
                    StringBuilder strbuild = new StringBuilder();
                    strbuild.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ddd; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
                    strbuild.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#2f72b4\"><strong><font color=\"#FFFFFF\">Request a callback </font></strong></td></tr>");
                    strbuild.AppendFormat("<tr><td width=\"200px\"><strong>Name : </strong></td><td> " + txtName.Text.ToString().Trim() + " </td></tr>");
                    strbuild.AppendFormat("<tr> <td><strong>Email Address : </strong></td> <td> " + txtEmail.Text.ToString().Trim() + " </td></tr>");
                    strbuild.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Phone No: </strong></td> <td> " + txtPhone.Text.ToString().Trim() + " </td> </tr>");
                    if (ddlLocation.SelectedValue == "Others")
                    {
                        strbuild.AppendFormat("<tr> <td><strong>Continent / Country : </strong></td> <td> " + txtLocationOthers.Text.ToString().Trim() + " </td></tr>");

                    }
                    else
                    {
                        strbuild.AppendFormat("<tr> <td><strong>Continent / Country : </strong></td> <td> " + ddlLocation.SelectedItem.Text.ToString().Trim() + " </td></tr>");
                    }

                    strbuild.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Message : </strong></td> <td> " + txtMessage.Text.ToString().Trim() + " </td> </tr>");
                    //sb.AppendFormat("<tr> <td><strong>Request from Ascent Pop-up : </strong></td> <td> " + (string)(Session["popup_request"]) + " </td></tr>");

                    strbuild.Append("</table>");

                    string ToEmailId = ConfigurationManager.AppSettings["RequestUsEmail"].ToString();
                    string[] emailid = new string[] { ToEmailId };
                    utility.SendEmail(strbuild.ToString(), emailid, "Request A CallBack", "", null);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "success", "alert('Your data is submitted,we will call you soon..')", true);
                    Reset();

                }
            }
        }

        public void Reset()
        {
            txtName.Text = string.Empty;
            txtMessage.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtPhone.Text = string.Empty;
            ddlLocation.SelectedValue = "Continent / Country";
            txtLocationOthers.Text = string.Empty;
        }
    }
}