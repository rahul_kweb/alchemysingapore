﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RequestACallback.ascx.cs" Inherits="AlchemyProject.RequestACallback" %>

<div class="row mb-5">
    <div class="col-sm-12 brownBorderNew pt-4 pb-4">
        <div class="row align-items-center">
            <div class="col-lg-2">
                <img src="/images/alchemy_white_logo.png" class="img-fluid">
            </div>
            <div class="col-lg-10">
                <h2 class="whiteColor">REQUEST A CALLBACK</h2>
                 <form>
                <div class="row">
                    <div class="col-md-4 mb-4">
                        <asp:TextBox ID="txtName" runat="server" placeholder="Name*" AutoComplete="off" CssClass="form-control"></asp:TextBox>
                        <span id="ErrorName" class="ErrorValidation">Name is required.</span>
                    </div>
                    <div class="col-md-4 mb-4">
                        <asp:TextBox ID="txtPhone" placeholder="Phone No." AutoComplete="off" runat="server" MinLength="10" MaxLength="12" onkeypress="return isNumberKey1(event)" CssClass="form-control"></asp:TextBox>
                        <span id="ErrorPhone" class="ErrorValidation">Phone No. is required.</span>
                    </div>
                    <div class="col-md-4 mb-4">
                        <asp:TextBox ID="txtEmail" runat="server" placeholder="Email*" autocomplete="off" CssClass="form-control"></asp:TextBox>
                        <span id="ErrorEmail1" class="ErrorValidation">Email is required.</span>
                        <span id="ErrorEmail2" class="ErrorValidation">Invalid email address.</span>
                    </div>

                    <div class="col-md-4 mb-4">
                        <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control ddllocation">
                            <asp:ListItem Value="Continent / Country" Text="Continent / Country*" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="USA" Text="USA"></asp:ListItem>
                            <asp:ListItem Value="CANADA" Text="CANADA">CANADA</asp:ListItem>
                            <asp:ListItem Value="EUROPE" Text="EUROPE">EUROPE</asp:ListItem>
                            <asp:ListItem Value="MIDDLE EAST" Text="MIDDLE EAST"></asp:ListItem>
                            <asp:ListItem Value="INDIA" Text="INDIA"></asp:ListItem>
                            <asp:ListItem Value="SOUTH EAST ASIA" Text="SOUTH EAST ASIA"></asp:ListItem>
                            <asp:ListItem Value="AUSTRALIA" Text="AUSTRALIA"></asp:ListItem>
                            <asp:ListItem Value="NEW ZEALAND" Text="NEW ZEALAND"></asp:ListItem>
                            <asp:ListItem Value="Others" Text="Others"></asp:ListItem>
                        </asp:DropDownList>
                        <span id="ErrorLocationddl" class="ErrorValidation"> Continent / Country is required.</span>

                         <asp:TextBox ID="txtLocationOthers" runat="server" class="form-control otherField mt-4" placeholder="Continent / Country" AutoComplete="off"></asp:TextBox>
                         <span id="ErrorLocationtxt" class="ErrorValidation"> Continent / Country is required.</span>
                    </div>                    
                 

                    <div class="col-md-8 mb-4">
                        <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" placeholder="Message..." AutoComplete="off" CssClass="form-control"></asp:TextBox>
                        <span id="ErrorMessage" class="ErrorValidation">Message is required.</span>
                        <span id="ErrorMessage1" class="ErrorValidation">Maximum of 500 characters.</span>
                    </div>

                    <div class="col-md-6 col-lg-4">
                        <label class="checkBox">
                            I authorize Alchemy Capital Management to contact me. This will override registry on the NDNC.
                 <input class="" id="check" type="checkbox" checked="checked" />
                            <span class="checkmark">
                                <small id="errorcheck" style="display: none">*</small>
                            </span>
                        </label>
                    </div>
                    <div class="col-md-4">
                        <asp:Button ID="btnReqSubmit" CssClass="brownBg submitBtn" Text="Submit" runat="server" type="submit" OnClick="btnReqSubmit_Click" OnClientClick="javascript:return validation();" />
                    </div>

                </div>
                     </form>
            </div>
        </div>
    </div>
</div>

<style>
    .ErrorValidation {
        display: none;
        color: #dc3545 !important;
        margin-bottom: 10px;      
    }

    .checkmark small {
        color: #dc3545;
        position: absolute;
        top: -17px;
        left: -12px;
        font-size: 25px;
    }
</style>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>--%>

<script type="text/javascript">

    function isNumberKey1(evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode;
        var textBox = document.getElementById('<%=txtPhone.ClientID%>').value.length;
        if (charCode > 31 && (charCode < 48 || charCode > 57) || textBox > 11)
            return false;

        return true;
    }

    //Form validation
    function validation() {
        var Name = $('#<%=txtName.ClientID%>').val();
        var Phone = $('#<%=txtPhone.ClientID%>').val();
        var Email = $('#<%=txtEmail.ClientID%>').val();
        var Message = $('#<%=txtMessage.ClientID%>').val();
        var Others = $('#<%=txtLocationOthers.ClientID%>').val();
        var blank = false;

        if (Name == '') {
            $('#ErrorName').css('display', 'block');
            blank = true;
        }
        else {
            $('#ErrorName').css('display', 'none');
        }

        if (Phone == '') {
            $('#ErrorPhone').css('display', 'block');
            blank = true;
        }
        else {
            $('#ErrorPhone').css('display', 'none');
        }

        if (Email == '') {
            $('#ErrorEmail1').css('display', 'block')
            $('#ErrorEmail2').css('display', 'none')
            blank = true;
        }
        else {

            var validEmail = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            if (!validEmail.test(Email)) {
                $('#ErrorEmail1').css('display', 'none')
                $('#ErrorEmail2').css('display', 'block')
                blank = true;
            }
            else {
                $('#ErrorEmail2').css('display', 'none')
            }
            $('#ErrorEmail1').css('display', 'none');
        }

        if ($('#<%=ddlLocation.ClientID%>').val() == 'Continent / Country') {
            $('#ErrorLocationddl').css('display', 'block');
            blank = true;
        }
        else {
            if ($('#<%=ddlLocation.ClientID%>').val() == 'Others') {
                if (Others == '') {
                    $('#ErrorLocationtxt').css('display', 'block');
                    blank = true;
                }
                else {

                    $('#ErrorLocationtxt').css('display', 'none');
                }
            }
            else {
                $('#ErrorLocationtxt').css('display', 'none');
            }
            $('#ErrorLocationddl').css('display', 'none');

        }

        if (Message == '') {
            $('#ErrorMessage').css('display', 'block');
            blank = true;

        }
        else {
            if (Message.length > 500) {
                $('#ErrorMessage1').css('display', 'block');
                blank = true;
            } else {
                $('#ErrorMessage1').css('display', 'none');
            }
            $('#ErrorMessage').css('display', 'none');
        }

        if (check.checked == false) {
            $('#errorcheck').css('display', 'block');
            Blank1 = true;
        }
        else {
            $('#errorcheck').css('display', 'none');
        }

        if (blank) {
            return false;
        }
        else {
            return true;
        }
    }

    /*******************************************************************/
        //Dropdownlist Selectedchange event
        //alert("Hi");
        //$('#ddlLocation').change(function () {
        //    debugger;
        //    // Get Dropdownlist seleted item text
        //    var selectedText = $("#ddlLocation option:selected").val();
        //    // Get Dropdownlist selected item value      
        //    console.log(selectedText);
        //    return false;
        //})

        $('.ddllocation').change(function () {
            var selectedText = $(this).find("option:selected").text();
            if (selectedText == 'Others') {

                $('.otherField').slideDown();
            } else {
                $('.otherField').slideUp();
            }
        })


</script>