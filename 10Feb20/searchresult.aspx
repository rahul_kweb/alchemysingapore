﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="search_result.aspx.cs" Inherits="search_result" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    
    <div class="container mb-4">
<div id="banner" class="position-relative mb-5">
        <h1>Search Result</h1>
        <img src="images/banner/contact.jpg" class="img-fluid" alt="">
    </div>
        <div class="row justify-content-center">
            <div class="col-xl-12">
                <div>
                    <!--<ul class="resp-tabs-list mb-4">
        <li>Group Founders</li>
        <li>Fund Management Team</li>
        <li>Advisory Team</li>

        </ul>-->

                    <div class="innerPageData" id="searchData">
                        <div>
                            <div class="whiteBox mt-5 p-4">
                                <div class="container p-0">
                                    <div class="row">
                                        <asp:Literal ID="ltrSearch" runat="server"></asp:Literal>
                                        <%--<div class="col-12 brown-dashed-border">
                                            <h4>Search Result</h4>
                                            <h5 class="text-uppercase"><i class="fa fa-file-text-o mr-2 mb-3" aria-hidden="true"></i>EQUITY MARKET OUTLOOK - NOVEMBER 2019</h5>

                                            <p><a href="#">http://www.alchemysingapore.com/equity_market_outlook_nov_2019.html</a> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>

                                        <div class="col-12 brown-dashed-border">
                                            <h4>Search Result</h4>
                                            <h5 class="text-uppercase"><i class="fa fa-file-text-o mr-2 mb-3" aria-hidden="true"></i>EQUITY MARKET OUTLOOK - NOVEMBER 2019</h5>

                                            <p><a href="#">http://www.alchemysingapore.com/equity_market_outlook_nov_2019.html</a> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>

                                        <div class="col-12 brown-dashed-border">
                                            <h4>Search Result</h4>
                                            <h5 class="text-uppercase"><i class="fa fa-file-text-o mr-2 mb-3" aria-hidden="true"></i>EQUITY MARKET OUTLOOK - NOVEMBER 2019</h5>

                                            <p><a href="#">http://www.alchemysingapore.com/equity_market_outlook_nov_2019.html</a> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>--%>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

