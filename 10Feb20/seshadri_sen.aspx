﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="seshadri_sen.aspx.cs" Inherits="seshadri_sen" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="banner" class="position-relative mb-5">
        <h1>Alchemy
            Team</h1>
        <img src="images/banner/about_banner.jpg" class="img-fluid" alt="">
    </div>

    <div class="container mb-4">
        <div class="row">
            <div class="col-12">

                <ul class="breadcrumb">
                <li><a href="index.aspx">Home</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li><a href="our_team.aspx">Our Team</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li>Seshadri Sen</li>
                </ul>

            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-12">
                <div>
                    <!--<ul class="resp-tabs-list mb-4">
        <li>Group Founders</li>
        <li>Fund Management Team</li>
        <li>Advisory Team</li>

        </ul>-->

                    <div class="innerPageData" id="personData">
                        <div>
                            <h4>Advisory Team</h4>

                            <div class="whiteBox">
                                <div class="container p-0">
                                    <div class="row align-items-center">
                                        <div class="col-sm-6 col-md-4 col-lg-3">
                                            <img src="images/team/seshadri_sen.jpg" class="img-fluid" alt="">
                                        </div>

                                        <div class="col-sm-6 col-md-8 col-lg-9">
                                            <h4>Seshadri Sen  - <span class="brownColor">Head of Research</span></h4>
                                            <p>A CFA Charter holder, Mr. Seshadri Sen has been covering Indian markets since 1992. Prior to joining Alchemy India in 2018, he had served as the lead analyst at JP Morgan, covering banks and financials. Mr. Sen has over 25 years of experience behind him and some of his key assignments in the past included Macquarie Capital, Soc Gen, ICICI Prudential AMC and a previous stint with Alchemy. An alumnus of Presidency College in Kolkata, Mr. Sen also holds an MBA degree from XLRI Jamshedpur.</p>

                                            <!--<a href="our_team.html" class="readMore brownBg">Back</a>-->
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div> 


                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }
        /******************************************************************************************/

        $(window).load(function () {

            var mainVal = sessionStorage.getItem("valueData");
            if (mainVal == "personData") {
                var scrollTop = $('#' + mainVal).offset().top;
                $('html, body').animate({
                    scrollTop: (scrollTop - 300)
                }, 500);
            }

            sessionStorage.removeItem("valueData");
            return false;

        })
        
    </script>
</asp:Content>

