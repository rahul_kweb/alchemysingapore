﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="banner" class="position-relative mb-5">
        <div class="slider stick-dots">
            <div class="slide">
                <div class="slide__img">
                    <img src="" alt="" data-lazy="images/banner/Set-Image-1v.jpg" class="full-image animated" data-animation-in="zoomInImage" />
                </div>
                <div class="slide__content container">
                    <div class="slide__content--headings">
                        <h2 class="animated" data-animation-in="fadeInLeft">Unwavering Business Practices  for Long-term Wealth Creation</h2>
                        <div class="clearfix"></div>
                        <!--<p class="animated" data-animation-in="fadeInRight" data-delay-in="0.3">Lorem Ipsum is simply dummy text of the printing and
typesetting industry.</p>-->

                        <a href="#footer" class="connectBtn brownBg fadeAnim position-relative scrollTo">
                            <span>
                            <img src="images/connect_icon.png">
                            Let's Connect</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="slide">
                <div class="slide__img">
                    <img src="" alt="" data-lazy="images/banner/Set-Image-2v.jpg" class="full-image animated" data-animation-in="zoomInImage" />
                </div>
                <div class="slide__content container">
                    <div class="slide__content--headings">
                        <h2 class="animated" data-animation-in="fadeInLeft">Prepped by Research,
                            <br>
                            Honed by Practice</h2>
                        <div class="clearfix"></div>
                        <!--<p class="animated" data-animation-in="fadeInRight" data-delay-in="0.3">Lorem Ipsum is simply dummy text of the printing and
typesetting industry.</p>-->

                        <a href="#footer" class="connectBtn brownBg fadeAnim position-relative scrollTo"><span>
                            <img src="images/connect_icon.png">
                            Let's Connect</span></a>
                    </div>
                </div>
            </div>

            <div class="slide">
                <div class="slide__img">
                    <img src="" alt="" data-lazy="images/banner/Set-Image-3v.jpg" class="full-image animated" data-animation-in="zoomInImage" />
                </div>
                <div class="slide__content container">
                    <div class="slide__content--headings">
                        <h2 class="animated" data-animation-in="fadeInLeft">An Association Built on Trust, Expertise and Insight</h2>
                        <div class="clearfix"></div>
                        <!--<p class="animated" data-animation-in="fadeInRight" data-delay-in="0.3">Lorem Ipsum is simply dummy text of the printing and
typesetting industry.</p>-->

                        <a href="#footer" class="connectBtn brownBg fadeAnim position-relative scrollTo">
                            <span>
                                <img src="images/connect_icon.png">
                                Let's Connect</span></a>
                    </div>
                </div>
            </div>

            <div class="slide">
                <div class="slide__img">
                    <img src="" alt="" data-lazy="images/banner/Set-Image-4v.jpg" class="full-image animated" data-animation-in="zoomInImage" />
                </div>
                <div class="slide__content container">
                    <div class="slide__content--headings">
                        <h2 class="animated" data-animation-in="fadeInLeft">Customized PMS Services For Optimum Returns</h2>
                        <div class="clearfix"></div>
                        <!--<p class="animated" data-animation-in="fadeInRight" data-delay-in="0.3">Lorem Ipsum is simply dummy text of the printing and
typesetting industry.</p>-->

                        <a href="#footer" class="connectBtn brownBg fadeAnim position-relative scrollTo">
                            <span>
                                <img src="images/connect_icon.png">
                                Let's Connect</span></a>
                    </div>
                </div>
            </div>

        </div>

        <div class="numberData container">
            <span class="activeNum">0</span> / <span class="totalNum">0</span>
        </div>
    </div>

    <div class="container aos-item" id="aboutSec" data-aos="fade-up">
        <div class="row justify-content-center mb-5 blackPara">
            <div class="col-lg-7 text-center aboutPara">
                <h1 class="text-uppercase brownColor">About us</h1>
                <p>Alchemy Investment Management Pte Ltd, Singapore (Alchemy Singapore) was incorporated in February 2007 as a 100% subsidiary of Alchemy Capital Management Pvt Ltd, India to provide Investment Management and Advisory Services to overseas investors / institutions.</p>
            </div>
        </div>

        <div class="row  mb-5">
            <div class="col-lg-4 text-center fadeAnim mb-3">

                <div class="box visionContainer">
                    <div class="icon vision">
                    </div>

                    <h2 class="text-uppercase brownColor">Vision</h2>

                    <p>To be a select asset and portfolio management firm through steady and excellent performance.</p>

                    <p>To be the investment manager of choice for Indian equities through delivering consistent long-term superior performance.</p>

                    <!--<a href="about.aspx#vision" class="readMore brownBg"><span>Read More</span></a>-->
                </div>
            </div>

            <div class="col-lg-4 text-center fadeAnim mb-3">
                <div class="box missionContainer">
                    <div class="icon mission">
                    </div>

                    <h2 class="text-uppercase brownColor">Mission</h2>

                    <p>To meet the long-term objectives of all our stakeholders through consistent superior performance, high standards of ethical behaviour and transparency with superior service and execution efficiency.</p>

                     <!--<a href="about.aspx#mission" class="readMore brownBg"><span>Read More</span></a>-->
                </div>
            </div>

            <div class="col-lg-4 text-center fadeAnim mb-3">
                <div class="box ethosContainer">
                    <div class="icon ethos">
                    </div>

                    <h2 class="text-uppercase brownColor">Ethos</h2>

                    <p>Where the mind is without fear and the head is held high.</p>

                    <p>Alchemy believes that knowledge is power. The firm stands on a culture of learning, excellence, integrity and insight that come together to produce results which meet and exceed the clientele’s expectations.</p>

                     <!--<a href="about.aspx#ethos" class="readMore brownBg"><span>Read More</span></a>-->
                </div>
            </div>
        </div>

    </div>

    <div class="container-fluid p-0 mb-5 aos-item" id="whyAlchemy" data-aos="fade-up">
        <div class="container centerDiv">
            <div class="row">
                <div class="col-lg-6 position-relative p-0">
                    <div class="grayBg p-5 whitePara whyAlchemyCont text-left">
                        <h1 class="text-uppercase">Why Alchemy</h1>

                        <p class="text-justify">Alchemy has been one of India’s successful portfolio management firms having a track record since 2002. The firm was founded by Lashit Sanghvi, Ashwin Kedia, Rakesh Jhunjhunwala and Hiren Ved, with each of the firm’s co-founders having over 25 years of experience each in investing in India’s equity market. The team has worked together for more than a decade and has instilled a culture of excellence and cultivated an institutional pedigree that has consistently resulted in superior performance over the last 17 years.</p>

                        <a href="advantage.aspx" class="readMore brownBg"><span>Read More</span></a>
                    </div>
                </div>
            </div>
        </div>

        <img src="images/why_alchemy.jpg" class="img-fluid" alt="Why Alchemy">
    </div>

    <div class="grayBorder mb-5"></div>

    <div class="container mb-4 aos-item" data-aos="fade-up">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center mb-4">
                <h1 class="brownColor text-uppercase">Our Team</h1>
                <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>-->
            </div>

            <div class="col-xl-11">
                <div id="horizontalTab">
                    <ul class="resp-tabs-list mb-4">
                        <li class="col-md-4">Group Founders</li>
                        <li class="col-md-4">Fund Management Team</li>
                        <li class="col-md-4">Advisory Team</li>
                    </ul>

                    <div class="resp-tabs-container team">
                        <div>
                            <div class="row">
                                <div class="col-md-4 col-lg-3 mb-4 text-center">
                                    <!--<a  data-src="#lashit" href="javascript:;" data-fancybox>-->
                                    <a href="lashit_sanghvi.aspx">
                                        <div class="brownBorder fadeAnim mb-4">
                                            <img src="images/team/lashit_sanghvi.jpg" class="img-fluid mb-3" alt="">
                                            <h5 class="text-uppercase">Lashit Sanghvi </h5>
                                            <p class="text-uppercase">Co-Founder / CEO</p>
                                        </div>
                                        <div class="plusIcon rounded-circle brownBg">
                                            <span>+</span>
                                        </div>
                                    </a>

                                    <div class="professorInfo container" id="lashit">
                                        <div class="row align-items-center">
                                            <div class="col-md-4">
                                                <img src="images/team/lashit_sanghvi.jpg" class="img-fluid mb-3" alt="">
                                            </div>
                                            <div class="col-md-8">
                                                <h5 class="text-uppercase">Lashit Sanghvi</h5>
                                                <p class="borderBottom">Co-Founder / CEO and Whole-Time Director</p>
                                                <p>Lashit Sanghvi, Director &amp; CEO: Mr. Lashit Sanghvi has over two decades of experience and has been featured in CNBC’s “Wizards of Dalal Street”, as one of India’s successful investors. Besides public equities, his area of expertise also involves development of systems critical for running a high-performance investment management business.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-3 mb-4 text-center">
                                    <!--<a  data-src="#hiren" href="javascript:;" data-fancybox>-->
                                    <a href="hiren_ved.aspx">
                                        <div class="brownBorder fadeAnim mb-4">
                                            <img src="images/team/hiren_ved.jpg" class="img-fluid mb-3" alt="">
                                            <h5 class="text-uppercase">Hiren Ved  </h5>
                                            <p class="text-uppercase">Co-Founder / CIO</p>
                                        </div>

                                        <div class="plusIcon rounded-circle brownBg">
                                            <span>+</span>
                                        </div>
                                    </a>

                                    <div class="professorInfo container" id="hiren">
                                        <div class="row align-items-center">
                                            <div class="col-md-4">
                                                <img src="images/team/hiren_ved.jpg" class="img-fluid mb-3" alt="">
                                            </div>
                                            <div class="col-md-8">
                                                <h5 class="text-uppercase">Hiren Ved</h5>
                                                <p class="borderBottom">Co-Founder / CEO and Whole-Time Director</p>
                                                <p>
                                                    Hiren Ved, Director
                                                    <!--and CIO:-->
                                                    Mr. Hiren Ved started his career by working with KR Choksey &amp; Co, a sell side firm known for its value investment approach, and eventually graduated to become the firm’s Head of Research. He brings over two decades of experience in bottom-up stock picking, managing portfolios and managing a high-performance investment team capable of delivering consistent, superior alpha.
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4 col-lg-3 mb-4 text-center">
                                    <!--<a  data-src="#rakesh" href="javascript:;" data-fancybox>-->
                                    <a href="rakesh_jhunjhunwala.aspx">
                                        <div class="brownBorder fadeAnim mb-4">
                                            <img src="images/team/rakesh_jhunjhunwala.jpg" class="img-fluid mb-3" alt="">
                                            <h5 class="text-uppercase">Rakesh Jhunjhunwala </h5>
                                            <p class="text-uppercase">Co-Founder</p>
                                        </div>

                                        <div class="plusIcon rounded-circle brownBg">
                                            <span>+</span>
                                        </div>
                                    </a>

                                    <div class="professorInfo container" id="rakesh">
                                        <div class="row align-items-center">
                                            <div class="col-md-4">
                                                <img src="images/team/rakesh_jhunjhunwala.jpg" class="img-fluid mb-3" alt="">
                                            </div>
                                            <div class="col-md-8">
                                                <h5 class="text-uppercase">Rakesh Jhunjhunwala</h5>
                                                <p class="borderBottom">Co-Founder</p>
                                                <p>Rakesh Jhunjhunwala: An experienced, longstanding investor in the Indian capital markets. Mr. Rakesh Jhunjhunwala has made all his wealth by investing and trading in the Indian markets for over 30 years. While he is not involved in the day-to-day functioning of the firm, his mentorship and guidance has helped the group develop its consistent ethos and performance.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-3 mb-4 text-center">
                                    <!--<a  data-src="#ashwin" href="javascript:;" data-fancybox>-->
                                    <a href="ashwin_kedia.aspx">
                                        <div class="brownBorder fadeAnim mb-4">
                                            <img src="images/team/ashwin_kedia.jpg" class="img-fluid mb-3" alt="">
                                            <h5 class="text-uppercase">Ashwin Kedia </h5>
                                            <p class="text-uppercase">Co-Founder</p>
                                        </div>

                                        <div class="plusIcon rounded-circle brownBg">
                                            <span>+</span>
                                        </div>
                                    </a>
                                    <div class="professorInfo container" id="ashwin">
                                        <div class="row align-items-center">
                                            <div class="col-md-4">
                                                <img src="images/team/ashwin_kedia.jpg" class="img-fluid mb-3" alt="">
                                            </div>
                                            <div class="col-md-8">
                                                <h5 class="text-uppercase">Ashwin Kedia</h5>
                                                <p class="borderBottom">Co-Founder</p>
                                                <p>Ashwin Kedia: He possesses over 25 years of comprehensive equity market experience. His key strengths are stock picking and developing corporate relationships. Ashwin has also featured on CNBC’s ‘’Wizards of Dalal Street- Gen Next’, a series on the most successful young investors in India.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="row">

                                <div class="col-md-4 col-lg-3 mb-4 text-center">
                                    <!--<a  data-src="#atul" href="javascript:;" data-fancybox>-->
                                    <a href="atul_sharma.aspx">
                                        <div class="brownBorder fadeAnim mb-4">
                                            <img src="images/team/atul_sharma.jpg" class="img-fluid mb-3" alt="">
                                            <h5 class="text-uppercase">Atul Sharma   </h5>
                                            <p class="text-uppercase">Fund Manager</p>
                                        </div>

                                        <div class="plusIcon rounded-circle brownBg">
                                            <span>+</span>
                                        </div>
                                    </a>

                                    <div class="professorInfo container" id="atul">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="images/team/atul_sharma.jpg" class="img-fluid mb-3" alt="">
                                            </div>
                                            <div class="col-md-8">
                                                <h5 class="text-uppercase">Atul Sharma </h5>
                                                <p class="borderBottom">Fund Manager</p>
                                                <p>Mr. Atul Sharma has over two decades of global business advisory and direct investment experience. Mr. Sharma has advised Asian Governments, Sovereign Wealth Funds, Conglomerates, MNCs and Multilateral Agencies across several industry sectors to address critical challenges related to economic development, creating economic profits / shareholder value, business strategy, growth, cost &amp; balance sheet management and corporate governance. He is a strong advocate of the fundamentals driven investment style that lays emphasis on the right combination of concepts / metrics like economic moats, ROIC, growth and valuation multiples. Prior to joining Alchemy, Mr. Sharma was an independent advisor helping clients identify and execute their private equity / direct investments and an adjunct faculty at a University in Singapore. Prior to this, Mr. Sharma worked for over 15 years in corporate finance and management consulting firms; including business leadership roles in Stern Stewart &amp; Co and advisory leadership roles in Accenture, Coopers &amp; Lybrand and Arthur Andersen. Mr. Sharma has an MBA degree from the Asian Institute of Management and a B.Sc. Physics degree from St. Xavier’s  College Mumbai.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div>
                            <div class="row">

                                <div class="col-md-4 col-lg-3 mb-4 text-center">
                                    <!--<a  data-src="#hiren_1" href="javascript:;" data-fancybox>-->
                                    <a href="hiren_ved_advisory_team.aspx" class="group_founders">
                                        <div class="brownBorder fadeAnim mb-4">
                                            <img src="images/team/hiren_ved.jpg" class="img-fluid mb-3" alt="">
                                            <h5 class="text-uppercase">Hiren Ved </h5>
                                            <p class="text-uppercase">Co-Founder / CIO </p>
                                        </div>

                                        <div class="plusIcon rounded-circle brownBg">
                                            <span>+</span>
                                        </div>
                                    </a>

                                    <div class="professorInfo container" id="hiren_1">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="images/team/hiren_ved.jpg" class="img-fluid mb-3" alt="">
                                            </div>
                                            <div class="col-md-8">
                                                <h5 class="text-uppercase">Hiren Ved </h5>
                                                <p class="borderBottom">
                                                    <!--Chief Investment Officer (CIO)-->
                                                    Co-Founder / CEO
                                                </p>
                                                <p>Mr. Hiren Ved joined Alchemy India in 1999, spearheading the firm’s asset management business. With over two decades of experience in equity markets, he has carved a niche in “Bottom-up” research and stock picking with extensive coverage of companies across various sectors. A certified cost accountant, he has over 25 years experience in the Indian equity markets. He has been a Co-Founder and Chief Investment Officer at Alchemy India since its inception.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-4 col-lg-3 mb-4 text-center">
                                    <!--<a  data-src="#amit" href="javascript:;" data-fancybox>-->
                                    <a href="amit_nadekar.aspx">
                                        <div class="brownBorder fadeAnim mb-4">
                                            <img src="images/team/amit_nadekar.jpg" class="img-fluid mb-3" alt="">
                                            <h5 class="text-uppercase">Amit Nadekar </h5>
                                            <p class="text-uppercase">Portfolio Manager</p>
                                        </div>

                                        <div class="plusIcon rounded-circle brownBg">
                                            <span>+</span>
                                        </div>
                                    </a>

                                    <div class="professorInfo container" id="amit">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="images/team/amit_nadekar.jpg" class="img-fluid mb-3" alt="">
                                            </div>
                                            <div class="col-md-8">
                                                <h5 class="text-uppercase">Amit Nadekar</h5>
                                                <p class="borderBottom">Portfolio Manager</p>
                                                <p>A Chartered Accountant by profession, Mr. Amit Nadekar has worked across equity research, corporate strategy, taxation and audit over the last one and a half decades. He started his career on the sell side, tracking the US banking & financial sector; later moving on to the corporate side as a part of the Corporate Strategy team at Raymond. He has been a part of the Alchemy India investment team since 2005.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-3 mb-4 text-center">
                                    <!--<a  data-src="#seshadri" href="javascript:;" data-fancybox>-->
                                    <a href="seshadri_sen.aspx">
                                        <div class="brownBorder fadeAnim mb-4">
                                            <img src="images/team/seshadri_sen.jpg" class="img-fluid mb-3" alt="">
                                            <h5 class="text-uppercase">Seshadri Sen</h5>
                                            <p class="text-uppercase">Head of Research</p>
                                        </div>

                                        <div class="plusIcon rounded-circle brownBg">
                                            <span>+</span>
                                        </div>
                                    </a>

                                    <div class="professorInfo container" id="seshadri">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="images/team/seshadri_sen.jpg" class="img-fluid mb-3" alt="">
                                            </div>
                                            <div class="col-md-8">
                                                <h5 class="text-uppercase">Seshadri Sen</h5>
                                                <p class="borderBottom">Head of Research</p>
                                                <p>A CFA Charterholder, Mr. Seshadri Sen has been covering Indian markets since 1992. Prior to joining Alchemy India in 2018, he had served as the lead analyst at JP Morgan, covering banks and financials. Mr. Sen has over 25 years of experience behind him and some of his key assignments in the past included Macquarie Capital, SocGen, ICICI Prudential AMC and a previous stint with Alchemy. An alumnus of Presidency College in Kolkata, Mr. Sen also holds an MBA degree from XLRI Jamshedpur.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                

                                <div class="col-md-4 col-lg-3 mb-4 text-center">
                                    <!--<a  data-src="#vikas" href="javascript:;" data-fancybox>-->
                                    <a href="vikas_kumar.aspx">
                                        <div class="brownBorder fadeAnim mb-4">
                                            <img src="images/team/vikas_kumar.jpg" class="img-fluid mb-3" alt="">
                                            <h5 class="text-uppercase">Vikas Kumar  </h5>
                                            <p class="text-uppercase">Portfolio Manager</p>
                                        </div>

                                        <div class="plusIcon rounded-circle brownBg">
                                            <span>+</span>
                                        </div>
                                    </a>

                                    <div class="professorInfo container" id="vikas">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="images/team/vikas_kumar.jpg" class="img-fluid mb-3" alt="">
                                            </div>
                                            <div class="col-md-8">
                                                <h5 class="text-uppercase">Vikas Kumar</h5>
                                                <p class="borderBottom">Portfolio Manager</p>
                                                <p>With over 19 years of equity market experience, Mr. Vikas Kumar has expansive experience that includes equity analysis, private client fund management and strategy building on a sell-side institutional desk. He specializes in creating data-based quantitative algorithms & mathematically objective implementation strategies. His unique research paper on investment methodology, A Quantitative System for Reflexive Financial Markets, earned him a U.S. copyright. He pursued a BA in Math from Delhi University, qualified for Indian Institute of Technology Joint Entrance Examination (IIT-JEE), was a state-level National Talent Search Examination (NTSE) merit scholar and one of the national CBSE toppers. Prior to Alchemy, he worked with Dalal & Broacha Stock Broking and collaborated at Reliance Capital. He was also nominated as one of the best Quant Analysts in Asia by Institutional Investor magazine in 2009.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="grayBorder mb-5"></div>

    <div class="container"  id="alchemy_offering">
        <div class="row justify-content-center">
            <div class="col-xl-9 text-center mb-5">
                <h1 class="brownColor text-uppercase">Alchemy Offering</h1>
                <p>Alchemy Investment Management Pte Ltd offers its fund management services in both Comingled Fund and Separately Managed Account format.</p>

            </div>

            <div class="col-xl-10">
                <div class="row">
                    <div class="col-lg-6 whitePara mb-5">
                        <div class="offeringImg mb-4">
                            <h5 class="whiteColor">Comingled Fund</h5>
                            <img src="images/combiled_fund.jpg" class="img-fluid fadeAnim">
                        </div>
                        <p class="text-justify">An interested investor, who is looking for actively managed Indian exposure, can invest through Alchemy’s pooled investment vehicle. </p>
                        <!-- <a class="readMore brownBg whiteColor" href="#">
         <span>Read More</span>
         </a> -->

                    </div>

                    <div class="col-lg-6 whitePara mb-5">
                        <div class="offeringImg mb-4">
                            <h5 class="whiteColor">Separate Managed Account</h5>
                            <img src="images/sma_1.jpg" class="img-fluid fadeAnim">
                        </div>
                        <p class="text-justify">Alchemy Investment Management Pte Ltd is open to providing its investment management and advisory expertise to a Separate Managed Account (SMA). </p>
                        <!-- <a class="readMore brownBg" href="#">
         <span>Read More</span>
         </a> -->

                    </div>

                    <div class="col-lg-12 text-center mb_5">
                        <a class="readMore brownBg whiteColor" href="our_product.aspx">
                            <span>Read More</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="grayBg height_245 mb-5"></div>

    <div class="container mb-5 aos-item" data-aos="fade-up" id="our_process">
        <div class="row">
            <div class="col-12 text-center mb-5">
                <h1 class="brownColor text-uppercase">Our Process</h1>
                <p>A Proven, Time-Tested Investment Process</p>
            </div>
        </div>
        <div class="row  processIcon justify-content-center">
            <div class="col-xl-10">
                <div class="row justify-content-between mb-3">
                    <div class="col-sm-6 col-md-4 col-lg-2 position-relative">
                        <div class="rounded-circle ">
                            <span>
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    width="79.68px" height="79.431px" viewBox="0 0 79.68 79.431" enable-background="new 0 0 79.68 79.431" xml:space="preserve">
                                    <g>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M54.165,57.707c1.448,1.448,2.32,3.038,2.292,5.063
		c-0.04,2.856-1.51,5.094-4.201,6.074c-0.697,0.253-0.968,0.624-1.116,1.32c-1.178,5.523-5.667,9.248-11.11,9.266
		c-5.755,0.02-10.242-3.51-11.498-9.09c-0.179-0.797-0.47-1.25-1.322-1.569c-4.056-1.522-5.369-6.642-2.65-10.052
		c0.271-0.341,0.567-0.662,0.879-1.024c-0.223-0.273-0.418-0.522-0.623-0.764c-2.344-2.754-2.238-6.436,0.261-9.005
		c0.99-1.017,0.869-3.354-0.212-4.221c-6.01-4.813-9.233-11.112-9.376-18.765C15.245,11.894,24.854,2.135,35.899,0.495
		c13.226-1.963,25.093,6.099,27.715,19.135c1.909,9.491-1.157,17.49-8.508,23.803c-1.445,1.242-1.647,3.319-0.357,4.706
		c2.211,2.376,2.327,6.065,0.268,8.566C54.765,57.012,54.502,57.311,54.165,57.707z M51.145,46.053
		c0.022-0.444,0.104-0.842,0.052-1.222c-0.188-1.388,0.426-2.212,1.529-3.072c6.41-5,9.37-11.587,8.529-19.724
		C59.998,9.879,48.29,0.956,36.305,3.19c-8.934,1.666-14.965,6.999-17.31,15.796c-2.333,8.758,0.275,16.288,7.172,22.161
		c1.484,1.265,2.588,2.376,2.222,4.418c-0.025,0.139,0.071,0.299,0.115,0.463c1.609,0,3.183,0,4.864,0
		c0-2.078-0.207-4.097,0.046-6.057c0.433-3.361-0.538-5.986-3.319-7.931c-0.097-0.068-0.17-0.168-0.256-0.252
		c-0.658-0.645-0.891-1.402-0.215-2.109c0.731-0.765,1.507-0.504,2.191,0.184c1.127,1.135,2.321,2.212,3.363,3.42
		c0.448,0.52,0.812,1.298,0.83,1.97c0.086,3.158,0.036,6.319,0.041,9.479c0.001,0.429,0.038,0.856,0.059,1.286
		c2.53,0,4.943,0,7.42,0c0-3.616-0.025-7.135,0.024-10.652c0.008-0.591,0.182-1.32,0.555-1.74c1.194-1.343,2.515-2.573,3.792-3.841
		c0.667-0.661,1.45-0.797,2.116-0.118c0.686,0.7,0.445,1.455-0.218,2.103c-0.686,0.671-1.308,1.419-2.05,2.02
		c-1.146,0.93-1.592,2.014-1.502,3.523c0.141,2.352,0.037,4.717,0.037,7.078c0,0.544,0,1.089,0,1.663
		C47.985,46.053,49.526,46.053,51.145,46.053z M39.96,48.873c-3.438,0-6.876-0.02-10.314,0.007
		c-2.258,0.017-3.867,1.726-3.759,3.918c0.104,2.133,1.793,3.544,4.315,3.551c5.517,0.014,11.034,0.005,16.552,0.004
		c1.119-0.001,2.245,0.059,3.356-0.035c2.248-0.19,3.779-1.895,3.655-3.958c-0.124-2.063-1.715-3.475-3.969-3.483
		C46.518,48.862,43.239,48.872,39.96,48.873z M39.721,66.517c3.319,0,6.637,0.006,9.957-0.002c2.365-0.006,4.104-1.612,4.09-3.756
		c-0.016-2.123-1.707-3.718-4.02-3.723c-6.638-0.014-13.275-0.015-19.913,0.001c-2.298,0.005-3.958,1.622-3.951,3.773
		c0.007,2.139,1.677,3.696,4,3.703C33.164,66.524,36.442,66.517,39.721,66.517z M31.148,69.351c0.632,4.259,4.525,7.517,8.798,7.395
		c5.178-0.147,8.21-4.205,8.513-7.395C42.716,69.351,36.98,69.351,31.148,69.351z" />
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M75.539,25.878c-0.796,0.001-1.592,0.013-2.388-0.001
		c-0.893-0.016-1.483-0.494-1.496-1.367c-0.014-0.879,0.566-1.37,1.467-1.376c1.631-0.012,3.263-0.021,4.894,0.004
		c0.888,0.014,1.422,0.464,1.408,1.406c-0.013,0.896-0.578,1.277-1.379,1.323C77.212,25.915,76.374,25.877,75.539,25.878z" />
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M4.097,25.877c-0.839,0.001-1.681,0.034-2.518-0.008
		c-0.825-0.042-1.352-0.475-1.368-1.352c-0.016-0.885,0.502-1.35,1.331-1.371c1.678-0.042,3.357-0.036,5.036-0.004
		c0.829,0.016,1.363,0.454,1.368,1.338c0.005,0.87-0.501,1.336-1.331,1.386C5.778,25.916,4.937,25.876,4.097,25.877z" />
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.305,43.336c0.134-0.174,0.338-0.646,0.697-0.871
		c1.417-0.886,2.874-1.709,4.333-2.524c0.722-0.403,1.414-0.3,1.871,0.425c0.497,0.787,0.25,1.474-0.511,1.932
		c-1.433,0.861-2.868,1.725-4.348,2.499C6.348,45.319,5.292,44.664,5.305,43.336z" />
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.305,5.58C5.279,4.376,6.263,3.669,7.233,4.152
		c1.598,0.795,3.157,1.691,4.627,2.7c0.355,0.243,0.58,1.284,0.357,1.61c-0.283,0.413-1.243,0.806-1.635,0.621
		C8.936,8.309,7.377,7.339,5.822,6.382C5.532,6.203,5.41,5.751,5.305,5.58z" />
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M68.383,39.625c0.542,0.21,0.895,0.303,1.201,0.475
		c1.285,0.72,2.559,1.459,3.831,2.202c0.772,0.45,1.248,1.071,0.746,1.964c-0.48,0.854-1.237,0.918-2.047,0.455
		c-1.417-0.81-2.874-1.564-4.207-2.496c-0.396-0.276-0.633-1.093-0.565-1.613C67.392,40.226,68.051,39.918,68.383,39.625z" />
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M67.205,7.638c0.148-0.193,0.368-0.695,0.752-0.939
		c1.341-0.851,2.727-1.634,4.119-2.401c0.77-0.424,1.564-0.474,2.072,0.402c0.515,0.887,0.064,1.516-0.71,1.969
		c-1.372,0.803-2.733,1.628-4.138,2.372C68.288,9.578,67.253,8.993,67.205,7.638z" />
                                    </g>
                                </svg>
                            </span>
                        </div>

                        <p>
                            IDEA
    GENERATION
                        </p>

                        <div class="curveArrow">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                width="401.023px" height="228.034px" viewBox="0 0 401.023 228.034" enable-background="new 0 0 401.023 228.034"
                                xml:space="preserve">
                                <defs>
                                    <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
                                        <stop offset="0%" style="stop-color: rgb(191,132,46); stop-opacity: 1" />
                                        <stop offset="100%" style="stop-color: rgb(233,177,97); stop-opacity: 1" />
                                    </linearGradient>
                                </defs>
                                <path fill-rule="evenodd" clip-rule="evenodd" fill="url(#grad1)" d="M0.539,20.521c0,0,58.861-49.482,128.52,5.228
        c21.508,13.667,105.184,130.676,107.621,132.768s67.219,101.407,146.281,44.953c2.438-1.951,2.09-4.182,2.09-4.182l-5.225-5.225
        h16.717c0,0,5.854-1.256,4.18,4.18c-0.418,2.51-3.135,15.682-3.135,15.682l-5.223-5.227l-3.135,1.045
        c0,0-43.117,32.896-92.994,10.453c-3.971-1.461-30.928-16.029-52.242-41.816c-2.719-2.508-48.064-61.68-48.064-61.68
        s-16.926-22.939-26.121-34.499c-11.016-13.845-24.034-29.271-24.034-29.271s-25.452-33.544-60.601-41.817
        C46.513,0.24,11.615,20.313,4.719,25.749C-2.212,27.612,0.539,20.521,0.539,20.521L0.539,20.521z" />
                            </svg>
                        </div>

                    </div>

                    <div class="col-sm-6 col-md-4 col-lg-2 position-relative">
                        <div class="rounded-circle">
                            <span>
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    width="671.792px" height="751.425px" viewBox="0 0 671.792 751.425" enable-background="new 0 0 671.792 751.425"
                                    xml:space="preserve">
                                    <g>
                                        <path fill="#fff" d="M100.342,746.624c49.752,12.877,93.941-0.176,131.334-38.792c4.359-4.502,8.809-7.017,16.286-4.737
            c56.657,17.376,114.68,17.435,172.442,0.177c8.5-2.545,14.259-0.982,20.537,5.551c33.947,35.338,74.658,48.75,121.012,39.885
            c53.508-10.235,89.508-42.986,104.106-94.708c14.75-52.279,1.003-98.32-40.861-136.843c-6.551-6.028-6.675-11.449-4.845-17.741
            c11.351-39.04,14.329-78.43,8.854-117.072c-13.691-96.666-62.793-170.739-145.938-220.158c-8.215-4.883-20.547-12.211-15.25-28.726
            c-0.197-2.056-0.374-4.075-0.548-6.066c-0.417-4.771-0.82-9.381-1.439-13.964c-4.669-34.569-24.076-66.629-53.242-87.966
            C389.653,8.537,361.949-0.489,334.34-0.489c-7.177,0-14.348,0.61-21.432,1.852C244.07,13.416,197.142,72.794,201.319,142.552
            c0.51,8.505-3.181,12.304-8.881,15.388C70.182,224.126,9.829,368.83,48.927,502.009c2.062,7.02,0.725,11.797-4.774,17.028
            C5.416,555.939-7.925,600.029,4.497,650.079C16.944,700.24,50.085,733.626,100.342,746.624z M191.426,711.292
            c-16.227,11.066-37.448,16.634-58.41,16.634c-20.014,0-39.792-5.069-54.787-15.269l-3.808-2.591l2.467-3.889
            c12.602-19.862,30.628-30.661,53.583-32.096c25.615-1.604,46.582,8.664,62.15,30.522l2.814,3.954L191.426,711.292z M104.745,621.64
            c0.368-15.577,13.28-28.102,28.866-28.102c0.136,0,0.27,0.004,0.402,0.004c7.614,0.104,14.791,3.247,20.212,8.846
            c5.365,5.542,8.245,12.716,8.109,20.196v0.003c-0.139,7.431-3.431,14.778-9.029,20.159c-5.4,5.189-12.537,8.137-19.661,8.137
            c-0.183,0-0.362-0.003-0.542-0.006c-7.484-0.158-14.877-3.495-20.293-9.159C107.517,636.18,104.575,628.861,104.745,621.64z
             M595.893,710.918c-14.679,11.227-36.563,16.884-58.261,16.884c-20.705,0-41.238-5.149-55.19-15.528l-3.665-2.728l2.579-3.771
            c14.613-21.36,34.404-32.215,59.112-31.844c23.149,0.214,42.127,10.459,56.406,30.444l2.676,3.747L595.893,710.918z
             M509.358,622.339c-0.056-7.542,2.91-14.71,8.354-20.178c5.483-5.511,12.716-8.545,20.369-8.545c0.022,0,0.044,0,0.068,0
            c7.586,0.019,15.058,3.207,20.5,8.75c5.415,5.514,8.335,12.659,8.217,20.115c-0.241,15.088-13.235,28.089-28.379,28.389
            c-0.17,0.003-0.341,0.003-0.512,0.003c-7.201,0-14.378-2.981-19.771-8.229C512.637,637.223,509.411,629.823,509.358,622.339z
             M648.81,612.789c1.096,28.432-9.014,57.238-26.383,75.175l-3.112,3.217l-41.481-35.557l2.371-3.515
            c9.044-13.391,11.667-28.435,7.589-43.507c-3.121-11.53-9.781-21.079-19.791-28.38c-19.453-14.196-46.746-12.49-64.89,4.06
            c-16.75,15.276-24.317,43.94-6.372,69.165l2.471,3.475l-40.16,35.752l-3.161-2.806c-29.222-25.921-44.774-92.188-3.72-141.229
            c20.879-24.937,47.264-39.267,76.306-41.439c29.895-2.254,60.243,8.775,85.369,31.015
            C635.271,557.177,647.689,583.662,648.81,612.789z M252.288,59.289c20.484-22.583,48.424-35.553,78.677-36.516
            c30.03-0.863,58.508,10.069,80.111,31.04c46.294,44.935,41.284,113.565,7.561,149.472l-3.121,3.318l-40.477-34.865l1.48-3.245
            c14.61-32.04,10.886-55.691-11.388-72.306c-19.273-14.372-46.371-13.449-64.432,2.177c-20.463,17.71-23.153,42.885-7.583,70.896
            l1.842,3.307l-39.848,35.925l-3.192-3.093C218.645,173.173,210.613,105.24,252.288,59.289z M333.209,189.359
            c24.51-0.811,47.242,10.902,59.441,30.455l2.232,3.579l-3.285,2.644c-14.38,11.57-35.148,17.416-56.426,17.416
            c-19.686,0-39.804-5.003-55.713-15.106l-3.737-2.375l2.103-3.898C288.47,202.345,309.174,190.115,333.209,189.359z
             M305.486,137.232c0.093-7.372,3.356-14.731,8.958-20.19c5.545-5.406,12.812-8.502,20.146-8.35
            c15.338,0.204,28.646,13.8,28.479,29.095c-0.08,7.338-3.294,14.67-8.821,20.106c-5.461,5.378-12.713,8.447-19.936,8.447
            c-0.093,0-0.186,0-0.279,0c-7.347-0.081-14.694-3.332-20.153-8.92C308.456,151.869,305.396,144.508,305.486,137.232z
             M202.346,178.845l4.406-2.358l2.137,4.517c27.521,58.159,71.398,86.635,130.256,84.842c54.685-1.728,95.146-30.228,120.269-84.708
            l2.093-4.537l4.427,2.311c109.537,57.152,167.668,194.672,132.341,313.08l-1.428,4.783l-4.709-1.656
            c-59.75-21.018-110.239-11.707-150.057,27.688c-42.089,41.656-51.446,93.164-27.804,153.088l1.914,4.849l-5.004,1.462
            c-25.397,7.418-50.728,11.128-76.336,11.128c-25.374,0-51.022-3.639-77.314-10.917l-5.1-1.413l1.948-4.919
            c23.404-59.193,14.602-110.193-26.163-151.577c-40.866-41.489-91.616-51.059-150.824-28.438l-4.632,1.769l-1.573-4.7
            C37.858,393.619,78.527,245.16,202.346,178.845z M46.373,549.929c18.961-24.24,46.213-39.174,76.739-42.053
            c30.531-2.866,60.128,6.713,83.313,27.002c22.391,19.602,35.674,46.892,37.401,76.842c1.65,28.691-7.707,56.508-25.671,76.314
            l-3.117,3.437l-41.622-35.929l2.439-3.514c9.069-13.072,11.567-28.352,7.233-44.186c-3.134-11.429-9.915-20.856-20.16-28.023
            c-19.717-13.794-46.725-11.901-64.224,4.483c-20.001,18.729-22.036,43.678-5.728,70.252l2.257,3.676l-3.44,2.604
            c-1.957,1.479-3.91,2.901-5.843,4.304c-4.257,3.093-8.276,6.016-11.92,9.289c-3.669,3.294-6.985,6.954-10.496,10.83
            c-1.555,1.715-3.112,3.431-4.697,5.114l-3.146,3.335l-3.449-3.019C23.501,665.522,7.361,599.799,46.373,549.929z" />
                                        <path fill="#fff" d="M98.884,102.144l-11.506,0.016l-11.787-0.012c-3.926-0.007-7.852-0.016-11.781-0.016
            c-5.22,0-10.443,0.012-15.663,0.053c-7.837,0.059-12.564,4.188-12.652,11.056c-0.04,3.263,0.972,6.043,2.933,8.041
            c2.155,2.195,5.344,3.366,9.217,3.384c26.168,0.133,52.329,0.155,78.495,0.068c3.882-0.016,7.084-1.177,9.251-3.366
            c1.966-1.985,2.994-4.762,2.975-8.032c-0.037-6.814-4.852-11.084-12.562-11.149C116.833,102.109,107.866,102.131,98.884,102.144z" />
                                        <path fill="#fff" d="M533.32,113.599c0.083,6.855,4.805,10.995,12.635,11.075c8.914,0.093,17.825,0.071,26.741,0.05
            l11.651-0.019l12.335,0.009c8.691,0.003,17.383,0.019,26.086-0.025c4.947-0.025,13.264-1.505,13.354-11.137
            c0.031-3.278-0.926-5.926-2.846-7.867c-2.253-2.282-5.768-3.492-10.158-3.505c-12.812-0.034-25.621-0.049-38.43-0.049
            c-12.812,0-25.621,0.016-38.433,0.049c-4.366,0.013-7.858,1.223-10.101,3.499C534.232,107.626,533.279,110.293,533.32,113.599z" />
                                        <path fill="#fff" d="M35.496,65.218c-0.016,3.307,0.944,5.96,2.852,7.879c2.242,2.258,5.759,3.458,10.171,3.471
            c7.028,0.024,14.038,0.015,21.057,0.013l9.366-0.059l9.545,0.021c7.214,0.021,14.421,0.046,21.627-0.053
            c3.889-0.053,7.084-1.254,9.239-3.47c1.938-1.997,2.938-4.762,2.886-8c-0.103-6.632-4.771-10.949-11.896-10.995
            c-10.332-0.065-20.67-0.096-31.005-0.096c-10.471,0-20.939,0.031-31.41,0.086C40.174,54.057,35.527,58.245,35.496,65.218z" />
                                        <path fill="#fff" d="M602.268,53.967l-9.536,0.019l-9.583-0.019c-3.186-0.012-6.372-0.021-9.558-0.021
            c-4.013,0-8.025,0.015-12.038,0.068c-7.599,0.099-12.264,4.474-12.175,11.422c0.087,6.654,4.735,10.991,11.84,11.047
            c20.808,0.155,41.62,0.177,62.423,0.078c7.65-0.037,12.459-4.347,12.546-11.249c0.043-3.319-1.022-6.109-3.077-8.072
            c-2.168-2.065-5.345-3.18-9.193-3.227C616.7,53.923,609.489,53.945,602.268,53.967z" />
                                        <path fill="#fff" d="M78.038,28.438c5.322,0.028,10.651,0.053,15.97-0.052c7.484-0.146,12.289-4.672,12.239-11.53
            C106.2,10.239,101.559,5.92,94.425,5.852c-15.796-0.155-31.658-0.155-47.151,0c-7.091,0.071-11.716,4.422-11.778,11.081
            c-0.065,6.901,4.626,11.267,12.243,11.391c5.326,0.083,10.654,0.065,15.979,0.043l11.92-0.019v0.083L78.038,28.438z" />
                                        <path fill="#fff" d="M565.439,17.233c0.077,6.846,4.79,10.995,12.604,11.097c5.13,0.068,10.271,0.046,15.416,0.034l6.929-0.015
            l7.202,0.015c5.31,0.013,10.633,0.034,15.945-0.034c7.812-0.099,12.515-4.258,12.586-11.122c0.034-3.226-0.976-5.979-2.914-7.95
            c-2.16-2.205-5.371-3.384-9.278-3.416h-0.003c-15.444-0.13-30.888-0.13-46.331,0c-3.898,0.034-7.098,1.211-9.249,3.406
            C566.408,11.224,565.402,13.986,565.439,17.233z" />
                                        <path fill="#232E62" d="M223.351,400.873c-6.133,0.09-11.025,4.932-11.143,11.029c-0.062,3.036,1.062,5.897,3.164,8.053
            c2.127,2.183,5.007,3.399,8.109,3.418c3.06-0.063,6.01-1.093,8.06-3.14c2.075-2.068,3.211-4.972,3.199-8.18
            c-0.013-3.177-1.165-6.05-3.245-8.091c-2.038-1.997-4.852-3.093-7.948-3.093C223.48,400.87,223.416,400.873,223.351,400.873z" />
                                        <path fill="#232E62" d="M321.277,531.318c0.213,6.329,4.867,10.893,11.078,10.893c0.037,0,0.071,0,0.108,0
            c3.232-0.028,6.168-1.217,8.261-3.351c2.046-2.087,3.158-4.95,3.124-8.068c-0.031-3.111-1.217-5.957-3.338-8.019
            c-2.093-2.035-4.932-3.146-8.01-3.146c-0.081,0-0.161,0-0.238,0.003h-0.003c-3.111,0.056-5.942,1.261-7.967,3.394
            C322.243,525.184,321.168,528.132,321.277,531.318z" />
                                        <path fill="#232E62" d="M261.954,322.232c-5.935,0.115-11.05,5.227-11.17,11.159c-0.062,3.009,1.037,5.83,3.096,7.942
            c2.13,2.185,5.06,3.399,8.249,3.417c0.031,0,0.059,0,0.087,0c3.207,0,5.972-1.071,8.003-3.099c2.038-2.034,3.149-4.938,3.13-8.18
            c-0.019-3.18-1.201-6.082-3.337-8.162c-2.044-1.991-4.809-3.081-7.81-3.081C262.122,322.229,262.037,322.229,261.954,322.232z" />
                                        <path fill="#232E62" d="M226.753,376.846c2.152,2.128,4.97,3.315,8.292,3.26c3.214-0.027,5.969-1.127,7.973-3.177
            c2.025-2.073,3.112-5.037,3.059-8.344c-0.099-6.214-4.843-10.955-11.035-11.025c-0.041,0-0.081,0-0.121,0
            c-2.938,0-5.864,1.232-8.043,3.396c-2.202,2.183-3.406,4.998-3.397,7.929C223.494,371.945,224.655,374.771,226.753,376.846z" />
                                        <path fill="#232E62" d="M229.949,444.86c-5.935,0-11.128,5.01-11.351,10.971c-0.235,6.3,4.793,11.829,10.982,12.074
            c2.755,0.121,5.61-1.024,7.824-3.121c2.337-2.207,3.707-5.201,3.752-8.214c0.053-3.168-1.121-6.14-3.297-8.362
            c-2.097-2.143-4.886-3.332-7.855-3.348C229.986,444.86,229.967,444.86,229.949,444.86z" />
                                        <path fill="#232E62" d="M334.191,306.269c-0.065,3.174,1.018,6.071,3.046,8.159c2.031,2.087,4.898,3.251,8.075,3.275
            c3.275,0.059,6.108-1.111,8.208-3.208c2.059-2.059,3.183-4.898,3.164-7.995c-0.022-3.111-1.276-6.037-3.536-8.239
            c-2.117-2.065-4.836-3.195-7.669-3.195c-0.071,0-0.14,0-0.207,0.002C339.282,295.188,334.309,300.209,334.191,306.269z" />
                                        <path fill="#232E62" d="M289.535,311.783c0.195,6.041,5.286,11.047,11.348,11.159c3.016,0.046,5.855-1.084,8.035-3.211
            c2.217-2.161,3.449-5.056,3.468-8.146c0.019-2.932-1.235-5.876-3.437-8.078c-2.201-2.198-5.241-3.709-8.056-3.427
            c-2.827,0.019-5.74,1.375-7.994,3.718C290.671,306.111,289.448,309.021,289.535,311.783z" />
                                        <path fill="#232E62" d="M422.495,358.923c2.883,0.124,5.672-0.96,7.854-3.025c2.264-2.143,3.586-5.143,3.629-8.233
            c0.05-3.263-1.05-6.208-3.093-8.292c-2.003-2.043-4.843-3.177-8.001-3.192c-0.019,0-0.037,0-0.059,0
            c-3.114,0-6.022,1.195-8.189,3.369c-2.117,2.124-3.27,4.935-3.245,7.922C411.434,353.232,416.729,358.69,422.495,358.923z" />
                                        <path fill="#232E62" d="M414.335,510.32c3.158-0.031,6.072-1.27,8.205-3.49c2.065-2.146,3.155-4.972,3.068-7.954
            c-0.17-5.86-5.458-11.077-11.313-11.164c-0.056,0-0.111-0.003-0.17-0.003c-2.901,0-5.645,1.13-7.731,3.192
            c-2.204,2.17-3.412,5.096-3.409,8.235c0.003,3.27,1.093,6.071,3.149,8.105C408.2,509.286,410.964,510.323,414.335,510.32z" />
                                        <path fill="#232E62" d="M428.972,465.531c0.18,5.932,5.391,10.97,11.382,10.998c0.019,0,0.037,0,0.053,0
            c3.037,0,6.056-1.289,8.298-3.543c2.167-2.174,3.344-4.926,3.315-7.749c-0.027-2.917-1.35-5.861-3.622-8.078
            c-2.248-2.195-5.06-3.415-8.054-3.384c-2.901,0.046-5.839,1.405-8.065,3.733C430.096,459.793,428.892,462.716,428.972,465.531z" />
                                        <path fill="#232E62" d="M431.968,385.242c0.181,5.932,5.391,10.97,11.382,10.997c0.019,0,0.037,0,0.053,0
            c3.037,0,6.056-1.288,8.298-3.542c2.167-2.174,3.344-4.927,3.315-7.75c-0.027-2.916-1.35-5.861-3.622-8.078
            c-2.248-2.195-5.059-3.415-8.053-3.384c-2.901,0.046-5.84,1.406-8.066,3.733C433.092,379.504,431.888,382.426,431.968,385.242z" />
                                        <path fill="#232E62" d="M438.482,423.751c0.18,5.933,5.391,10.97,11.382,10.998c0.019,0,0.037,0,0.053,0
            c3.036,0,6.056-1.288,8.297-3.542c2.168-2.173,3.345-4.926,3.316-7.75c-0.027-2.916-1.35-5.86-3.622-8.077
            c-2.248-2.195-5.06-3.416-8.054-3.385c-2.901,0.047-5.839,1.405-8.065,3.734C439.606,418.015,438.401,420.937,438.482,423.751z" />
                                        <path fill="#232E62" d="M241.473,494.316c-0.089,2.845,1.024,5.622,3.134,7.82c2.188,2.282,5.198,3.607,8.257,3.638h0.002
            c2.905-0.111,6.004-1.143,8.187-3.306c2.149-2.131,3.329-4.97,3.319-7.995c-0.009-2.941-1.313-5.897-3.576-8.111
            c-2.177-2.134-5.02-3.348-7.821-3.348c-0.056,0-0.114,0-0.174,0.003C247.055,483.123,241.655,488.506,241.473,494.316z" />
                                        <path fill="#232E62" d="M376.736,511.828c-0.04,0-0.084,0-0.124,0c-2.832,0-5.728,1.245-7.96,3.428
            c-2.272,2.22-3.589,5.151-3.616,8.043c-0.028,2.806,1.148,5.543,3.306,7.713c2.248,2.261,5.279,3.552,8.319,3.552
            c0.02,0,0.035,0,0.051,0h0.002c3.065-0.013,5.921-1.22,8.039-3.403c2.11-2.177,3.229-5.056,3.151-8.111
            C387.746,517.042,382.634,511.905,376.736,511.828z" />
                                        <path fill="#232E62" d="M376.934,318.75c-0.063,3.18,1.05,6.103,3.127,8.236c2.062,2.118,4.926,3.294,8.063,3.316
            c3.245-0.186,6.103-1.196,8.351-3.427c2.152-2.14,3.335-4.932,3.331-7.865c-0.006-2.879-1.266-5.768-3.461-7.926
            c-2.242-2.208-5.271-3.455-8.232-3.406C381.956,307.768,377.045,312.631,376.934,318.75z" />
                                        <path fill="#232E62" d="M289.107,532.414c2.85-0.021,5.605-1.173,7.794-3.334c2.3-2.273,3.613-5.301,3.607-8.311
            c-0.016-6.204-4.743-11.019-11.001-11.201c-3.188-0.094-6.167,0.997-8.316,3.077c-2.102,2.021-3.27,4.833-3.294,7.91
            C277.85,526.89,282.983,532.321,289.107,532.414z" />
                                    </g>
                                </svg>
                            </span>
                        </div>

                        <p>
                            ENGAGE IN
                COMPANY
                MANAGEMENT
                MEETINGS
                        </p>

                        <div class="curveArrow">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                width="401.023px" height="228.034px" viewBox="0 0 401.023 228.034" enable-background="new 0 0 401.023 228.034"
                                xml:space="preserve">
                                <defs>
                                    <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
                                        <stop offset="0%" style="stop-color: rgb(191,132,46); stop-opacity: 1" />
                                        <stop offset="100%" style="stop-color: rgb(233,177,97); stop-opacity: 1" />
                                    </linearGradient>
                                </defs>
                                <path fill-rule="evenodd" clip-rule="evenodd" fill="url(#grad1)" d="M0.539,20.521c0,0,58.861-49.482,128.52,5.228
        c21.508,13.667,105.184,130.676,107.621,132.768s67.219,101.407,146.281,44.953c2.438-1.951,2.09-4.182,2.09-4.182l-5.225-5.225
        h16.717c0,0,5.854-1.256,4.18,4.18c-0.418,2.51-3.135,15.682-3.135,15.682l-5.223-5.227l-3.135,1.045
        c0,0-43.117,32.896-92.994,10.453c-3.971-1.461-30.928-16.029-52.242-41.816c-2.719-2.508-48.064-61.68-48.064-61.68
        s-16.926-22.939-26.121-34.499c-11.016-13.845-24.034-29.271-24.034-29.271s-25.452-33.544-60.601-41.817
        C46.513,0.24,11.615,20.313,4.719,25.749C-2.212,27.612,0.539,20.521,0.539,20.521L0.539,20.521z" />
                            </svg>
                        </div>

                    </div>

                    <div class="col-sm-6 col-md-4 col-lg-2 position-relative">
                        <div class="rounded-circle">
                            <span>

                                <svg version="1.1" id="Layer_1" class="laptop" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    width="607.333px" height="400.415px" viewBox="0 0 607.333 400.415" enable-background="new 0 0 607.333 400.415"
                                    xml:space="preserve">
                                    <g>
                                        <defs>
                                            <rect id="SVGID_1_" x="-236.668" y="-338.667" width="1080" height="1080" />
                                        </defs>
                                        <clipPath id="SVGID_2_">
                                            <use xlink:href="#SVGID_1_" overflow="visible" />
                                        </clipPath>
                                        <path clip-path="url(#SVGID_2_)" fill="#232E62" d="M582.4,326.543c20.737-2.521,22.221,9.072,21.51,23.692
		c-1.498,30.788-19.525,49.971-50.062,50.016c-167.355,0.244-334.712,0.172-502.068,0.065c-41.09-0.026-52.56-35.108-48.339-64.546
		c0.679-4.729,10.708-8.116,19.662-14.405V4.341c7.592-0.726,13.498-1.779,19.404-1.782c174.01-0.088,348.02,0.052,522.027-0.308
		c13.612-0.028,18.342,3.246,18.231,17.932c-0.713,95.188-0.366,190.384-0.366,285.577V326.543z M303.631,15.083
		c-83.952,0-167.905,0.257-251.853-0.269c-13.002-0.081-16.465,4.171-16.407,16.694c0.431,93.163,0.488,186.332-0.053,279.494
		c-0.077,13.344,4.686,16.249,16.908,16.121c51.695-0.54,103.403,0.027,155.101-0.417c12.027-0.104,21.285,2.748,29.993,11.69
		c4.939,5.072,13.679,8.71,20.844,8.952c29.144,0.987,58.354-0.044,87.515,0.651c10.61,0.253,17.731-3.512,25.647-10.217
		c6.679-5.656,16.613-10.265,25.196-10.419c52.197-0.938,104.428-0.989,156.628-0.134c14.56,0.238,17.94-4.522,17.856-18.359
		c-0.562-91.627-0.573-183.26,0.002-274.886c0.091-14.337-3.291-19.39-18.597-19.253C469.49,15.471,386.56,15.082,303.631,15.083
		 M588.086,340.759c-62.258,0-123.61,0.231-184.958-0.207c-11.474-0.082-20.299,2.29-28.446,11.082
		c-4.14,4.466-12.206,7.255-18.563,7.38c-35.295,0.691-70.619,0.767-105.909-0.052c-6.729-0.156-13.343-5.265-20.012-8.086
		c-7.97-3.373-15.901-9.551-23.92-9.667c-57.811-0.839-115.64-0.466-173.463-0.419c-4.977,0.004-9.953,0.427-14.672,0.644
		c-5.027,30.926,7.626,46.269,37.079,46.271c165.283,0.013,330.566,0.013,495.85,0C580.525,387.702,593.18,372.379,588.086,340.759" />
                                        <path clip-path="url(#SVGID_2_)" fill="#232E62" d="M336.993,156.179c12.038-4.899,12.395,1.664,12.383,9.034
		c-0.058,35.84-0.021,71.678-0.016,107.518c0.005,25.476,0.007,25.476,27.151,22.261V178.066c13.856-2.667,14.175-2.669,14.25,9.187
		c0.19,30.207,0.068,60.415,0.071,90.622c0.001,5.482,0,10.966,0,17.431h27.137c0-11.388-0.002-22.889,0-34.39
		c0.003-18.432-0.479-36.887,0.39-55.277c0.195-4.128,5.127-8.033,7.878-12.042c1.733,0.845,3.467,1.69,5.2,2.536v97.174
		c25.451,7.191,28.292,5.174,28.309-18.627c0.011-16.385-0.405-32.787,0.337-49.138c0.171-3.779,4.293-7.38,6.597-11.063
		c1.72,0.736,3.439,1.474,5.159,2.211v78.703c6.498,0.362,11.48,0.445,16.419,0.955c9.547,0.985,12.25-3.419,12.13-12.562
		c-0.425-32.206-0.166-64.422-0.166-96.25c10.665-5.963,11.569-0.056,11.54,7.715c-0.099,27.134-0.033,54.271-0.031,81.406
		c0.001,5.564,0,11.13,0,18.371c5.751,0.477,10.632,0.732,15.473,1.313c9.96,1.195,13.289-2.854,13.174-13.006
		c-0.482-42.49-0.198-84.989-0.197-127.483c0.001-3.007,0-6.013,0-9.02c1.42-0.904,2.84-1.807,4.26-2.711
		c2.503,3.306,7.157,6.593,7.188,9.921c0.463,51.176,0.322,102.357,0.322,154.774H55.293v-21.37
		c11.992,3.756,23.854,7.472,38.687,12.118v-42.259c1.565-0.914,3.129-1.828,4.694-2.742c2.464,2.954,5.799,5.566,7.133,8.964
		c1.22,3.109,0.28,7.067,0.283,10.651c0.026,24.53,0.748,25.087,27.842,20.143v-66.693c1.37-1.071,2.738-2.142,4.107-3.213
		c2.835,3.013,7.89,5.869,8.116,9.067c0.938,13.246,0.421,26.596,0.429,39.907c0.015,25.247,0.376,25.547,27.53,21.085v-55.761
		c10.249-3.59,14.674-1.65,14.361,8.755c-0.459,15.304-0.118,30.632-0.118,47.39h25.073c1.835-12.711,3.625-25.788,5.966-38.765
		c0.092-0.509,5.464-0.065,9.435-0.065v37.016c24.465,8.08,28.105,5.614,28.128-17.979c0.01-9.728-0.415-19.486,0.285-29.165
		c0.221-3.053,3.452-5.889,5.305-8.823c2.132,3.119,5.978,6.17,6.109,9.371c0.637,15.745,0.289,31.53,0.289,48.494
		c5.634,0.535,10.044,0.859,14.429,1.392c10.269,1.244,14.583-2.377,14.258-13.528c-0.732-25.065-0.461-50.172,0.008-75.251
		c0.078-4.125,3.241-8.191,4.978-12.284c2.1,4.244,5.894,8.441,6.014,12.741c0.617,21.999,0.284,44.023,0.287,66.04
		c0.004,24.531,0.949,25.264,28.072,20.148V156.179z" />
                                        <path clip-path="url(#SVGID_2_)" fill="#232E62" d="M551.945,33.622c0,21.73,0.371,42.173-0.294,62.583
		c-0.143,4.39-3.114,9.55-6.331,12.843c-23.598,24.151-47.854,47.664-71.284,71.974c-6.333,6.571-11.02,6.503-18.309,2.127
		c-31.034-18.628-63.019-35.756-93.311-55.497c-14.347-9.35-22.499-6.505-33.686,4.287c-31.647,30.528-64.407,59.905-96.761,89.699
		c-2.982,2.747-6.2,5.234-9.634,8.116c-20.113-14.513-40.033-28.637-59.651-43.167c-7.469-5.531-12.952-6.684-21.283,0.127
		c-27.576,22.545-56.124,43.903-85.941,66.987V33.622H551.945z M463.882,173.157c17.747-17.866,33.739-36.86,52.671-52.234
		c20.091-16.316,26.032-35.396,24.155-60.106c-0.952-12.531-4.753-15.241-16.349-15.216c-147.48,0.321-294.962,0.324-442.443-0.004
		C70.248,45.571,65.681,48.19,65.86,60.911c0.698,49.663,0.228,99.342,0.377,149.014c0.015,4.698,1.332,9.392,2.527,17.278
		c29.094-22.507,55.606-43.018,82.464-63.795c23.421,16.671,46.605,33.174,70.438,50.14
		c40.735-37.414,80.585-74.015,120.967-111.104C383.133,126.064,423.119,149.383,463.882,173.157" />
                                    </g>
                                </svg>
                            </span>
                        </div>

                        <p>
                            CONDUCT
    IN-DEPTH
    ANALYSIS AND
    EVALUATION
                        </p>

                        <div class="curveArrow">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                width="401.023px" height="228.034px" viewBox="0 0 401.023 228.034" enable-background="new 0 0 401.023 228.034"
                                xml:space="preserve">
                                <defs>
                                    <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
                                        <stop offset="0%" style="stop-color: rgb(191,132,46); stop-opacity: 1" />
                                        <stop offset="100%" style="stop-color: rgb(233,177,97); stop-opacity: 1" />
                                    </linearGradient>
                                </defs>
                                <path fill-rule="evenodd" clip-rule="evenodd" fill="url(#grad1)" d="M0.539,20.521c0,0,58.861-49.482,128.52,5.228
        c21.508,13.667,105.184,130.676,107.621,132.768s67.219,101.407,146.281,44.953c2.438-1.951,2.09-4.182,2.09-4.182l-5.225-5.225
        h16.717c0,0,5.854-1.256,4.18,4.18c-0.418,2.51-3.135,15.682-3.135,15.682l-5.223-5.227l-3.135,1.045
        c0,0-43.117,32.896-92.994,10.453c-3.971-1.461-30.928-16.029-52.242-41.816c-2.719-2.508-48.064-61.68-48.064-61.68
        s-16.926-22.939-26.121-34.499c-11.016-13.845-24.034-29.271-24.034-29.271s-25.452-33.544-60.601-41.817
        C46.513,0.24,11.615,20.313,4.719,25.749C-2.212,27.612,0.539,20.521,0.539,20.521L0.539,20.521z" />
                            </svg>
                        </div>

                    </div>

                    <div class="col-sm-6 col-md-4 col-lg-2 position-relative">
                        <div class="rounded-circle ">
                            <span>
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    width="739.427px" height="725.281px" viewBox="0 0 739.427 725.281" enable-background="new 0 0 739.427 725.281"
                                    xml:space="preserve">
                                    <polygon fill="#232E62" points="512.488,540.032 501.403,551.114 501.403,646.114 528.313,646.114 528.313,540.032 " />
                                    <polygon fill="#232E62" points="248.589,592.811 237.511,603.893 237.511,646.114 264.413,646.114 264.413,592.811 " />
                                    <polygon fill="#232E62" points="380.534,566.422 369.456,577.503 369.456,646.114 396.359,646.114 396.359,566.422 " />
                                    <polygon fill="#232E62" points="437.286,386.184 454.297,403.198 459.61,344.834 401.251,350.147 418.27,367.162 350.937,434.472
        192.602,434.472 79.167,547.922 79.167,585.967 203.756,461.386 362.099,461.386 " />
                                    <polygon fill="#232E62" points="0,284.057 0,725.281 739.427,725.281 739.427,698.371 26.902,698.371 26.902,310.967 " />
                                    <g>
                                        <defs>
                                            <rect id="SVGID_1_" x="-176.756" y="-176" width="1080" height="1080" />
                                        </defs>
                                        <clipPath id="SVGID_2_">
                                            <use xlink:href="#SVGID_1_" overflow="visible" />
                                        </clipPath>
                                        <polygon clip-path="url(#SVGID_2_)" fill="#232E62" points="644.435,487.254 633.356,498.336 633.356,646.114 660.259,646.114
            660.259,487.254 	" />
                                        <path clip-path="url(#SVGID_2_)" fill="#232E62" d="M148.941,245.671c48.787,0,88.488-39.692,88.488-88.488
            c0-48.796-39.701-88.488-88.488-88.488s-88.488,39.692-88.488,88.488C60.453,205.979,100.154,245.671,148.941,245.671
             M148.941,88.359c37.951,0,68.824,30.882,68.824,68.824c0,37.942-30.873,68.823-68.824,68.823
            c-37.952,0-68.824-30.881-68.824-68.823C80.117,119.241,110.989,88.359,148.941,88.359" />
                                        <path clip-path="url(#SVGID_2_)" fill="#232E62" d="M148.941,206.343c27.106,0,49.159-22.053,49.159-49.16
            c0-27.106-22.053-49.16-49.159-49.16c-27.107,0-49.16,22.054-49.16,49.16C99.781,184.29,121.834,206.343,148.941,206.343
             M148.941,127.687c16.262,0,29.496,13.234,29.496,29.496s-13.234,29.496-29.496,29.496s-29.496-13.234-29.496-29.496
            S132.679,127.687,148.941,127.687" />
                                        <path clip-path="url(#SVGID_2_)" fill="#232E62" d="M296.42,194.388v-74.409l-27.471-6.754c-1.208-3.274-2.575-6.538-4.099-9.783
            l14.679-24.236l-52.621-52.621l-24.236,14.68c-3.254-1.515-6.518-2.882-9.782-4.09l-6.746-27.471h-74.408l-6.754,27.471
            c-3.264,1.208-6.529,2.575-9.783,4.09l-24.236-14.67l-52.62,52.611l14.679,24.246c-1.515,3.244-2.881,6.508-4.101,9.773
            l-27.46,6.754v71.219c2.248,0.919,4.359,2.194,6.25,3.753c5.208,0.86,9.952,3.424,13.414,7.477v-3.204l7.806,1.919
            c1.21,3.264,2.576,6.528,4.091,9.772l-6.816,11.266c-0.123,0.491-0.448,0.898-0.607,1.377c-0.215,0.624-0.528,1.207-0.797,1.811
            c-0.775,1.738-1.706,3.363-2.912,4.873c-0.585,0.755-1.208,1.462-1.878,2.144c-0.443,0.443-0.749,0.983-1.224,1.398
            c1.725,2.042,3.317,4.21,4.701,6.524l36.964,36.964v1.562c2.373,0.772,4.638,1.898,6.899,3.024
            c4.407,0.358,8.711,1.279,12.765,2.997v-2.618l15.091-9.134c3.245,1.514,6.51,2.88,9.783,4.09l6.745,27.47h74.408l6.755-27.47
            c3.265-1.21,6.529-2.576,9.783-4.09l24.246,14.669l52.62-52.621l-14.679-24.236c1.514-3.244,2.881-6.508,4.101-9.782
            L296.42,194.388z M254.723,232.162l-30.814,30.804l-20.382-12.34l-4.906,2.547c-5.643,2.93-11.346,5.32-16.96,7.099l-5.269,1.661
            l-5.664,23.066h-43.575l-5.673-23.056l-5.25-1.671c-5.634-1.79-11.346-4.178-16.98-7.109l-4.906-2.546l-20.382,12.338
            l-30.813-30.803l12.338-20.372l-2.535-4.896c-2.921-5.623-5.311-11.336-7.099-16.989l-1.672-5.251l-23.056-5.673v-43.575
            l23.056-5.673l1.672-5.25c1.788-5.624,4.178-11.336,7.099-16.99l2.535-4.897L43.158,82.204L73.971,51.4L94.354,63.74l4.905-2.546
            c5.644-2.93,11.347-5.319,16.961-7.099l5.27-1.662l5.663-23.065h43.575l5.673,23.076l5.27,1.661
            c5.614,1.779,11.317,4.169,16.961,7.099l4.905,2.546l20.382-12.339l30.814,30.804l-12.339,20.392l2.546,4.895
            c2.91,5.614,5.3,11.327,7.099,16.98l1.671,5.251l23.047,5.663v43.575l-23.056,5.673l-1.672,5.251
            c-1.8,5.643-4.179,11.355-7.099,16.979l-2.546,4.896L254.723,232.162z" />
                                        <path clip-path="url(#SVGID_2_)" fill="#232E62" d="M712.248,137.266l-35.153-46.88l54.788-87.668h-37.27
            c-13.021,0-19.948,7.907-24.533,13.139c-4.272,4.879-5.967,6.457-9.788,6.457s-5.516-1.578-9.787-6.457
            c-4.586-5.232-11.513-13.139-24.533-13.139s-19.938,7.916-24.513,13.148c-4.253,4.87-5.958,6.448-9.749,6.448
            c-3.792,0-5.496-1.578-9.758-6.448c-4.575-5.232-11.493-13.148-24.503-13.148h-37.271l54.798,87.678l-35.163,46.87
            c-13.894,18.537-21.555,41.493-21.555,64.664v6.535c0,59.421,48.351,107.772,107.771,107.772
            c59.422,0,107.772-48.351,107.772-107.772v-6.535C733.803,178.769,726.15,155.803,712.248,137.266 M557.449,22.314
            c3.791,0,5.496,1.577,9.748,6.446c4.575,5.232,11.492,13.148,24.513,13.148c13.012,0,19.928-7.916,24.503-13.148
            c4.263-4.869,5.958-6.446,9.759-6.446c3.821,0,5.516,1.577,9.788,6.456c4.585,5.231,11.512,13.138,24.532,13.138
            c13.021,0,19.948-7.907,24.533-13.138c4.271-4.879,5.967-6.456,9.788-6.456h1.92l-36.74,58.784h-67.515l-36.74-58.784H557.449z
             M714.208,208.465c0,48.625-39.563,88.177-88.178,88.177s-88.176-39.552-88.176-88.177v-6.535c0-18.958,6.26-37.75,17.635-52.906
            l36.25-48.331h68.583l36.25,48.331c11.375,15.166,17.636,33.957,17.636,52.906V208.465z" />
                                        <path clip-path="url(#SVGID_2_)" fill="#232E62" d="M635.828,159.478c5.398,0,9.797,4.39,9.797,9.797h19.596
            c0-16.205-13.188-29.392-29.393-29.392v-19.595h-19.596v19.595h-29.392v29.392c0,16.205,13.188,29.392,29.392,29.392h19.596
            c5.398,0,9.797,4.39,9.797,9.798v9.797h-29.393c-5.397,0-9.797-4.388-9.797-9.797h-19.595c0,16.205,13.188,29.393,29.392,29.393
            v19.595h19.596v-19.595h29.393v-29.393c0-16.205-13.188-29.392-29.393-29.392h-19.596c-5.397,0-9.797-4.389-9.797-9.798v-9.797
            H635.828z" />
                                    </g>
                                </svg>
                            </span>
                        </div>
                        <p>
                            PORTFOLIO
    CONSTRUCTION
    AND ONGOING
    REVIEW
                        </p>

                        <div class="curveArrow">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                width="401.023px" height="228.034px" viewBox="0 0 401.023 228.034" enable-background="new 0 0 401.023 228.034"
                                xml:space="preserve">
                                <defs>
                                    <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
                                        <stop offset="0%" style="stop-color: rgb(191,132,46); stop-opacity: 1" />
                                        <stop offset="100%" style="stop-color: rgb(233,177,97); stop-opacity: 1" />
                                    </linearGradient>
                                </defs>
                                <path fill-rule="evenodd" clip-rule="evenodd" fill="url(#grad1)" d="M0.539,20.521c0,0,58.861-49.482,128.52,5.228
        c21.508,13.667,105.184,130.676,107.621,132.768s67.219,101.407,146.281,44.953c2.438-1.951,2.09-4.182,2.09-4.182l-5.225-5.225
        h16.717c0,0,5.854-1.256,4.18,4.18c-0.418,2.51-3.135,15.682-3.135,15.682l-5.223-5.227l-3.135,1.045
        c0,0-43.117,32.896-92.994,10.453c-3.971-1.461-30.928-16.029-52.242-41.816c-2.719-2.508-48.064-61.68-48.064-61.68
        s-16.926-22.939-26.121-34.499c-11.016-13.845-24.034-29.271-24.034-29.271s-25.452-33.544-60.601-41.817
        C46.513,0.24,11.615,20.313,4.719,25.749C-2.212,27.612,0.539,20.521,0.539,20.521L0.539,20.521z" />
                            </svg>
                        </div>

                    </div>

                    <div class="col-md-4 col-lg-2 position-relative">
                        <div class="rounded-circle ">
                            <span>
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    width="866.957px" height="629.967px" viewBox="0 0 866.957 629.967" enable-background="new 0 0 866.957 629.967"
                                    xml:space="preserve">
                                    <g>
                                        <defs>
                                            <rect id="SVGID_1_" x="-134.112" y="-222.608" width="1080" height="1080" />
                                        </defs>
                                        <clipPath id="SVGID_2_">
                                            <use xlink:href="#SVGID_1_" overflow="visible" />
                                        </clipPath>
                                        <path clip-path="url(#SVGID_2_)" fill="#232E62" d="M866.838,316.285c0.028-0.343,0.05-0.736,0.05-1.133
            c0-0.394-0.018-0.784-0.061-1.174l-0.29-1.738c-0.057-0.215-0.114-0.401-0.168-0.587l-0.16-0.565
            c-0.083-0.23-0.177-0.454-0.267-0.68l-0.128-0.319c-0.15-0.329-0.254-0.525-0.357-0.722l-1.778-2.626
            c-0.157-0.175-0.325-0.354-0.5-0.526L737.374,180.413c-3.217-3.219-6.977-3.702-8.931-3.702c-1.952,0-5.712,0.483-8.928,3.698
            c-3.22,3.22-3.699,6.98-3.699,8.933c0,1.954,0.479,5.714,3.695,8.929l104.256,104.248H267.146c-6.961,0-12.628,5.667-12.628,12.633
            c0,6.965,5.667,12.632,12.628,12.632h556.618L719.519,432.033c-3.221,3.215-3.703,6.975-3.703,8.928s0.482,5.713,3.703,8.93
            c3.215,3.223,6.976,3.702,8.929,3.702c1.954,0,5.709-0.479,8.926-3.695l125.806-125.81c0.199-0.196,0.389-0.404,0.575-0.615
            l1.635-2.396c0.157-0.309,0.286-0.552,0.397-0.784l0.243-0.602c0.035-0.093,0.089-0.229,0.15-0.39l0.357-1.173
            c-0.021,0.067-0.032,0.1-0.032,0.1c-0.003,0,0.021-0.097,0.054-0.232L866.838,316.285z" />
                                        <path clip-path="url(#SVGID_2_)" fill="#232E62" d="M316.631,1.412c-172.998,0-313.743,140.745-313.743,313.74
            c0,172.994,140.745,313.738,313.743,313.738c104.388,0,201.639-51.707,260.15-138.315c2.547-3.77,2.307-7.552,1.935-9.469
            c-0.372-1.918-1.56-5.521-5.333-8.07c-5.76-3.882-13.641-2.369-17.54,3.401c-53.801,79.644-143.228,127.194-239.212,127.194
            c-159.068,0-288.479-129.412-288.479-288.479c0-159.064,129.411-288.479,288.479-288.483c96.267,0,185.841,47.764,239.604,127.776
            c3.882,5.778,11.75,7.323,17.533,3.434c5.777-3.881,7.319-11.744,3.435-17.525C518.731,53.355,421.32,1.412,316.631,1.412" />
                                    </g>
                                </svg>
                            </span>
                        </div>
                        <p>
                            EXITING
    POSITIONS
                        </p>

                    </div>
                </div>

                <div class="row justify-content-between">
                    <div class="col-12 text-center">
                        <a href="our_process.aspx" class="readMore brownBg"><span>Read More</span></a>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <script>

        //personData


        /*******************************************/

        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            //closed: 'accordion', // Start closed if in accordion view
            activate: function (event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });

        /*$('.heading').click(function () {
            if ($(this).children('.fa').hasClass('fa-plus')) {
                $(this).children('.fa').removeClass('fa-plus');
                $(this).siblings('.heading').children('.fa').addClass('fa-plus');
                $(this).children('.fa').addClass('fa-minus');
                $(this).siblings('.heading').children('.fa').removeClass('fa-minus');
            } else {
                $(this).children('.fa').addClass('fa-plus');
                //$(this).siblings('.heading').children('.fa').removeClass('fa-plus');
                $(this).children('.fa').removeClass('fa-minus');
                //$(this).siblings('.heading').children('.fa').addClass('fa-minus');
            }
            $(this).next().siblings('.content').slideUp();
            $(this).next().slideToggle();

        })*/

        /*$('.connectBtn').click(function () {
            var footer = $(this).attr('href');
            console.log(footer);
            body.animate({ scrollTop: $(footer).offset().top - 200 }, 500, 'swing');
        })*/

        $('.offering').click(function () {
            var body = $('body, html');
            var scrollTopVert = $('#alchemy_offering').offset().top - 250;
            body.animate({ scrollTop: scrollTopVert }, 500, 'swing');

            /*if ($(window).width() < 768) {
                $('body').removeClass('pushy-open-right');
                $('.menu-btn').removeClass('open');
            }*/

        })

        

        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

        /**********************************************/
    </script>
</asp:Content>

