﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="equity_market_outlook_nov_2019.aspx.cs" Inherits="equity_market_outlook_nov_2019" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="banner" class="position-relative mb-5">
        <h1>Thought Leadership</h1>
        <img src="images/banner/thought_leadership.jpg" class="img-fluid" alt="">
    </div>

    <div class="container mb-4">
        <div class="row">
            <div class="col-12">

                <ul class="breadcrumb">
                <li><a href="index.aspx">Home</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li><a href="thought_leadership.aspx">Thought Leadership</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li>Equity Market Outlook - November 2019</li>
                </ul>

            </div>
        </div>
        <div class="row mb-5 innerPageData" id="blog">
            <div class="col-12 mb-4">

                <h4 class="brownColor">EQUITY MARKET OUTLOOK - NOVEMBER 2019 </h4>

                <p><strong>The Market continues to be strong...  </strong></p>



                <p>October 2019 was another strong month for equities, with the Nifty rising 3.5% on the back of a 4% rally in September. A notable feature was that the Nifty midcap outperformed the Nifty, rising by 4.9%. The sectoral performance remained tilted towards Autos and FMCG, with banks slightly underperforming. </p>

                <p>The market has responded to a series of pro-cyclical regulatory and policy measures over the last 2-3 months — deep interest rate cuts, the corporate tax rate cut and decisive moves on privatization. On Nov 6, 2019, the government announced the contours of a real estate package announced earlier. We feel that this is a well-timed intervention as the sector is in a negative spiral with the liquidity and solvency crisis feeding into each other. This intervention could break that spiral, execution risks notwithstanding. </p>

                <p>The government measures will take time to impact aggregate demand and growth. The concurrent indicators for growth remain weak — IIP, core sector or even the high-frequency indicators like auto sales. Some of these data points, though, are incrementally improving — one example is car sales. We do expect that the consumption recovery should begin in early CY2020. The investment cycle could take longer to revive. </p>

                <p>This weakness is reflected in the earnings data for 2QFY19. Sales growth for Nifty companies (those which have published results as on 31 Oct 2019) has been sluggish though PAT growth has accelerated because of the tax rate cuts. This divergence will widen further in 3QFY20 because DTA adjustments were a drag on the 2Q profits. From CY20, multiple factors should play out to help revive the flagging toplines - lower rates, monsoons, some reinvestment of the tax bonanza by the corporates. </p>

                <p>We remain positive on the markets and continue our usual pace of deployment of fresh money. We remained focused on our stock-picking philosophy: high-growth companies with strong balance sheets, healthy cash-flow conversion, elevated return ratios and clean corporate governance records. We believe that this is a good time to invest for a 2-3 year horizon from a structural perspective. </p>

                <p><strong>Alchemy Group Research</strong></p>

            </div>




        </div>

        <div class="brown-dashed-border mb-5"></div>


    </div>
    <script>
        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

    </script>
</asp:Content>

