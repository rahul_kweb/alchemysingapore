﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="equity_market_outlook_august_2019.aspx.cs" Inherits="equity_market_outlook_august_2019" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="banner" class="position-relative mb-5">
        <h1>Thought Leadership</h1>
        <img src="images/banner/thought_leadership.jpg" class="img-fluid" alt="">
    </div>

    <div class="container mb-4">

        <div class="row">
            <div class="col-12">

                <ul class="breadcrumb">
                <li><a href="index.aspx">Home</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li><a href="thought_leadership.aspx">Thought Leadership</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li><a href="thought_leadership.aspx">Equity Market Outlook - August 2019</a></li>
                </ul>

            </div>
        </div>

        <div class="row mb-5 innerPageData" id="blog">
            <div class="col-12 mb-4">

                <h4 class="brownColor">EQUITY MARKET OUTLOOK - AUGUST 2019  </h4>
                <p><strong>Market in Consolidation Mode </strong></p>

                <p>The rate cut has come and gone, but the post-budget sell-off in equities seems to be unabated and broad-based. The FPI tax was a small catalyst - the markets have belatedly started tracking the broader economy. The silver lining is that some enablers for a macro recovery are in place, but we see enough risks to remain cautious on the broader market. </p>

                <p><strong>The silver linings </strong></p>

                <p>The RBI delivered another 35 bps rate cut to take the repo rate to 5.4%. It has also promised to keep liquidity in surplus and have taken measures to encourage credit flow to NBFCs and SMEs. Oil prices remain low and stable, and inflation remains benign. Government bond yields have also fallen significantly since the budget. The positives, however, circumscribe a dark cloud of uncertainty. Until the risks are addressed, the cyclical slowdown could continue or even intensify. </p>
                <p><strong>The Key challenges</strong></p>

                <p>
                    <strong>Muted transmission :</strong> Transmission of RBI's rate cuts have been muted - bank MCLRs have fallen by < 25bps vs. -100bps repo rate cut since early 2019. The core problem is that deposit rates have remained rigid as bank balance sheets are precariously poised. The signaling effect of sticky small savings rates does not help. We may see better transmission in the coming quarters as banks get more confident on the durability of liquidity. Lower lending rates are necessary for the revival: a 75-100bps cut in mortgage and car loan rates could make a big difference.
                </p>
                <p><strong>Credit crunch :</strong> Credit growth is seemingly healthy at 12%, but all is not well. Blue-chip companies are raising money with ease while smaller borrowers, especially SMEs are gasping for credit. Easy liquidity will help, but is not the complete solution. Banks need confidence to take more risk, and that can come from speedy resolution of existing stressed cases. Real estate asset quality, in particular, needs to be addressed quickly - much of the NBFC credit crunch emanates from the stress in developer balance sheets.</p>

                <p><strong>Auto slowdown :</strong> The other pain point is the auto sector. It has taken hits from all directions - low consumer confidence, the credit squeeze and policy uncertainty. Some sort of government intervention would help -clarity around the EV policy and a possible GST rate cut, with demand elasticity covering risks to revenue. Lower rates and easier liquidity to dealers may help, but underlying demand has to recover. There is some hope around the festive season sparking a second-half revival, but that is unlikely without some intervention. </p>

                <p><strong>Fiscal risk :</strong> The headline fiscal deficit has been contained, but the consolidated deficit including PSU and state borrowings remains a worry. Tax revenues are tracking well below budget estimates. This raises the risk a spending cutback in 4QFY20 - that would stop any post-Diwali recovery in its tracks. This government is caught in a vicious cycle: it raises tax rates to combat sluggish revenues but higher rates push growth down further, which transmits to a further deceleration in tax collections. A leap-of-faith cut in tax rates, especially in slow sectors like autos, may help break the cycle.</p>

                <p><strong>No quick-fix solutions</strong></p>

                <p>There are multiple pain-points for the market and the solutions are multi-factor and complex. For sure, some government intervention would help - a cut in auto GST rates the most obvious one. Resolution of some of the stressed loans would also help sentiment, but that will take its own time and solutions are not easy. The recovery in the monsoon could also help, as much of the consumption pain point emanates from the rural segments. There is no single event that would turn the markets - it has to be series of coordinated measures to help the cyclical recovery. </p>

                <p><strong>Our positioning  </strong></p>

                <p>The correction in prices has brought valuations into a more reasonable zone, but the macro risks persist. We continue to look for bottom-up opportunities but have to remain mindful of earnings weakness emanating from the macro stress. The broader markets are likely to remain in consolidation in the near term, and would recover only when there is visibility on the economic cycle turning.</p>

                <p><strong>Source-Alchemy Group Research </strong></p>



            </div>




        </div>

        <div class="brown-dashed-border mb-5"></div>


    </div>

    <script>
        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

    </script>
</asp:Content>

