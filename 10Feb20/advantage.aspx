﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="advantage.aspx.cs" Inherits="ourAdvantage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="banner" class="position-relative mb-5">
        <h1>Our Advantages</h1>
        <img src="images/banner/advantage_banner.jpg" class="img-fluid" alt="">
    </div>

    <div class="container mb-4">
        <div class="row">
            <div class="col-12">
                <ul class="breadcrumb">
                <li><a href="index.aspx">Home</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li><a href="about.aspx">About</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li>Our Advantages</li>
                </ul>

            </div>
        </div>
        <div class="row mb-4">
            <h1 class="col-12 text-uppercase brownColor">Why Alchemy?</h1>
            <div class="col-12">
                <p>Alchemy has been one of India’s successful portfolio management firms having a track record since 2002. The firm was founded by Lashit Sanghvi, Ashwin Kedia, Rakesh Jhunjhunwala and Hiren Ved, with each of the firm’s co-founders having over 25 years of experience each in investing in India’s equity market. The team has worked together for more than a decade and has instilled a culture of excellence and cultivated an institutional pedigree that has consistently resulted in superior performance over the last 17 years.</p>

                <p>A dedicated team of professionals work round the clock to make sure that you benefit from the association. The exclusive Bottom-Up strategy and periodic monitoring make Alchemy what it is today; an asset management organization of choice.</p>
            </div>
        </div>

        <div class="brown-dashed-border mb-5"></div>

        <div class="row justify-content-center">
            <div class="col-xl-12">
                <div id="verticalTab">
                    <ul class="resp-tabs-list mb-4">
                        <li>ALCHEMY EXPERTISE</li>
                        <li>THE ALCHEMY EDGE</li>
                        <li>CULTIVATING AN ECOSYSTEM OF EXCELLENCE</li>
                    </ul>

                    <div class="resp-tabs-container innerPageData">

                        <div>

                            <div class="whiteBox p-4 mb-4">

                                <div class="heading">
                                    <strong>FOCUSED, BOTTOM-UP STRATEGY</strong>
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </div>

                                <div class="content" style="display: block;">

                                    <p>
                                        Alchemy focuses on bottom-up stock picking based on  a combination of primary and secondary research using a 
disciplined process. The team takes long-term concentrated positions with the objective of <strong>generating long-term superior risk-adjusted returns on investors’ capital.***<br>
    ***</strong> The objective stated here is merely a target and there are no assurances that  they will be achieved.
                                    </p>

                                </div>

                                <div class="heading">
                                    <strong>EXPERIENCED MANAGEMENT TEAM</strong>
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </div>

                                <div class="content">
                                    <p>Alchemy is led by the four founders that comprise its Founders’ Group, a team with <strong>experience that spans several market cycles</strong> of investing in the Indian equity markets. The long-tenured team possesses extensive networks of intelligence and relationships that are critical when evaluating companies and identifying trends at the ground level.</p>
                                </div>

                                <div class="heading">

                                    <strong>OUTSTANDING TRACK RECORD#</strong>
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </div>

                                <div class="content">
                                    <p>Alchemy has achieved an exceptional track record over its 17 years of managing and advising clients’ portfolios. Currently, the group has over USD 1 billion   of assets under management.</p>

                                    <p>
                                        Alchemy has achieved benchmark-beating performance year-after-year in its three strategies. Alchemy High Growth, Alchemy High Growth Select Stock and Alchemy India Long Term Fund have achieved annualized alpha of 7.3%*, 7.8%* and 6.6%**, respectively, since their inceptions
                                    </p>

                                    <p><strong>Inception Dates:</strong></p>

                                    <ul>
                                        <li>Alchemy Hight Growth – May 2002.</li>
                                        <li>Alchemy High Growth – Select Stock – December 2008.</li>
                                        <li>Alchemy India Long Term Fund – June 2008.</li>
                                    </ul>

									<p># Past performance is not the assurance of future performance.</br>
									* Indian rupee terms, net of fees and expenses, ending Aug 31, 2019.</br>
									** USD terms, net of fees and expenses, ending Aug 31, 2019.</br>
									***Alchemy High Growth and Alchemy High Growth Select Stock are strategies managed by Alchemy India.</p>

                                </div>

                                <div class="heading">

                                    <strong>RIGOROUS RISK MANAGEMENT</strong>
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </div>

                                <div class="content">
                                    <p>Alchemy has a <strong>robust infrastructure</strong> that enables rigorous risk management based on continuous review of contributors to – and detractors from – performance <strong>to ensure soundness and integrity of the firm’s investment thesis.</strong> An independent member of the board of Alchemy India presides over Alchemy India’s Investment Committee meetings to bring independence to the process.</p>

                                </div>

                                <div class="heading">
                                    <strong>REGULATED ENVIRONMENT</strong>
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </div>

                                <div class="content">
                                    <ul>
                                        <li>Alchemy holds fund management license with MAS in Singapore and is a registered investment adviser with SEC in USA.</li>
                                        <li>Alchemy operates in a highly regulated environment with periodic fillings / reporting’s to regulators in India, Singapore 
    and / or USA.</li>
                                    </ul>
                                </div>




                            </div>

                        </div>

                        <div>
                            <div class="whiteBox p-4 mb-4">

                                <div class="heading">
                                    <strong>ORGANIZATION OF CHOICE AMONG TIER-ONE INVESTORS </strong>
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </div>

                                <div class="content" style="display: block;">
                                    <p>Alchemy has a top-tier standing among leading global institutional investors and some of the top Indian families, which is built on the firm’s long-term integrity.</p>
                                </div>

                                <div class="heading">
                                    <strong>OUTSTANDING CULTURE </strong>
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </div>

                                <div class="content">
                                    <p>Alchemy is renowned for its collaborative and innovative culture — one that prioritizes quality of ideas and is focused on generating consistent alpha combined with a superior client experience.</p>
                                </div>

                                <div class="heading">
                                    <strong>STRONG INVESTMENT LEADERSHIP  </strong>
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </div>

                                <div class="content">
                                    <p>Continuity in philosophy and strategy, with founder Hiren Ved as Alchemy India’s Chief Investment Officer. Hiren and the Alchemy’s investment management team have built a culture of performance excellence that has been instrumental in delivering Alchemy’s  superior and consistent track record.</p>
                                </div>

                                <div class="heading">
                                    <strong>TREMENDOUS ON-THE-GROUND INSIGHT </strong>
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </div>

                                <div class="content">
                                    <p>Comprehensive networks of relationships built over decades provide team with a regular  flow of intelligence and insight on stocks/sectors/businesses and potential opportunities in the Indian business ecosystem.</p>
                                </div>

                                <div class="heading">
                                    <strong>OUTSTANDING TRACK RECORD  </strong>
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </div>

                                <div class="content">
                                    <p>
                                        A focus on superior performance and sustainable long-term wealth creation.<br>
                                        —    Alchemy India Long Term Fund Limited has outperformed its benchmark 9 of the last 11 calendar years.
                                    </p>
                                </div>

                            </div>

                        </div>

                        <div>
                            <div class="whiteBox p-4 mb-4">

                                <div class="heading">
                                    <strong>ALCHEMY’S CULTURE OF EXCELLENCE</strong>
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </div>

                                <div class="content" style="display: block;">
                                    <p>Alchemy strongly believes knowledge is power. The firm is built on a foundation of intelligence and has nurtured a culture of learning, innovation and performance that permeates the entire organization and produces tangible results for the company’s clients.</p>
                                </div>

                                <div class="heading">
                                    <strong>HIGHLY COLLABORATIVE TEAM</strong>
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </div>

                                <div class="content">
                                    <ul>
                                        <li>Alchemy India’s Advisory Team has worked together for more than 10 years, ensuring continuity of track record.</li>
                                        <li>Anyone can bring ideas to the table.</li>
                                        <li>All ideas are discussed collaboratively and openly amongst the team.</li>
                                    </ul>
                                </div>

                                <div class="heading">
                                    <strong>PERFORMANCE - FOCUSED CULTURE AND ALIGNMENT OF INTERESTS</strong>
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </div>

                                <div class="content">
                                    <ul>
                                        <li>Alchemy has set aside 15% of the firm’s equity for senior team members.</li>
                                        <li>Senior Management team —is part of the company’s ESOP plan.</li>
                                        <li>Alchemy holds annual off-site events, at which employees and departments are recognized for their outstanding work.</li>
                                    </ul>
                                </div>

                                <div class="heading">
                                    <strong>HIGH STANDARDS OF SOCIAL RESPONSIBILITY</strong>
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </div>

                                <div class="content">
                                    <ul>
                                        <li>Alchemy takes its role in promoting corporate social responsibility very seriously.</li>
                                        <li>About 2% of annual EBIT is set aside for various non-profit organizations with an educational focus.</li>
                                        <li>A dedicated committee has the responsibility of allocating that 2% accordingly.</li>
                                    </ul>
                                </div>

                                <div class="heading">
                                    <strong>SECURE, WORLD-CLASS INFRASTRUCTURE</strong>
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </div>

                                <div class="content">
                                    <ul>
                                        <li>Alchemy Singapore, the investment manager for our offshore mandates, is registered with the U. S. Securities and Exchange Comission.</li>
                                        <li>Digital assets are protected by a business continuity plan that backs up and replicates to another location.</li>
                                        <li>Alchemy employs reputable administrators and auditors across all geographies in which the firm operates.</li>
                                        <li>Alchemy Singapore uses fully automated Order Management and Portfolio Management System.</li>

                                    </ul>
                                </div>

                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            //closed: 'accordion', // Start closed if in accordion view
            activate: function (event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });

        var body = $("html, body");

        $('h2.resp-accordion').click(function () {
            var scrollTopVert = $(this).parent().offset().top;
            console.log(scrollTopVert);
            body.animate({ scrollTop: scrollTopVert }, 500, 'swing');
        });

        $('.offering').click(function () {
            var scrollTopVert = $('#alchemy_offering').offset().top - 100;
            body.animate({ scrollTop: scrollTopVert }, 500, 'swing');
        })

        /*******************************************************************/

        $('form select').change(function () {
            var selectedText = $(this).find("option:selected").text();
            console.log(selectedText);

            if (selectedText == 'Others') {

                $('.otherField').slideDown();
            } else {
                $('.otherField').slideUp();
            }

        })

        $('.heading').click(function () {
            if ($(this).children('.fa').hasClass('fa-plus')) {
                $(this).children('.fa').removeClass('fa-plus');
                $(this).siblings('.heading').children('.fa').addClass('fa-plus');
                $(this).children('.fa').addClass('fa-minus');
                $(this).siblings('.heading').children('.fa').removeClass('fa-minus');
            } else {
                $(this).children('.fa').addClass('fa-plus');
                //$(this).siblings('.heading').children('.fa').removeClass('fa-plus');
                $(this).children('.fa').removeClass('fa-minus');
                //$(this).siblings('.heading').children('.fa').addClass('fa-minus');
            }
            $(this).next().siblings('.content').slideUp();
            $(this).next().slideToggle();

        })

        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }
		
		
		  var body = $("html, body");
		  
		  /*$('.heading').click(function () {
		      if ($(window).width() <= 768) {
		          var scrollTopVert = $(this).parents('.whiteBox').offset().top;
		          //console.log(scrollTopVert);
		          body.animate({ scrollTop: scrollTopVert }, 'swing');
		      } else {
		          var divHeight = $(this).prev().height();
		          var scrollTopVert = $(this).offset().top - (divHeight + 200);
		          console.log(scrollTopVert);
		          body.animate({ scrollTop: scrollTopVert }, 'swing');
		      }
		      });*/
	</script>
</asp:Content>

