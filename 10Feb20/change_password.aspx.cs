﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class change_password : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if (Session["UserId"] == null)
            {
                Response.Redirect("login.aspx");
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("stp_Registration"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Para", "UpdatePassword");
            cmd.Parameters.AddWithValue("@RegId", Session["UserId"].ToString());
            cmd.Parameters.AddWithValue("@Password", txtold.Text.Trim());
            cmd.Parameters.AddWithValue("@NewPassword", txtnew.Text.Trim());
            if (utility.Execute(cmd))
            {
                divSuccess.Visible = true;
                divError.Visible = false;
            }
            else
            {
                lblError.Text = "Old password incorrect";
                divSuccess.Visible = false;
                divError.Visible = true;
            }
        }
    }
}