﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="change_password.aspx.cs" Inherits="change_password" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="banner" class="position-relative mb-5">
        <h1>Change Password</h1>
        <img src="images/banner/about_banner.jpg" class="img-fluid" alt="">
    </div>

    <div class="container mb-4">

        <div class="row mb-5" id="loginForm">
            <div class="col-12 col-md-8 col-lg-5">

                <div class="row">
                    <div class="col-12">
                        <div class="alert alert-warning alert-dismissible fade show" role="alert" style="display: none;">
                            <strong>All fileds are required!</strong><br/> <label id="lblmsg"></label>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="alert alert-danger alert-dismissible fade show" role="alert" id="divError" runat="server" visible="false">
                            <strong>
                                <asp:Label ID="lblError" runat="server"></asp:Label>
                            </strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="alert alert-success alert-dismissible fade show" role="alert" id="divSuccess" runat="server" visible="false">
                            Password changed successfully!
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <label>Old Password</label>
                        <asp:TextBox ID="txtold" runat="server" CssClass="form-control" autocomplete="off" TextMode="Password"></asp:TextBox>

                    </div>
                    <div class="col-12">
                        <label>New Password</label>
                        <asp:TextBox ID="txtnew" runat="server" CssClass="form-control" autocomplete="off" TextMode="Password"></asp:TextBox>
                    </div>
                    <div class="col-12">
                        <label>Confirm Password</label>
                        <asp:TextBox ID="txtconfirm" runat="server" CssClass="form-control" autocomplete="off" TextMode="Password"></asp:TextBox>
                    </div>
                    <div class="col-12">
                        <div class="row align-items-center">
                            <div class="col-7">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="changePassBtn" Text="Change Password" OnClick="btnSubmit_Click" OnClientClick="return checkValidation()" />

                            </div>

                            <!--<div class="col-7 text-right logLinks">
            <span>
            <a href="#">Forgot Password</a> | <a href="registration.html">New Registration</a>
            </span>
        </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="brown-dashed-border mb-5"></div>


    </div>

    <script>

        function checkValidation() {

            var oldPassword = $('#<%=txtold.ClientID%>').val();
            var password = $('#<%=txtnew.ClientID%>').val();
            var confpassword = $('#<%=txtconfirm.ClientID%>').val();
            var blank = false;
            var msg = "";

            if (oldPassword == '') {
                $('#<%=txtold.ClientID%>').css('border-color', 'red');
                msg += "Password, ";
                blank = true;

            } else {
                $('#<%=txtold.ClientID%>').css('border-color', '');
            }

            if (password == '') {
                $('#<%=txtnew.ClientID%>').css('border-color', 'red');
                msg += "New Password, ";
                blank = true;
            } else {
                if (password.length < 6) {
                    $('#<%=txtnew.ClientID%>').css('border-color', 'red');
                    msg += "Your new password must be at least 6 characters. Please try again, ";
                    blank = true;
                } else {
                    $('#<%=txtnew.ClientID%>').css('border-color', '');
                }
            }

            if (confpassword == '') {
                $('#<%=txtconfirm.ClientID%>').css('border-color', 'red');
                msg += "Confirm Password, ";
                blank = true;
            } else {
                if (confpassword != password) {
                    $('#<%=txtconfirm.ClientID%>').css('border-color', 'red');
                    msg += "Enter same new and confirm password., ";
                    blank = true;
                } else {
                    $('#<%=txtconfirm.ClientID%>').css('border-color', 'red');
                }
            }

            if (msg != "") {
                msg = msg.substring(0, msg.length - 2);
                $('#lblmsg').text(msg);
                $('.alert').slideDown('slow');
            } else {
                $('.alert').hide('slow');
            }

            if (blank) {
                return false;
            } else {
                return true;
            }
        }

        $(".close").click(function () {
            $('.alert').slideUp('slow');
        });

        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

    </script>

</asp:Content>

