﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="our_product.aspx.cs" Inherits="our_product" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="banner" class="position-relative mb-5">

        <h1>Our Products</h1>
        <img src="images/banner/fund_banner.jpg" class="img-fluid" alt="">
        
    </div>



    <div class="container mb-4">
        <div class="row">
            <div class="col-12">

                <ul class="breadcrumb">
                <li><a href="index.aspx">Home</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li>Offering</li>
                </ul>

            </div>
        </div>

        <div class="row innerPageData mb-4">
            <div class="col-12">
                <h4>Alchemy Offering</h4>
                <p>Alchemy Investment Management Pte Ltd offers its fund management services in both <strong>Comingled Fund and Separately Managed Account Format.</strong></p>

                <p class="text-uppercase">Merits of A Long-Biased Approach To India</p>

                <ul class="bullet">
                    <li>A long-biased strategy enables investors to tap into India’s long-term secular upside.</li>

                    <li>Affords the flexibility to manage company- and market-specific risks by hedging through shorting certain indices and stocks.</li>

                    <li>Generate better alpha by focusing on  mispricing of high quality companies and identifying under researched ideas.</li>

                    <li>Not concerned with market-timing issues like a typical long/short equity manager.</li>

                    <li>Not concerned with working against long-term trends like a typical market-neutral equity manager.</li>

                </ul>

                <div class="brown-dashed-border"></div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-xl-12">
                <div id="verticalTab">
                    <ul class="resp-tabs-list mb-4">
                        <li>Comingled Fund</li>
                        <li>Separate Managed Account (SMA)</li>
                    </ul>

                    <div class="resp-tabs-container innerPageData">
                        <div>
                            <div class="whiteBox p-4">
                                <h4>Comingled Fund</h4>
                                <p>An interested investor, who is looking for actively managed Indian exposure, can invest through Alchemy’s pooled investment vehicle. The vehicle is a private investment fund, structured in a master feeder format, which offers interests to qualifying global investor through its feeder funds. The portfolio is managed pursuant to a long/short equity strategy with long bias and the objective is to generate absolute returns over a market cycle. The strategy has a consistent track record of over a decade. ."Log in / Register here" to learn  more about Alchemy Pooled Investment Vehicle.</p>
                                <a href="#footer" class="connectBtn brownBg fadeAnim position-relative scrollTo" ><span>
                                    <img src="images/phone_icon.png">
                                    GET In Touch</span></a>

                            </div>
                        </div>

                        <div>
                            <div class="whiteBox p-4">
                                <h4>Separate Managed Account (SMA)</h4>
                                <p>Alchemy Investment Management Pte Ltd is open to providing its investment management and advisory expertise to a Separate Managed Account (SMA), where the long term objective of the investor matches with our investment philosophy. These SMAs are offered to an institutional investors like Sovereign Wealth Fund, Pension funds, Endowment Funds, Mutual funds, Country Funds, Fund of Funds. Alchemy has  past experience of managing SMAs for high pedigree institutional investors including a SWF for eight years.</p>
                                <a href="#footer" class="connectBtn brownBg fadeAnim position-relative scrollTo"><span>
                                    <img src="images/phone_icon.png">
                                    GET In Touch</span></a>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.heading').click(function () {
            if ($(this).children('.fa').hasClass('fa-plus')) {
                $(this).children('.fa').removeClass('fa-plus');
                $(this).siblings('.heading').children('.fa').addClass('fa-plus');
                $(this).children('.fa').addClass('fa-minus');
                $(this).siblings('.heading').children('.fa').removeClass('fa-minus');
            } else {
                $(this).children('.fa').addClass('fa-plus');
                //$(this).siblings('.heading').children('.fa').removeClass('fa-plus');
                $(this).children('.fa').removeClass('fa-minus');
                //$(this).siblings('.heading').children('.fa').addClass('fa-minus');
            }
            $(this).next().siblings('.content').slideUp();
            $(this).next().slideToggle();

        })

        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            //closed: 'accordion', // Start closed if in accordion view
            activate: function (event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });

        /**************************************************************************/

        var body = $("html, body");

        $('h2.resp-accordion').click(function () {
            var scrollTopVert = $(this).parent().offset().top;
            console.log(scrollTopVert);
            body.animate({ scrollTop: scrollTopVert }, 500, 'swing');
        });

        $('.offering').click(function () {
            var scrollTopVert = $('#alchemy_offering').offset().top - 100;
            body.animate({ scrollTop: scrollTopVert }, 500, 'swing');
        })

        /*******************************************************************/

        $('form select').change(function () {
            var selectedText = $(this).find("option:selected").text();
            console.log(selectedText);

            if (selectedText == 'Others') {

                $('.otherField').slideDown();
            } else {
                $('.otherField').slideUp();
            }

        })

        /*******************************************************************/

        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }
		







    </script>

</asp:Content>

