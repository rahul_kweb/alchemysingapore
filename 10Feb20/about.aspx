﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="about.aspx.cs" Inherits="aboutUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="banner" class="position-relative mb-5">
        <h1>ABOUT US</h1>
        <img src="images/banner/about_banner.jpg" class="img-fluid" alt="">
    </div>

    

    <div class="container mb-4">
        <div class="row">
            <div class="col-12">

                <ul class="breadcrumb">
                <li><a href="index.aspx">Home</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li><a href="about.aspx">About</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li>Overview</li>
                </ul>

            </div>

        </div>

        <div class="row mb-5">
            <div class="col-12">
                <h1 class="brownColor">Overview</h1>
                <p>Alchemy Investment Management Pte Ltd, Singapore (Alchemy Singapore) was incorporated in February 2007 as a 100% subsidiary of Alchemy Capital Management Pvt Ltd, India to provide Investment Management and Advisory Services to overseas investors/institutions. Alchemy Singapore holds a Capital Market Services license issued by the Monetary Authority of Singapore (MAS) to carry out Fund management activity for Accredited / Institutional Investors and is a registered Investment Adviser under Section 203(c) of the Investment Adviser Act of 1940 with the Securities and Exchange Commission (SEC) of the USA.</p>
                <p>The target clientele of Alchemy Singapore are Accredited / Institutional Investors like Sovereign Wealth Funds, Pension funds, Endowment Funds, Mutual funds, Country Funds, Fund of Funds, Family offices and UHNIs, who want an actively managed exposure to India.</p>
                <p>Alchemy Singapore receives non binding advisory services from its parent entity Alchemy Capital Management Pvt Ltd, India.</p>
            </div>
        </div>

        <div class="brown-dashed-border mb-5"></div>

        <div class="row justify-content-center">
            <div class="col-xl-12">
                <div id="verticalTab">
                    <ul class="resp-tabs-list mb-4">
                        <li id="vision">Our Vision</li>
                        <li id="mission">Our Mission</li>
                        <li id="ethos">Ethos</li>
                        <li id="values">Values</li>
                        <li id="milestones">Milestones</li>
                    </ul>

                    <div class="resp-tabs-container innerPageData">
                        <div class="content">
                            <h4>Our Vision</h4>

                            <ul class="bullet">
                                <li>To be a select asset and portfolio management firm through steady and excellent performance.</li>
                                <li>To be the investment manager of choice for Indian equities through delivering consistent long-term superior performance.</li>
                            </ul>
                        </div>

                        <div>
                            <h4>Our Mission</h4>

                            <p>To meet the long-term objectives of all our stakeholders through consistent superior performance, high standards of ethical behaviour and transparency with superior service and execution efficiency.</p>
                        </div>

                        <div>
                            <h4>Ethos</h4>

                            <p>Where the mind is without fear and the head is held high.</p>

                            <p>Alchemy believes that knowledge is power. The firm stands on a culture of learning, excellence, integrity and insight that come together to produce results which meet and exceed the clientele’s expectations.</p>
                        </div>

                        <div>
                            <h4>Values</h4>

                            <p class="text-uppercase">Alchemy’s foundation is based on:</p>
                            <ul class="bullet">
                                <li>Using knowledge, insights and experience to meet and exceed client expectations.</li>
                                <li>The realization that there could be no limit to knowledge.</li>
                                <li>Being resolute in achieving long term goals, yet being dynamic and original in our approach.</li>
                                <li>Pursuing excellence through highest quality practices.</li>
                                <li>Leading by example and nurturing teamwork.</li>
                                <li>Marrying integrity and transparency in all transactions and relationships.</li>
                            </ul>


                        </div>

                        <div>
                            <h4>Milestones</h4>
                            <h5 class="brownColor"><i class="fa fa-flag mr-3" aria-hidden="true"></i><strong>2007</strong></h5>
                            <p>Alchemy Investment Management Pte Ltd (Alchemy Singapore) is formed and operates as an Exempt Fund Manager and Advisor with the Monetary Authority of Singapore.</p>
                            <div class="brown-dashed-border"></div>

                            <h5 class="brownColor"><i class="fa fa-flag mr-3" aria-hidden="true"></i><strong>2007</strong></h5>
                            <p>Alchemy Singapore launches its first equity fund - Alchemy India Fund Ltd.</p>
                            <div class="brown-dashed-border"></div>

                            <h5 class="brownColor"><i class="fa fa-flag mr-3" aria-hidden="true"></i><strong>2008</strong></h5>
                            <p>Alchemy Singapore launches its second equity fund Alchemy India Long Term Fund Ltd.</p>
                            <div class="brown-dashed-border"></div>

                            <h5 class="brownColor"><i class="fa fa-flag mr-3" aria-hidden="true"></i><strong>2014</strong></h5>
                            <p>Alchemy Singapore gets  fund management  license with Monetary Authority of Singapore from its exempt status.</p>
                            <div class="brown-dashed-border"></div>

                            <h5 class="brownColor"><i class="fa fa-flag mr-3" aria-hidden="true"></i><strong>2015</strong></h5>
                            <p>Alchemy Singapore becomes registered Investment Adviser under Section 203(c) of the Investment Adviser Act of 1940 with the Securities and Exchange Commission (SEC) of the USA.</p>
                            <div class="brown-dashed-border"></div>

                            <h5 class="brownColor"><i class="fa fa-flag mr-3" aria-hidden="true"></i><strong>2016  </strong></h5>
                            <p>Alchemy Singapore won two pension fund mandates from North America.</p>
                            <div class="brown-dashed-border"></div>



                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            //closed: 'accordion', // Start closed if in accordion view
            activate: function (event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });



        $('.heading').click(function () {
            if ($(this).children('.fa').hasClass('fa-plus')) {
                $(this).children('.fa').removeClass('fa-plus');
                $(this).siblings('.heading').children('.fa').addClass('fa-plus');
                $(this).children('.fa').addClass('fa-minus');
                $(this).siblings('.heading').children('.fa').removeClass('fa-minus');
            } else {
                $(this).children('.fa').addClass('fa-plus');
                //$(this).siblings('.heading').children('.fa').removeClass('fa-plus');
                $(this).children('.fa').removeClass('fa-minus');
                //$(this).siblings('.heading').children('.fa').addClass('fa-minus');
            }
            $(this).next().siblings('.content').slideUp();
            $(this).next().slideToggle();

        })

        if (window.location.hash) {
            var hash = window.location.hash;
            console.log(hash);
            $(hash).trigger('click');
            $('html, body').animate({ scrollTop: $('#verticalTab').offset().top - 80 }, 1500);
        }

        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

        /**********************************************/

    </script>


</asp:Content>

