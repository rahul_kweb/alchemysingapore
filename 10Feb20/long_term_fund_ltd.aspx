﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="long_term_fund_ltd.aspx.cs" Inherits="long_term_fund_ltd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="banner" class="position-relative mb-5">
        <h1>ALCHEMY INDIA LONG TERM FUND LTD</h1>
        <img src="images/banner/fund_banner.jpg" class="img-fluid" alt="">
    </div>

    <div class="container mb-4">

        <div class="row">
            <div class="col-12">

                <ul class="breadcrumb">
                <li><a href="index.aspx">Home</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li>Funds</li>
                </ul>

            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-xl-12">
                <div id="verticalTab">
                    <ul class="resp-tabs-list mb-4">
                        <li>Fund Strategy ALCHEMY INDIA LONG TERM FUND LTD</li>
                        <li>Fund Terms* ALCHEMY INDIA LONG TERM FUND LTD</li>
                        <li>Fund Structure ALCHEMY INDIA LONG TERM FUND LTD</li>
                        <li>Fund Reports ALCHEMY INDIA LONG TERM FUND LTD</li>
                    </ul>

                    <div class="resp-tabs-container innerPageData">
                        <div>
                            <h4>Fund Strategy</h4>
                            <p class="text-uppercase mb-4">Alchemy India Long Term Fund uses a number of levers within its strategy* to generate long-term absolute returns on investor capital.</p>

                            <div class="container p-0">
                                <div class="row">
                                    <div class="col-md-4 mb-4">
                                        <div class="whiteBox text-center h-100">
                                            <div class="circle rounded-circle d-flex mb-4">
                                                <span class="m-auto">1</span>
                                            </div>

                                            <h2 class="mb-4">Long
                                                <br>
                                                Strategy</h2>

                                            <p>The Fund takes long-term concentrated positions in publicly listed companies in India without being benchmarked to any index </p>

                                        </div>
                                    </div>

                                    <div class="col-md-4 mb-4">
                                        <div class="whiteBox text-center h-100">
                                            <div class="circle rounded-circle d-flex mb-4">
                                                <span class="m-auto">2</span>
                                            </div>

                                            <h2 class="mb-4">Short
                                                <br>
                                                Strategy</h2>

                                            <p>The Fund does not seek to be consistently hedged. However, the portfolio manager may make opportunistic short calls to hedge a part of the portfolio through options and/or futures on the index or individual stocks </p>

                                        </div>
                                    </div>

                                    <div class="col-md-4 mb-4">
                                        <div class="whiteBox text-center h-100">
                                            <div class="circle rounded-circle d-flex mb-4">
                                                <span class="m-auto">3</span>
                                            </div>

                                            <h2 class="mb-4">PIPES / IPO<span class="smallText">s</span> /
                                                <br>
                                                Pre-IPO<span class="smallText">s</span></h2>

                                            <p>From time to time, the Fund will invest in PIPES, IPOs and Pre-IPOs that the management team deems compelling</p>

                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <p class="col-12">
                                        * The Fund’s strategy is subject to Risk Factors and the provisions of the Offer Documents. 
For complete details, please refer to the Offer Documents.
                                    </p>
                                </div>
                            </div>

                        </div>

                        <div>
                            <h4>Fund Terms*</h4>

                            <div class="whiteBox p-4 mb-4">
                                <div class="container fundTerms p-0">
                                    <div class="row">
                                        <div class="col-md-4 ">
                                            <p class="brownColor">Fund Type</p>
                                        </div>
                                        <div class="col-md-1 d-none d-md-block">:</div>
                                        <div class="col-md-7 ">
                                            <p>Open Ended</p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 ">
                                            <p class="brownColor">Minimum Investment </p>
                                        </div>
                                        <div class="col-md-1 d-none d-md-block">:</div>
                                        <div class="col-md-7 ">
                                            <p>US$ 500,000</p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 ">
                                            <p class="brownColor">Subscription Frequency </p>
                                        </div>
                                        <div class="col-md-1 d-none d-md-block">:</div>
                                        <div class="col-md-7 ">
                                            <p>Monthly</p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 ">
                                            <p class="brownColor">Management Fee</p>
                                        </div>
                                        <div class="col-md-1 d-none d-md-block">:</div>
                                        <div class="col-md-7">
                                            <p>1.5% p.a. charged monthly in arrears</p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 ">
                                            <p class="brownColor">Performance Fee</p>
                                        </div>
                                        <div class="col-md-1 d-none d-md-block">:</div>
                                        <div class="col-md-7">
                                            <p>15% p.a. with High Water Mark</p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 ">
                                            <p class="brownColor">Lock in </p>
                                        </div>
                                        <div class="col-md-1 d-none d-md-block">:</div>
                                        <div class="col-md-7">
                                            <p>One year hard lock</p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 ">
                                            <p class="brownColor">Redemptions </p>
                                        </div>
                                        <div class="col-md-1 d-none d-md-block">:</div>
                                        <div class="col-md-7">
                                            <p>Calendar Quarterly post expiry of lock-in period</p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 ">
                                            <p class="brownColor">Notice Period </p>
                                        </div>
                                        <div class="col-md-1 d-none d-md-block">:</div>
                                        <div class="col-md-7">
                                            <p>45 days</p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 ">
                                            <p class="brownColor">Exit Load 	</p>
                                        </div>
                                        <div class="col-md-1 d-none d-md-block">:</div>
                                        <div class="col-md-7">
                                            <p>No Exit load, post expiry of lock in period</p>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <p>*These are not the complete terms; please refer the respective PPM and supplement for complete details.</p>

                        </div>

                        <div>
                            <h4>Fund Structure</h4>
                            <div class="whiteBox p-4 mb-4">

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="heading_1 row justify-content-start">
                                            <div class="col-2 col-lg-2">
                                                <span class="rounded-circle circle mr-3 ml-0 d-block">A</span>
                                            </div>
                                            <div class="col-10 col-lg-10">
                                                <strong>Fund Structure for US taxable investor</strong>
                                                <!--<i class="fa fa-minus" aria-hidden="true"></i>-->
                                            </div>

                                            <div class="brown-dashed-border col-12 mt-3"></div>
                                        </div>

                                        <div class="content" style="display: block;">
                                            <h4><span class="brownColor">Investor Type :</span> US Taxable Investors</h4>

                                            <p class="text-uppercase">Partnership Feeder Fund - Cayman Island
                                                <br>
                                                Alchemy India Fund (Cayman) Partners LP</p>

                                            <p>
                                                A Limited Partnership incorporated under Cayman Islands laws.<br>
                                                Registered with Cayman Island Monetary Authority (CIMA)
                                            </p>

                                            <div class="brown-dashed-border"></div>

                                            <p class="text-uppercase">
                                                Master Fund - Mauritius<br>
                                                Alchemy India Long Term Fund Ltd
                                            </p>

                                            <p>
                                                A company incorporated with Limited Liability under Mauritius laws.
                                                <br>
                                                Registered with Financial Services Commission (FSC), holding Global Business License
                                                <br>
                                                Category 1 (GBL). Registered as Category II FPI under SEBI (FPI) Regulations
                                            </p>

                                            <div class="brown-dashed-border"></div>

                                            <p class="text-uppercase">
                                                Investment Manager &amp; General Partner - Singapore
Alchemy Investment Management Pte Ltd
                                            </p>

                                            <p>
                                                A company incorporated with Limited Liability under Singapore laws.
                                                <br>
                                                Licensed Fund Management Co by Monetary Authority of Singapore (MAS).
                                                <br>
                                                Registered Investment Advisor with Securities Exchange Commission (SEC) of USA.<br>
                                                Investment Manager of the Master Fund and General Partner of the Partnership Feeder Fund
                                            </p>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="heading_1 row justify-content-start">
                                            <div class="col-2 col-lg-2">
                                                <span class="rounded-circle circle mr-3 ml-0 d-block">B</span>
                                            </div>
                                            <div class="col-10 col-lg-10">
                                                <strong>FUND STRUCTURE FOR US TAX EXEMPT AND NON US INVESTOR</strong>
                                                <!--<i class="fa fa-minus" aria-hidden="true"></i>-->
                                            </div>
                                            <div class="brown-dashed-border col-12 mt-3"></div>
                                        </div>

                                        <div class="content" style="display: block">
                                            <h4><span class="brownColor">Investor Type :</span> US Tax-exempt &amp; Non US Investors</h4>
                                            <p class="text-uppercase">
                                                Company Feeder Fund - Cayman Island<br>
                                                Alchemy India Equity Fund (Cayman) Ltd
                                            </p>

                                            <p>
                                                A Limited Company incorporated under Cayman Island laws. <br /> Registered with Cayman Island 
Monetary Authority (CIMA)
                                            </p>

                                            <div class="brown-dashed-border"></div>

                                            <p class="text-uppercase">
                                                Master Fund - Mauritius<br>
                                                Alchemy India Long Term Fund Ltd
                                            </p>

                                            <p>
                                                A company incorporated with Limited Liability under Mauritius laws.<br>
                                                Registered with Financial Services Commission (FSC), holding Global Business License 
Category 1 (GBL). Registered as Category II FPI under SEBI (FPI) Regulations
                                            </p>

                                            <div class="brown-dashed-border"></div>

                                            <p class="text-uppercase">
                                                Investment Manager - Singapore
                                                <br>
                                                Alchemy Investment Management Pte Ltd
                                            </p>

                                            <p>
                                                A company incorporated with Limited Liability under Singapore laws. Licensed Fund 
Management Co by Monetary Authority of Singapore (MAS). Registered Investment Advisor 
with Securities Exchange Commission (SEC) of USA. Investment Manager of the Master Fund 
and Feeder Fund
                                            </p>

                                        </div>
                                    </div>
                                </div>


                            </div>

                        </div>

                        <div>
                            <h4>Fund Reports</h4>
                            <div class="whiteBox p-4 mb-4">

                                <div class="heading">
                                    <span class="rounded-circle circle d-inline-block mr-3">1</span>
                                    <strong>Download Reports for 2019-2020</strong>
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </div>

                                <div class="content newCont" style="display: block;">

                                    <div class="report">
                                        <a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                        </a>
                                    </div>

                                    <div class="report">
                                        <a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                        </a>
                                    </div>

                                    <div class="brown-dashed-border"></div>

                                </div>

                                <div class="heading">
                                    <span class="rounded-circle circle d-inline-block mr-3">2</span>
                                    <strong>Download Reports for 2018-2019</strong>
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </div>

                                <div class="content">
                                    <div class="report">
                                        <a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                        </a>
                                    </div>

                                    <div class="report">
                                        <a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                        </a>
                                    </div>

                                    <div class="brown-dashed-border"></div>

                                </div>


                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            //closed: 'accordion', // Start closed if in accordion view
            activate: function (event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });

        /**************************************************************************/

        var body = $("html, body");

        $('h2.resp-accordion').click(function () {
            var scrollTopVert = $(this).parent().offset().top;
            console.log(scrollTopVert);
            body.animate({ scrollTop: scrollTopVert }, 500, 'swing');
        });

        $('.offering').click(function () {
            var scrollTopVert = $('#alchemy_offering').offset().top - 100;
            body.animate({ scrollTop: scrollTopVert }, 500, 'swing');
        })

        /*******************************************************************/

        $('form select').change(function () {
            var selectedText = $(this).find("option:selected").text();
            console.log(selectedText);

            if (selectedText == 'Others') {

                $('.otherField').slideDown();
            } else {
                $('.otherField').slideUp();
            }

        })

        /*******************************************************************/

        /*$('.heading').click(function () {
            if ($(this).children('.fa').hasClass('fa-plus')) {
                $(this).children('.fa').removeClass('fa-plus');
                $(this).siblings('.heading').children('.fa').addClass('fa-plus');
                $(this).children('.fa').addClass('fa-minus');
                $(this).siblings('.heading').children('.fa').removeClass('fa-minus');
            } else {
                $(this).children('.fa').addClass('fa-plus');
                //$(this).siblings('.heading').children('.fa').removeClass('fa-plus');
                $(this).children('.fa').removeClass('fa-minus');
                //$(this).siblings('.heading').children('.fa').addClass('fa-minus');
            }
            $(this).next().siblings('.content').slideUp();
            $(this).next().slideToggle();

        })*/

        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }


    </script>

</asp:Content>

