﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="test-email-settings.aspx.cs" Inherits="test_email_settings" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlEmail" GroupingText="Email Tesing Setup" runat="server" Width="70%" style="margin: 0px auto;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="width: 10%">
                    Server Name : 
                </td>
                <td style="width: 90%">
                    <asp:TextBox ID="txtServerName" Width="100%" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Port : 
                </td>
                <td>
                    <asp:TextBox ID="txtPort" Width="100%" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Username : 
                </td>
                <td>
                    <asp:TextBox ID="txtUsername" Width="100%" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Password : 
                </td>
                <td>
                    <asp:TextBox ID="txtPassword" Width="100%" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:CheckBox ID="chkUseDefaultCredentials" Text="UseDefaultCredentials" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:CheckBox ID="chkEnableSsl" Text="EnableSsl" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    From : 
                </td>
                <td>
                    <asp:TextBox ID="txtFrom" Width="100%" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    To : 
                </td>
                <td>
                    <asp:TextBox ID="txtTo" Width="100%" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    CC : 
                </td>
                <td>
                    <asp:TextBox ID="txtCC" Width="100%" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    BCC : 
                </td>
                <td>
                    <asp:TextBox ID="txtBCC" Width="100%" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Body : 
                </td>
                <td>
                    <asp:TextBox ID="txtBody" Width="100%" TextMode="MultiLine" Rows="5" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:CheckBox ID="chkIsBodyHtml" Text="IsBodyHtml" runat="server" />
                    (will append &lt;b&gt;YOUR TEXT&lt;/b&gt;)
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:CheckBox ID="chkIsWriteToDisk" Text="Write emails to disk" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnSend" Text="Send" runat="server" onclick="btnSend_Click" /><br />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblResult" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    </form>
</body>
</html>
