﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="atul_sharma.aspx.cs" Inherits="atul_sharma" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="banner" class="position-relative mb-5">
        <h1>Alchemy
            Team</h1>
        <img src="images/banner/about_banner.jpg" class="img-fluid" alt="">
    </div>

    <div class="container mb-4">
        <div class="row">
            <div class="col-12">

                <ul class="breadcrumb">
                <li><a href="index.aspx">Home</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li><a href="our_team.aspx">Our Team</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li>Atul Sharma</li>
                </ul>

            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-xl-12">
                <div>
                    <!--<ul class="resp-tabs-list mb-4">
        <li>Group Founders</li>
        <li>Fund Management Team</li>
        <li>Advisory Team</li>

        </ul>-->

                    <div class="innerPageData" id="personData">
                        <div>
                            <h4>Fund Management Team</h4>

                            <div class="whiteBox">
                                <div class="container p-0">
                                    <div class="row align-items-center">
                                        <div class="col-sm-6 col-md-4 col-lg-3">
                                            <img src="images/team/atul_sharma.jpg" class="img-fluid" alt="">
                                        </div>

                                        <div class="col-sm-6 col-md-8 col-lg-9">
                                            <h4>Atul Sharma - <span class="brownColor">Fund Manager</span></h4>
                                            <p>Mr. Atul Sharma has over two decades of global business advisory and direct investment experience. Mr. Sharma has advised Asian Governments, Sovereign Wealth Funds, Conglomerates, MNCs and Multilateral Agencies across several industry sectors to address critical challenges related to economic development, creating economic profits / shareholder value, business strategy, growth, cost &amp; balance sheet management and corporate governance. He is a strong advocate of the fundamentals driven investment style that lays emphasis on the right combination of concepts / metrics like economic moats, ROIC, growth and valuation multiples. Prior to joining Alchemy, Mr. Sharma was an independent advisor helping clients identify and execute their private equity / direct investments and an adjunct faculty at a University in Singapore. Prior to this, Mr. Sharma worked for over 15 years in corporate finance and management consulting firms; including business leadership roles in Stern Stewart &amp; Co and advisory leadership roles in Accenture, Coopers &amp; Lybrand and Arthur Andersen. Mr. Sharma has an MBA degree from the Asian Institute of Management and a B.Sc. Physics degree from St. Xavier’s  College Mumbai.</p>

                                            <!--<a href="our_team.html" class="readMore brownBg">Back</a>-->
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>







                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

        /*****************************************/

        $(window).load(function () {

            var mainVal = sessionStorage.getItem("valueData");
            if (mainVal == "personData") {
                var scrollTop = $('#' + mainVal).offset().top;
                $('html, body').animate({
                    scrollTop: (scrollTop - 300)
                }, 500);
            }

            sessionStorage.removeItem("valueData");
            return false;

        })
    </script>
</asp:Content>

