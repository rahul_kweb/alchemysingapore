﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="thought_leadership.aspx.cs" Inherits="thought_leadership" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="banner" class="position-relative mb-5">
        <h1>Thought Leadership</h1>
        <img src="images/banner/thought_leadership.jpg" class="img-fluid" alt="">
    </div>

    <div class="container mb-4">
        <div class="row">
            <div class="col-12">

                <ul class="breadcrumb">
                <li><a href="index.aspx">Home</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li>Thought Leadership</li>
                </ul>

            </div>
        </div>
        <div class="row mb-5 innerPageData" id="thoughtLeadership">
            <div class="col-12 mb-4">
                <p>Alchemy strives not only to expand your wealth, but also the horizon of your financial knowledge. Thought Leadership is an exhaustive collection of blogs with additional inputs from industry experts about the different aspects to superlative asset management</p>
            </div>

            <div class="col-md-6 col-lg-4 mb-4">
                <div class="whiteBox h-100">
                    <h4 class="brownColor mb-2">EQUITY MARKET OUTLOOK</h4>
                    <p><strong>December 2019 </strong></p>

                    <p class="text-justify">Markets pausing for breath... The markets remained resilient through November 2019 with the Nifty rising 2.2% and the NSE Midcap rising 5.7%. The NSE Bank was one of the key sectoral indices, rising >8% this month. The concurrent indicators for the economy re-mained weak. </p>

                    <a href="equity_market_outlook_dec_2019.aspx" class="readMore brownBg">Read More</a>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-4">
                <div class="whiteBox h-100">
                    <h4 class="brownColor mb-2">EQUITY MARKET OUTLOOK</h4>
                    <p><strong>November 2019</strong></p>

                    <p class="text-justify"><strong>The Market continues to be strong...</strong></p>

                    <p class="text-justify">October 2019 was another strong month for equities, with the Nifty rising 3.5% on the back of a 4% rally in September. A notable feature was that the Nifty midcap outperformed the Nifty, rising by 4.9%. The sectoral performance remained tilted towards Autos and FMCG, with banks slightly underperforming. </p>

                    <a href="equity_market_outlook_nov_2019.aspx" class="readMore brownBg">Read More</a>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-4">
                <div class="whiteBox h-100">
                    <h4 class="brownColor mb-2">EQUITY MARKET OUTLOOK</h4>
                    <p><strong>September 2019</strong></p>

                    <p class="text-justify">Fear of unknown risks  The 7% correction in the market (with poor inter-nals) since April 2019 has created a better sync between expectations and reality. The momentum in the economy remains poor and gives little reason for comfort, </p>

                    <a href="equity_market_outlook_sep_2019.aspx" class="readMore brownBg">Read More</a>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-4">
                <div class="whiteBox h-100">
                    <h4 class="brownColor mb-2">EQUITY MARKET OUTLOOK</h4>
                    <p><strong>August 2019</strong></p>

                    <p class="text-justify">Market in Consolidation Mode </p>
                    <p class="text-justify">The rate cut has come and gone, but the post-budget sell-off in equities seems to be unabated and broad-based. The FPI tax was a small catalyst - the markets have belatedly started tracking the broader economy. </p>

                    <a href="equity_market_outlook_august_2019.aspx" class="readMore brownBg">Read More</a>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-4">
                <div class="whiteBox h-100">
                    <h4 class="brownColor mb-2">Indian Markets - Post Election Outlook</h4>
                    <p><strong>13 June, 2019</strong></p>

                    <p class="text-justify">13 June 2019 The BJP-led National Democratic Alliance won an absolute majority in India's parliamentary elections last month. This paved the way for Narendra Modi to be re-appointed as Prime Minister for a second consecutive term.  </p>

                    <a href="equity_market_outlook_june_2019.aspx" class="readMore brownBg">Read More</a>
                </div>
            </div>






        </div>

        <div class="brown-dashed-border mb-5"></div>


    </div>

    <script>
        $('.heading').click(function () {
            if ($(this).children('.fa').hasClass('fa-plus')) {
                $(this).children('.fa').removeClass('fa-plus');
                $(this).siblings('.heading').children('.fa').addClass('fa-plus');
                $(this).children('.fa').addClass('fa-minus');
                $(this).siblings('.heading').children('.fa').removeClass('fa-minus');
            } else {
                $(this).children('.fa').addClass('fa-plus');
                //$(this).siblings('.heading').children('.fa').removeClass('fa-plus');
                $(this).children('.fa').removeClass('fa-minus');
                //$(this).siblings('.heading').children('.fa').addClass('fa-minus');
            }
            $(this).next().siblings('.content').slideUp();
            $(this).next().slideToggle();

        })

        /**************************************************************************/

        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function (event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });


        /**************************************************************************/

        var body = $("html, body");

        $('h2.resp-accordion').click(function () {
            var scrollTopVert = $(this).parent().offset().top;
            console.log(scrollTopVert);
            body.animate({ scrollTop: scrollTopVert }, 500, 'swing');
        });

        $('.offering').click(function () {
            var scrollTopVert = $('#alchemy_offering').offset().top - 100;
            body.animate({ scrollTop: scrollTopVert }, 500, 'swing');
        })

        /*******************************************************************/

        $('form select').change(function () {
            var selectedText = $(this).find("option:selected").text();
            console.log(selectedText);

            if (selectedText == 'Others') {

                $('.otherField').slideDown();
            } else {
                $('.otherField').slideUp();
            }

        })

        /*******************************************************************/

        if (window.location.hash) {
            var hash = window.location.hash;
            console.log(hash);
            $(hash).trigger('click');
            $('html, body').animate({ scrollTop: $('#verticalTab').offset().top - 80 }, 1500);
        }

        /*******************************************************************/


        $('.radioContainer').click(function () {
            //alert($(this).checked);
            if ($(this).children('input').is(':checked')) {
                $('.radioBtnField').slideUp()
                $(this).parents('.radioBtn').siblings('.radioBtnField').slideDown();
            }
        })

        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

    </script>

</asp:Content>

