﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="equity_market_outlook_june_2019.aspx.cs" Inherits="equity_market_outlook_june_2019" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="banner" class="position-relative mb-5">
        <h1>Thought Leadership</h1>
        <img src="images/banner/thought_leadership.jpg" class="img-fluid" alt="">
    </div>

    <div class="container mb-4">

        <div class="row">
            <div class="col-12">

                <ul class="breadcrumb">
                <li><a href="index.aspx">Home</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li><a href="thought_leadership.aspx">Thought Leadership</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                <li>Indian Markets - Post Election Outlook - 13 June 2019</li>
                </ul>

            </div>
        </div>

        <div class="row mb-5 innerPageData" id="blog">
            <div class="col-12 mb-4">

                <h4 class="brownColor">Indian Markets - Post Election Outlook - 13 June 2019</h4>

                <p>The BJP-led National Democratic Alliance won an absolute  majority in India's parliamentary elections last month. This paved the way for  Narendra Modi to be re-appointed as Prime Minister for a second consecutive  term. The markets have responded positively to the results, with the Nifty  rallying 4% since the close of 17 May, 2019 (Bloomberg). </p>

                <p>The most significant political consequence of the result is  stability. The BJP surprised pundits by winning a parliament majority on its  own, which gives the Prime Minister significantly higher degrees of freedom to  implement policy. There were expectations that the elections would result in a  hung Parliament and a coalition government — the BJP's outright victory put  that fear to rest. </p>

                <p>The short-term objectives for the government should be  two-fold. First, it needs to address the extreme risk-aversion in the bond  markets and, consequently, help NBFCs and HFCs back on their feet so they can  start lending again. The best way to achieve this is to ensure an orderly resolution  of some of the stressed credits that are worrying lenders. The government,  though, has to tread carefully. It should not be seen to be bailing out  insolvent companies and creating moral hazard issues for the future. Second,  there is an urgent need to revive growth, which has been sluggish since the  beginning of 2019. In the short term, the focus should be on reviving the  consumption cycle. The longer-term sustainability of the growth revival  depends, however, on resurrecting the private sector investment cycle. </p>

                <p><strong>Policy options before the government</strong></p>
                <p>We believe the government is presented with an opportunity to undertake significant structural reform, given the strength of the mandate. </p>

                

                <p><strong>Interest rates </strong> require immediate attention. The RBI's cumulative 75bp rate cut is a positive, but needs to be accompanied by a significant injection of liquidity. The move to an accommodative stance in the policy of June 6, 2019 was a significant step in that direction, but the market is waiting for the new framework that is expected in mid-July, according to the RBI. The central bank is well-placed to ease liquidity, given weak inflation and greater post-election policy certainty. A major cut in interest rates, when transmitted, is one of the necessary conditions for a growth revival, though not sufficient. </p>

                <p>The political strength of the new government creates the opportunity for accelerated <strong>privatization</strong>. The plan has to go beyond incremental disinvestment: the government should hand over management reins and majority owner-ship of PSUs to the private sector. This has a twin-impact on the fiscal. It helps raise resources and reduces the need for the government to continue to fund losses of weak PSUs. The second-order benefit is that it improves capital efficiency and productivity of the economy — many PSUs operate sub-optimally and hurt the efficiency of its competitors too. </p>

                <p>The revival of the domestic capex cycle is heavily dependent on the <strong>infrastructure sector</strong>. The Modi-led government's first term was marked by a successful revival of the roads sector, via innovative structures like the hybrid annuity model and focused execution. It is now time to focus on the power sector. The BJP's improved political clout should help — the key to revival of the power sector would be to reform the state discoms. The central government needs to work in tandem with the state governments to help achieve this, primarily by lowering transmission and distribution loss ratios with greater billing efficiency. Once the discoms are back on their feet, they can start buying power from stranded gencos and create a virtuous cycle.</p>

                <p>A major area of reform that could unlock growth and employment is labour laws. The rigid structures have, perversely, ended up hurting employment as the cost of compliance has pushed companies to either outsource to contractors or speed up automation. It is also an impediment to foreign investment in India, especially in manufacturing. Some of the heavy lifting on this, admittedly, has to be done by the state governments. </p>

                <p>A strong central government, however, can and should act as an enabler and work with the state governments to help relax labour laws. There is an existing NITI Aayog report suggesting that 44 labour laws be consolidated into four simplified codes: this election victory provides a window to implement that.</p>

                <p>India's agriculture sector is suffering from multiple stress points — a misallocation of crops, weak credit flow, high intermediation costs, and fragmented land holdings. The Modi-led government should work with state governments to help address issues like APMC monopolies, modernising land records and, possibly, aggregating holdings in some form. On its part, the central government should work on attracting greater investments into the food processing sector. </p>

                <p>Rationalising tax rates is difficult in the current stressed fiscal scenario, especially with GST collections undershooting targets. A possible windfall dividend from the RBI and aggressive privatisation could, however, open the window. Two areas need attention: bringing down GST rates in general to help improve the efficiency of the economy. Also, the cascading taxes on corporate profits (corporate tax+dividend tax+income tax on large-ticket dividends) are a deterrent to private sector investment. A company that pays 100% of its profits as dividends ends up with an effective tax rate of —50% - this is not conducive to attracting private investments. </p>

                <p>Some commentators have stressed the need to revive jobs, real estate, FDI, exports and import substitution. There are some specific measures that can be taken to help these (like removing sectoral caps for FDI), but we see these largely as outcome variables. The focus should be to create a conducive atmosphere for investments — positive results should follow. In the near term, some sort of social security transfers may be necessary: that can be addressed by improving the efficiency of outcomes through schemes like DBT. The PM-KISAN scheme, which would presumably be continued, goes some way in serving that purpose.</p>

                <p><strong>Markets: short-term weakness would be an opportunity </strong></p>

                <p>In the short term, the markets do look vulnerable. Earnings momentum has been weak and we expect 1QFY20 to be another soft quarter. In that context, valuations look stretched we would not rule out a short-term correction, especially if one of the large potential insolvencies materialise. This would, however, be an opportunity to pick quality stocks for the longer term. Our optimism stems from expectations of a revival in the consumption cycle from the latter half of 2019, driven by post-election stability, lower interest rates and revival of credit flow. If the new Modi-led government undertakes structural reform, we see a revival in the investment cycle and an upward trend of India's potential growth rate. This should drive a broad-based market rally over two-three years that would extend beyond the financials-consumer led performance of the last four-five years. </p>

                <p><strong>Alchemy Group Research</strong></p>



            </div>




        </div>

        <div class="brown-dashed-border mb-5"></div>


    </div>
    <script>
        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

    </script>
</asp:Content>

