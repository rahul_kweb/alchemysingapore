﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="page_not_found.aspx.cs" Inherits="page_not_found" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container mb-4">

        <div class="row mb-5 innerPageData" id="loginForm">
            <div class="col-12 text-center pt-5 mb-4">
                <img src="images/404.jpg" class="img-fluid m-auto" alt="">
            </div>

            <div class="col-12 text-center">
                <h4>Page Not Found</h4>

                <a href="index.html" class="readMore brownBg">Back to Home</a>
            </div>

        </div>

        <div class="brown-dashed-border mb-5"></div>


    </div>

    <script>
        function initMap() {
            var locations = [
            ['Alchemy Investment Management Pte Ltd.', 1.282911, 103.849966],
            ['Alchemy Capital Management Pvt. Ltd.', 1.285006, 103.851992]
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(1.284309, 103.851467),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [{
                        saturation: 0
                    }]
                }]
            });
            var infowindow = new google.maps.InfoWindow();
            var image = "image";
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: "images/map-icon.png"
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

    </script>
</asp:Content>

