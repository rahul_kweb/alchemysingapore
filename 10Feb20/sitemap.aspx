﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="sitemap.aspx.cs" Inherits="sitemap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

	 <div id="banner" class="position-relative mb-5">
        <h1>Sitemap</h1>
        <img src="images/banner/sitemap.jpg" class="img-fluid" alt="">
    </div>

    <div class="container mb-4">

		<div class="row sitemap_wrp">
        	
            <div class="col-md-3">
            	
                <h4 class="brownColor mb-4">Our Team</h4>
                
              
                
                <ul class="bullet">
                <li><a href="lashit_sanghvi.aspx#personData"> Lashit Sanghvi </a></li>
<li><a href="hiren_ved.aspx#personData"> Hiren Ved </a></li>
<li><a href="rakesh_jhunjhunwala.aspx#personData"> Rakesh Jhunjhunwala   </a></li>
<li><a href="ashwin_kedia.aspx#personData"> Ashwin kedia </a></li>
<li><a href="atul_sharma.aspx#personData"> Atul Sharma </a></li>
<li><a href="amit_nadekar.aspx#personData"> Amit Nadekar </a></li>
<li><a href="seshadri_sen.aspx#personData"> Seshadri Sen </a></li>
<li><a href="vikas_kumar.aspx#personData"> Vikas Kumar </a></li>
               </ul> 
            </div>
            
            <div class="col-md-3">
            	
                <h4 class="brownColor mb-4">Our Products</h4>
                
                <ul class="bullet">
                    <li><a href="our_product.aspx">Offering</a></li>
                    <li><a href="long_term_fund_ltd.aspx">Funds</a></li>
				</ul>
                
            </div>
			
			<div class="col-md-3">
            	
                <h4 class="brownColor mb-4">Other Links</h4>
                
                <ul class="bullet">
					<li><a href="about.aspx">Overview</a></li>
					<li><a href="our_team.aspx">Our Team</a></li>
                  	<li><a href="advantage.aspx">Our Advantages</a></li> 
					<li><a href="our_process.aspx">Our Process</a></li>
					<li><a href="thought_leadership.aspx">Thought Leadership</a></li>
                    <li><a href="https://www.alchemycapital.com/" target="_blank">Alchemy Capital</a></li>
				</ul>
                
            </div>
            
            <div class="col-md-3">
            	
                <h4 class="brownColor mb-4">Support</h4>
                
              
                
                <ul class="bullet">
                <li><a href="lashit_sanghvi.aspx#personData"> Login </a></li>
<li><a href="hiren_ved.aspx#personData"> Register </a></li>
<li><a href="contact.aspx">Contact Us</a></li>
<li><a href="termsofuse.aspx">Terms Of Use</a></li>
               </ul> 
            </div>
            
           
            
        </div>
        
    </div>
    
</asp:Content>

