﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class our_process : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindFoundation(7);
            BindAProven(8);
            BindHandPicking(9);
            BindPortfolio(10);
            BindRiskManagement(11);
            BindInnerBanner(6);
        }
    }

    public string BindFoundation(int Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS'," + Id + "");
        if (dt.Rows.Count > 0)
        {
            ltrFoundation.Text = dt.Rows[0]["Description"].ToString();
        }
        return ltrFoundation.Text;
    }

    public string BindAProven(int Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS'," + Id + "");
        if (dt.Rows.Count > 0)
        {
            ltrAProven.Text = dt.Rows[0]["Description"].ToString();
        }
        return ltrAProven.Text;
    }

    public string BindHandPicking(int Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS'," + Id + "");
        if (dt.Rows.Count > 0)
        {
            ltrHandPicking.Text = dt.Rows[0]["Description"].ToString();
        }
        return ltrHandPicking.Text;
    }

    public string BindPortfolio(int Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS'," + Id + "");
        if (dt.Rows.Count > 0)
        {
            ltrPortfolio.Text = dt.Rows[0]["Description"].ToString();
        }
        return ltrPortfolio.Text;
    }

    public string BindRiskManagement(int Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS'," + Id + "");
        if (dt.Rows.Count > 0)
        {
            ltrRiskManagement.Text = dt.Rows[0]["Description"].ToString();
        }
        return ltrRiskManagement.Text;
    }

    public void BindInnerBanner(int Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            imgInnerBanner.ImageUrl = "Content/uploads/InnerBanner/" + dt.Rows[0]["Image"].ToString();
        }
        else
        {
            imgInnerBanner.ImageUrl = "images/banner/about_banner.jpg";
        }
    }
}