﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class market_views : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        BindInnerBanner(18);
        //BindUpperDesc(14);
        BindMarkets();
    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            imgInnerBanner.ImageUrl = "Content/uploads/InnerBanner/" + dt.Rows[0]["Image"].ToString();
        }
        else
        {
            imgInnerBanner.ImageUrl = "images/banner/about_banner.jpg";
        }


    }

    public void BindMarkets()
    {
        int Id = 5;
        StringBuilder strbuild = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_Insights 'bindYear',0,'" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            strbuild.Append("<div>");
            int count = 1;
            foreach (DataRow dr in dt.Rows)
            {
                if (count == 1)
                {
                    strbuild.Append("<div class='heading'>" + dr["Year"].ToString() + "<i class='fa fa-minus' aria-hidden='true'></i></div>");
                    count++;
                }
                else
                {
                    strbuild.Append("<div class='heading'>" + dr["Year"].ToString() + "<i class='fa fa-plus' aria-hidden='true'></i></div>");
                }

                strbuild.Append("<div class='content' style='display:block'>");
                strbuild.Append("<ul class='newsLetter'>");

                DataTable dt1 = new DataTable();
                dt1 = utility.Display("Exec Proc_Insights 'BindMarkets',0,'" + Id + "','" + dr["Year"].ToString() + "'");
                if (dt1.Rows.Count > 0)
                {
                    foreach (DataRow dr1 in dt1.Rows)
                    {
                        strbuild.Append("<li class='newsData'>");
                        if (dr1["Type"].ToString() == "Pdf")
                        {
                            strbuild.Append("<a href='Content/uploads/Pdf/" + dr1["Pdf"].ToString() + "' target='_blank'>");
                        }
                        else
                        {
                            strbuild.Append("<a href='" + dr1["Url"].ToString() + "' target='_blank'>");
                        }

                        strbuild.Append("<div class='newsDate'>" + dr1["PostDate"].ToString() + "</div>");
                        strbuild.Append("<span><strong>" + dr1["Title"].ToString() + "</strong></span>");
                        strbuild.Append("</a>");
                        strbuild.Append("</li>");
                    }
                }

                strbuild.Append("</ul>");
            strbuild.Append("</div>");
            }
            strbuild.Append("</div>");

        }
        ltrMarkets.Text = strbuild.ToString();

    }
}