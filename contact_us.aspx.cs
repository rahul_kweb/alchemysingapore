﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class contact : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindInnerBanner(7);
            SocialMedia(1);
        }
    }

    public void BindInnerBanner(int Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            imgInnerBanner.ImageUrl = "Content/uploads/InnerBanner/" + dt.Rows[0]["Image"].ToString();
        }
        else
        {
            imgInnerBanner.ImageUrl = "images/banner/about_banner.jpg";
        }
    }

    public void SocialMedia(int Id)
    {
        DataTable dt = new DataTable();
        Dictionary<string, string> obj = new Dictionary<string, string>();
        dt = utility.Display("Exec Proc_SocialMedia 'getbyId','" + Id + "'");
        if (dt.Rows.Count > 0)
        {

            ltrInvestorContact.Text = dt.Rows[0]["HeadOffice"].ToString();
            ltrRegisteredAddress.Text = dt.Rows[0]["CallUs"].ToString();
            ltrOfficeAddress.Text = dt.Rows[0]["MailUs"].ToString();
        }

    }
}